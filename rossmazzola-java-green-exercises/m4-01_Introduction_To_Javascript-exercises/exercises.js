﻿
function sumDouble(x, y)
{
    if (x == y)
    {
        return (x + y) * 2;
    }
    return x + y;
}

function hasTeen(x, y, z)
{
    if ((x >= 13 && x <= 19) ||
        (y >= 13 && y <= 19) ||
        (z >= 13 && z <= 19))
    {
        return true;
    }
    return false;
}

function lastDigit(x, y)
{
    if (x % 10 == y % 10)
    {
        return true;
    }
    return false;
}

function seeColor(str)
{
    if (str.slice(0, 3) == 'red')
    {
        return 'red';
    }
    if (str.slice(0, 4) == 'blue')
    {
        return 'blue';
    }
    return '';
}

function oddOnly(arr)
{
    for (i = 0; i < arr.length; i++)
    {
        if (arr[i] % 2 == 0)
        {
            arr.splice(i, 1);
            --i;
        }
    }
    return arr;
}

function frontAgain(str)
{
    if (str.slice(0, 2) ==
        str.slice(str.length - 2))
    {
        return true;
    }
    return false;
}

function cigarParty(x, bool)
{
    if (x >= 40 && x <= 60)
    {
        return true;
    }
    else if (x >= 40 && bool)
    {
        return true;
    }
    return false;
}

function fizzBuzz(x)
{
    let str = '';
    if (x % 3 == 0)
    {
        str += 'Fizz';
    }
    if (x % 5 == 0)
    {
        str += 'Buzz';
    }
    if (str != '')
    {
        return str;
    }
    return x;
}

function filterEvens(arr)
{
    for (i = 0; i < arr.length; i++)
    {
        if (arr[i] % 2 != 0)
        {
            arr.splice(i, 1);
            --i;
        }
    }
    return arr;
}

function filterBigNumbers(arr)
{
    for (i = 0; i < arr.length; i++)
    {
        if (arr[i] < 100)
        {
            arr.splice(i, 1);
            --i;
        }
    }
    return arr;
}

function filterMultiplesOfX(arr, x)
{
    for (i = 0; i < arr.length; i++)
    {
        if (arr[i] % x != 0)
        {
            arr.splice(i, 1);
            --i;
        }
    }
    return arr;
}

function createObject()
{
    let person = {
        firstName: 'abc',
        lastName: 'xyz',
        age: 1
    };
    return person;
}
