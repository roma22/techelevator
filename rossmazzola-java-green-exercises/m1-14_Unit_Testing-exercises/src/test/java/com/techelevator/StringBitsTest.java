package com.techelevator;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class StringBitsTest
{
	StringBits testing;

	@Before
	public void setUp() throws Exception
	{
		testing = new StringBits();
	}

	@Test
	public void getBits_ShouldReturnStringThatRemovedEveryOtherChar_WhenValueIsValidStringTest()
	{
		assertEquals("", testing.getBits(""));
		assertEquals("a", testing.getBits("a"));
		assertEquals("a", testing.getBits("ab"));
		assertEquals("ac", testing.getBits("abc"));
		assertEquals("ac", testing.getBits("abcd"));
		assertEquals("ace", testing.getBits("abcde"));
	}
}
