package com.techelevator;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class AnimalGroupNameTest
{
	AnimalGroupName testing;

	@Before
	public void setUp()
	{
		testing = new AnimalGroupName();
	}

	@Test
	public void getHerd_ShouldReturnUnknown_WhenValueIsNullTest()
	{
		assertEquals("unknown", testing.getHerd(null));
	}

	@Test
	public void getHerd_ShouldReturnUnknown_WhenValueIsEmptyStringTest()
	{
		assertEquals("unknown", testing.getHerd(""));
	}

	@Test
	public void getHerd_ShouldReturnUnknown_WhenValueIsNotFoundTest()
	{
		assertEquals("unknown", testing.getHerd("abc"));
	}

	@Test
	public void getHerd_ShouldReturnUnknown_WhenValueContainsButDoesNotEqualValidStringTest()
	{
		assertEquals("unknown", testing.getHerd("doge"));
	}

	@Test
	public void getHerd_ShouldAcceptValueRegardlessOfCase_WhenValueIsValidAndContainsLowercaseTest()
	{
		assertEquals("Pack", testing.getHerd("dog"));
	}

	@Test
	public void getHerd_ShouldAcceptValueRegardlessOfCase_WhenValueIsValidAndContainsUppercaseTest()
	{
		assertEquals("Pack", testing.getHerd("DOG"));
	}

	@Test
	public void getHerd_ShouldReturnExistingEntry_WhenValueIsValidTest()
	{
		assertEquals("Crash", testing.getHerd("Rhino"));
		assertEquals("Tower", testing.getHerd("Giraffe"));
		assertEquals("Herd", testing.getHerd("Elephant"));
		assertEquals("Murder", testing.getHerd("Crow"));
		assertEquals("Kit", testing.getHerd("Pigeon"));
		assertEquals("Pat", testing.getHerd("Flamingo"));
		assertEquals("Herd", testing.getHerd("Deer"));
		assertEquals("Pack", testing.getHerd("Dog"));
		assertEquals("Float", testing.getHerd("Crocodile"));
		assertEquals("Pride", testing.getHerd("Lion"));
	}
}
