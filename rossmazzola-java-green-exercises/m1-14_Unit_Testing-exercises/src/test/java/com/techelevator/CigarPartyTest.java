package com.techelevator;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class CigarPartyTest
{
	CigarParty testing;

	@Before
	public void setUp()
	{
		testing = new CigarParty();
	}

	@Test
	public void haveParty_ShouldReturnPartySuccessIsFalse_WhenCigarValueIsTooLowRegardlessOfWeekendStatusTest()
	{
		assertFalse(testing.haveParty(0, true));
		assertFalse(testing.haveParty(0, false));

		assertFalse(testing.haveParty(39, true));
		assertFalse(testing.haveParty(39, false));
	}

	@Test
	public void haveParty_ShouldReturnPartySuccessIsFalse_WhenCigarValueIsTooHighOnWeekdaysTest()
	{
		assertFalse(testing.haveParty(61, false));
	}

	@Test
	public void haveParty_ShouldReturnPartySuccessIsTrue_WhenCigarValueIsWithinRangeRegardlessOfWeekendStatusTest()
	{
		assertTrue(testing.haveParty(40, true));
		assertTrue(testing.haveParty(40, false));

		assertTrue(testing.haveParty(50, true));
		assertTrue(testing.haveParty(50, false));

		assertTrue(testing.haveParty(60, true));
		assertTrue(testing.haveParty(60, false));
	}

	@Test
	public void haveParty_ShouldReturnPartySuccessIsTrue_WhenWeekendIsTrueAndDueToWeekendsHavingNoUpperLimitOnCigarsTest()
	{
		assertTrue(testing.haveParty(61, true));
		assertTrue(testing.haveParty(2147483647, true));
	}
}
