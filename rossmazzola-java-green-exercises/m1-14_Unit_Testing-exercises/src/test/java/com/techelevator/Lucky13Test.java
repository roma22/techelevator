package com.techelevator;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class Lucky13Test
{
	Lucky13 testing;

	@Before
	public void setUp()
	{
		testing = new Lucky13();
	}

	@Test
	public void getLucky_ShouldReturnFalse_WhenOneIsPresentTest()
	{
		assertFalse(testing.getLucky(new int[] {1, 2}));
	}

	@Test

	public void getLucky_ShouldReturnFalse_WhenThreeIsPresentTest()
	{
		assertFalse(testing.getLucky(new int[] {2, 3}));
	}

	@Test
	public void getLucky_ShouldReturnTrue_WhenOneAndThreeAreNotPresentTest()
	{
		assertTrue(testing.getLucky(new int[] {0, 2, 4}));
	}
}
