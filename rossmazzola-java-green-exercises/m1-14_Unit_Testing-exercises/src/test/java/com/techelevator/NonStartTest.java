package com.techelevator;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class NonStartTest
{
	NonStart testing;

	@Before
	public void setUp()
	{
		testing = new NonStart();
	}

	@Test
	public void getPartialString_ShouldReturnConcatStringWithRemovedFirstChars_WhenValuesAreValidStringsTest()
	{
		assertEquals("", testing.getPartialString("a", "z"));
		assertEquals("bz", testing.getPartialString("ab", "yz"));
		assertEquals("bcyz", testing.getPartialString("abc", "xyz"));
		assertEquals("bcdxyz", testing.getPartialString("abcd", "wxyz"));
	}
}
