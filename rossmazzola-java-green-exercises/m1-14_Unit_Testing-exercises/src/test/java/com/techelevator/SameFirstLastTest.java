package com.techelevator;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class SameFirstLastTest
{
	SameFirstLast testing;

	@Before
	public void setUp() throws Exception
	{
		testing = new SameFirstLast();
	}

	@Test
	public void isItTheSame_ShouldReturnFalse_WhenArrayLengthIsZeroTest()
	{
		assertFalse(testing.isItTheSame(new int[0]));
	}

	@Test
	public void isItTheSame_ShouldReturnFalse_WhenFirstAndLastElementsAreDifferentAndArrayLengthIsTwoTest()
	{
		assertFalse(testing.isItTheSame(new int[] {-1, 1}));
	}

	@Test
	public void isItTheSame_ShouldReturnFalse_WhenFirstAndLastElementsAreDifferentAndArrayLengthIsGreaterThanTwoTest()
	{
		assertFalse(testing.isItTheSame(new int[] {-1, 0, 1}));
	}

	@Test
	public void isItTheSame_ShouldReturnFalse_WhenFirstAndLastElementsAreDifferentButAnyNonLastElementsMayBeEqualToFirstTest()
	{
		assertFalse(testing.isItTheSame(new int[] {1, 1, 0}));
	}

	@Test
	public void isItTheSame_ShouldReturnFalse_WhenFirstAndLastElementsAreDifferentButAnyNonFirstElementsMayBeEqualToLastTest()
	{
		assertFalse(testing.isItTheSame(new int[] {0, 1, 1}));
	}

	@Test
	public void isItTheSame_ShouldReturnTrue_WhenArrayLengthIsOneTest()
	{
		assertTrue(testing.isItTheSame(new int[] {-1}));
		assertTrue(testing.isItTheSame(new int[] {0}));
		assertTrue(testing.isItTheSame(new int[] {1}));
	}

	@Test
	public void isItTheSame_ShouldReturnTrue_WhenFirstAndLastElementsAreEqualAndArrayLengthIsTwoTest()
	{
		assertTrue(testing.isItTheSame(new int[] {-1, -1}));
		assertTrue(testing.isItTheSame(new int[] {0, 0}));
		assertTrue(testing.isItTheSame(new int[] {1, 1}));
	}

	@Test
	public void isItTheSame_ShouldReturnTrue_WhenFirstAndLastElementsAreEqualAndArrayLengthIsGreaterThanTwoTest()
	{
		assertTrue(testing.isItTheSame(new int[] {-1, 0, -1}));
		assertTrue(testing.isItTheSame(new int[] {0, 0, 0}));
		assertTrue(testing.isItTheSame(new int[] {1, 0, 1}));
	}
}
