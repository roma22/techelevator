package com.techelevator;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class FrontTimesTest
{
	FrontTimes testing;

	@Before
	public void setUp()
	{
		testing = new FrontTimes();
	}

	@Test
	public void generateString_ShouldReturnEmptyString_WhenValueLengthIsZeroTest()
	{
		assertEquals("", testing.generateString("", 0));
		assertEquals("", testing.generateString("", 1));
		assertEquals("", testing.generateString("", 999999));
	}

	@Test
	public void generateString_ShouldReturnStatedNumberOfCopies_WhenValueLengthIsLessThanThreeTest()
	{
		assertEquals("", testing.generateString("a", 0));
		assertEquals("a", testing.generateString("a", 1));
		assertEquals("aaaaaaaaaaaa", testing.generateString("a", 12));

		assertEquals("", testing.generateString("ab", 0));
		assertEquals("ab", testing.generateString("ab", 1));
		assertEquals("abababababab", testing.generateString("ab", 6));
	}

	@Test
	public void generateString_ShouldReturnStatedNumberOfCopies_WhenValueLengthIsExactlyThreeTest()
	{
		assertEquals("", testing.generateString("abc", 0));
		assertEquals("abc", testing.generateString("abc", 1));
		assertEquals("abcabcabcabc", testing.generateString("abc", 4));
	}
	
	@Test
	public void generateString_ShouldReturnStatedNumberOfCopies_WhenValueLengthIsMoreThanThreeTest()
	{
		assertEquals("", testing.generateString("abcd", 0));
		assertEquals("abc", testing.generateString("abcd", 1));
		assertEquals("abcabcabc", testing.generateString("abcd", 3));
	}
}
