package com.techelevator;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class Less20Test
{
	Less20 testing;

	@Before
	public void setUp()
	{
		testing = new Less20();
	}

	@Test
	public void isLessThanMultipleOf20_ShouldReturnFalse_WhenValueIsAnExactMultipleOf20Test()
	{
		assertFalse(testing.isLessThanMultipleOf20(40));
	}

	@Test
	public void isLessThanMultipleOf20_ShouldReturnFalse_WhenValueIsOneOrTwoGreaterThanMultipleOf20Test()
	{
		assertFalse(testing.isLessThanMultipleOf20(21));
		assertFalse(testing.isLessThanMultipleOf20(22));
	}

	@Test
	public void isLessThanMultipleOf20_ShouldReturnFalse_WhenValueIsThreeOrMoreLessThanMultipleOf20Test()
	{
		assertFalse(testing.isLessThanMultipleOf20(37));
		assertFalse(testing.isLessThanMultipleOf20(30));
	}

	@Test
	public void isLessThanMultipleOf20_ShouldReturnTrue_WhenValueIsOneOrTwoLessThanMultipleOf20Test()
	{
		assertTrue(testing.isLessThanMultipleOf20(39));
		assertTrue(testing.isLessThanMultipleOf20(38));
	}
}
