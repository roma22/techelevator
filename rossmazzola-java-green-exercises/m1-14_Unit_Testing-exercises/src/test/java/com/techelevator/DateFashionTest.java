package com.techelevator;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class DateFashionTest
{
	DateFashion testing;

	@Before
	public void setUp()
	{
		testing = new DateFashion();
	}

	@Test
	public void getATable_ShouldReturnFailure_WhenOnePartyIsTwoOrLessRegardlessOfOtherPartyRatingTest()
	{
		assertEquals(0, testing.getATable(2, 8));
		assertEquals(0, testing.getATable(8, 2));
		assertEquals(0, testing.getATable(1, 8));
		assertEquals(0, testing.getATable(8, 1));
	}

	@Test
	public void getATable_ShouldReturnMaybe_WhenNeitherPartyIsLessThanThreeOrGreaterThanSevenTest()
	{
		assertEquals(1, testing.getATable(3, 7));
		assertEquals(1, testing.getATable(7, 3));
		assertEquals(1, testing.getATable(5, 5));
	}

	@Test
	public void getATable_ShouldReturnSuccess_WhenOnePartyIsEightOrMoreAndOtherPartyIsMoreThanTwoTest()
	{
		assertEquals(2, testing.getATable(3, 8));
		assertEquals(2, testing.getATable(8, 3));
		assertEquals(2, testing.getATable(3, 9));
		assertEquals(2, testing.getATable(9, 3));
	}
}
