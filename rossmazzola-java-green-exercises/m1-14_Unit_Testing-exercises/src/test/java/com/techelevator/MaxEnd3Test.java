package com.techelevator;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class MaxEnd3Test
{
	MaxEnd3 testing;

	@Before
	public void setUp()
	{
		testing = new MaxEnd3();
	}

	@Test
	public void makeArray_ShouldReturnArrayWithOnlyGreaterElement_WhenCheckingIfFirstOrLastElementIsGreaterSimpleCaseTest()
	{
		assertArrayEquals(new int[] {3, 3, 3}, testing.makeArray(new int[] {1, 2, 3}));
	}

	@Test
	public void makeArray_ShouldReturnArrayWithOnlyGreaterElement_WhenCheckingIfFirstOrLastElementIsGreaterAndCanHandleEqualValuesTest()
	{
		assertArrayEquals(new int[] {1, 1, 1}, testing.makeArray(new int[] {1, 2, 1}));
	}

	@Test
	public void makeArray_ShouldReturnArrayWithOnlyGreaterElement_WhenCheckingIfFirstOrLastElementIsGreaterAndCanHandleNegativeValuesTest()
	{
		assertArrayEquals(new int[] {-1, -1, -1}, testing.makeArray(new int[] {-1, -2, -3}));
	}
}
