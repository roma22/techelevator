package com.techelevator;

public interface IVehicle
{
	String getType();

	double calculateToll(int distance);
}
