package com.techelevator.postage;

public class ThirdClass extends PostalService
{
	private String type = "Postal Service (3rd Class)";
	private double[] ratePerMile =
	{ 0.0020, 0.0022, 0.0024, 0.0150, 0.0160, 0.0170 };

	public double getRatePerMile(int x)
	{
		return ratePerMile[x];
	}

	@Override
	public String getType()
	{
		return type;
	}

	@Override
	public double calculateRate(int distance, double weight)
	{
		return super.calculateRate(distance, weight);
	}
}
