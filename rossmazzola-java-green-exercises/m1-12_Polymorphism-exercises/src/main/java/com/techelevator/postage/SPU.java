package com.techelevator.postage;

import java.util.HashMap;
import java.util.Map;

public class SPU implements DeliveryDriver
{
	private String type = "SPU";
	private String classSPU = "SPU";

	public SPU(String classSPU)
	{
		this.classSPU = classSPU;
	}

	@Override
	public String getType()
	{
		type = "SPU (" + classSPU + ")";
		return type;
	}

	@Override
	public double calculateRate(int distance, double weight)
	{
		Map<String, Double> rateTable = new HashMap<String, Double>();
		
		rateTable.put("4-Day Ground", 0.0050);
		rateTable.put("2-Day Business", 0.050);
		rateTable.put("Next Day", 0.075);

		return ((weight / 16) * rateTable.get(classSPU)) * distance;
	}
}
