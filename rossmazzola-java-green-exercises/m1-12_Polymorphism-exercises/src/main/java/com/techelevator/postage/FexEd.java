package com.techelevator.postage;

public class FexEd implements DeliveryDriver
{
	private String type = "FexEd";

	@Override
	public String getType()
	{
		return type;
	}

	@Override
	public double calculateRate(int distance, double weight)
	{
		double rate = 20.00;

		if (distance > 500)
		{
			rate += 5.00;
		}
		if (weight > 48)
		{
			rate += 3.00;
		}
		return rate;
	}
}
