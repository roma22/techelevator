package com.techelevator.postage;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class PostageCalculator
{
	public static void main(String[] args)
	{
		try (Scanner scans = new Scanner(System.in))
		{
			List<DeliveryDriver> dList = new ArrayList<DeliveryDriver>();

			FirstClass firstC = new FirstClass();
			SecondClass secondC = new SecondClass();
			ThirdClass thirdC = new ThirdClass();
			FexEd fex = new FexEd();
			SPU sup = new SPU("4-Day Ground");
			SPU sup2 = new SPU("2-Day Business");
			SPU sup3 = new SPU("Next Day");

			dList.add(firstC);
			dList.add(secondC);
			dList.add(thirdC);
			dList.add(fex);
			dList.add(sup);
			dList.add(sup2);
			dList.add(sup3);

			String inputWeight = "0";
			String inputPoundsOrOunces = null;
			String inputDistance = "0";

			boolean shouldWeLoop = true;

			while (shouldWeLoop)
			{
				System.out.print("Please enter the weight of the package? ");
				inputWeight = scans.nextLine();

				if (!inputWeight.matches("(\\d+)"))
				{
					System.out.println("Whole numbers only, please.");
					continue;
				}

				System.out.print("(P)ounds or (O)unces? ");
				inputPoundsOrOunces = scans.nextLine();

				if (!inputPoundsOrOunces.equalsIgnoreCase("P") && !inputPoundsOrOunces.equalsIgnoreCase("O"))
				{
					System.out.println("P or O only, please.");
					continue;
				}

				System.out.print("What distance will it be traveling? ");
				inputDistance = scans.nextLine();

				if (!inputDistance.matches("(\\d+)"))
				{
					System.out.println("Whole numbers only, please.");
					continue;
				}
				break;
			}

			int ouncesWeight = Integer.parseInt(inputWeight);
			int distance = Integer.parseInt(inputDistance);

			if (inputPoundsOrOunces.equalsIgnoreCase("P"))
			{
				ouncesWeight *= 16;
			}

			System.out.println("");
			System.out.format("%-33s %s %s", "Delivery Method", "$ cost", "\n");
			System.out.println("----------------------------------------");

			for (DeliveryDriver var : dList)
			{
				BigDecimal cost = new BigDecimal(var.calculateRate(distance, ouncesWeight)).setScale(2, BigDecimal.ROUND_HALF_DOWN);

				System.out.format("%-33s %s %s", var.getType(), "$" + cost, "\n");
			}
		}
	}
}
