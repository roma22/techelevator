package com.techelevator.postage;

abstract class PostalService implements DeliveryDriver
{
	private String type = "Postal Service";
	private double[] ratePerMile = new double[6];

	public double getRatePerMile(int x)
	{
		return ratePerMile[x];
	}

	@Override
	public String getType()
	{
		return type;
	}

	@Override
	public double calculateRate(int distance, double weight)
	{
		double rate = 0.00;

		if (weight >= 0 && weight <= 2)
		{
			rate = getRatePerMile(0);
		}
		else if (weight >= 3 && weight <= 8)
		{
			rate = getRatePerMile(1);
		}
		else if (weight >= 9 && weight <= 15)
		{
			rate = getRatePerMile(2);
		}
		else if (weight >= 16 && weight <= 48)
		{
			rate = getRatePerMile(3);
		}
		else if (weight >= 49 && weight <= 143)
		{
			rate = getRatePerMile(4);
		}
		else if (weight >= 144)
		{
			rate = getRatePerMile(5);
		}
		return rate * distance;
	}
}
