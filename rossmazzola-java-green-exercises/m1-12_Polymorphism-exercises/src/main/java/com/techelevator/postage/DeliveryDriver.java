package com.techelevator.postage;

public interface DeliveryDriver
{
	String getType();

	double calculateRate(int distance, double weight);
}
