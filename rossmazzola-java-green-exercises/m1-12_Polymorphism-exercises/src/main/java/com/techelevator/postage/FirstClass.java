package com.techelevator.postage;

public class FirstClass extends PostalService
{
	private String type = "Postal Service (1st Class)";
	private double[] ratePerMile =
	{ 0.035, 0.040, 0.047, 0.195, 0.450, 0.500 };

	public double getRatePerMile(int x)
	{
		return ratePerMile[x];
	}

	@Override
	public String getType()
	{
		return type;
	}

	@Override
	public double calculateRate(int distance, double weight)
	{
		return super.calculateRate(distance, weight);
	}
}
