package com.techelevator.postage;

public class SecondClass extends PostalService
{
	private String type = "Postal Service (2nd Class)";
	private double[] ratePerMile =
	{ 0.0035, 0.0040, 0.0047, 0.0195, 0.0450, 0.0500 };

	public double getRatePerMile(int x)
	{
		return ratePerMile[x];
	}

	@Override
	public String getType()
	{
		return type;
	}

	@Override
	public double calculateRate(int distance, double weight)
	{
		return super.calculateRate(distance, weight);
	}
}
