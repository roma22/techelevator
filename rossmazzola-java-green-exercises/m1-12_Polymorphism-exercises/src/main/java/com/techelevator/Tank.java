package com.techelevator;

public class Tank implements IVehicle
{
	private String type = "Tank";

	public Tank()
	{

	}

	@Override
	public String getType()
	{
		return type;
	}

	@Override
	public double calculateToll(int distance)
	{
		return 0;
	}
}
