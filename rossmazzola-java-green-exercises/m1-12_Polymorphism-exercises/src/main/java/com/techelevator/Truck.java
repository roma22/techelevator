package com.techelevator;

public class Truck implements IVehicle
{
	private String type = "Truck";
	private int numberOfAxles;

	public Truck(int numberOfAxles)
	{
		this.numberOfAxles = numberOfAxles;
	}

	public int getNumberOfAxles()
	{
		return numberOfAxles;
	}

	@Override
	public String getType()
	{
		type = "Truck (" + numberOfAxles + " axles)";
		return type;
	}

	@Override
	public double calculateToll(int distance)
	{
		double toll = 0.00;

		if (numberOfAxles == 4)
		{
			toll = 0.040;
		}
		else if (numberOfAxles == 6)
		{
			toll = 0.045;
		}
		else if (numberOfAxles >= 8)
		{
			toll = 0.048;
		}
		return distance * toll;
	}
}
