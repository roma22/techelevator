package com.techelevator;

import java.util.Random;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class TollCalculator
{
	public static void main(String[] args)
	{
		List<IVehicle> vList = new ArrayList<IVehicle>();

		Random rand = new Random();

		Car myCar = new Car(false);
		Car myCar2 = new Car(true);
		Tank myTank = new Tank();
		Truck myTruck = new Truck(4);
		Truck myTruck2 = new Truck(6);
		Truck myTruck3 = new Truck(8);

		vList.add(myCar);
		vList.add(myCar2);
		vList.add(myTank);
		vList.add(myTruck);
		vList.add(myTruck2);
		vList.add(myTruck3);

		System.out.format("%-18s %-22s %s %s", "Vehicle", "Distance Traveled", "Toll $", "\n");
		System.out.println("------------------------------------------------");

		int totalDistance = 0;
		BigDecimal totalToll = new BigDecimal("0");

		for (IVehicle var : vList)
		{
			int distance = rand.nextInt(240) + 10;
			totalDistance += distance;

			BigDecimal toll = new BigDecimal(var.calculateToll(distance)).setScale(2, BigDecimal.ROUND_HALF_UP);
			totalToll = totalToll.add(toll);

			System.out.format("%-18s %-22s %s %s", var.getType(), distance, "$" + toll, "\n");
		}
		System.out.println("\nTotal Miles Traveled: " + totalDistance);
		System.out.println("Total Tollbooth Revenue: " + "$" + totalToll);
	}
}
