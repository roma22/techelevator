<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
	<head>
		<title>Exercise 1 - FizzBuzz</title>
		<style>
			li {
				list-style-type: none;
			}
			
			.fizz {
				color: blue;
			}
			
			.buzz {
				color: red;
			}
			
			.fizzbuzz {
				color: purple;
				font-size: 150%;
			}
		</style>
	</head>
	<body>
		<h1>Exercise 1 - FizzBuzz</h1>
		<ul>
			<c:forEach var="i" begin="1" end="100">
				<c:choose>
					<c:when test="${ i % 15 == 0 }">
						<c:set var="result" value="FizzBuzz!"/>
						<c:set var="classType" value="fizzbuzz"/>
					</c:when>
					<c:when test="${ i % 3 == 0 }">
						<c:set var="result" value="Fizz!"/>
						<c:set var="classType" value="fizz"/>
					</c:when>
					<c:when test="${ i % 5 == 0 }">
						<c:set var="result" value="Buzz!"/>
						<c:set var="classType" value="buzz"/>
					</c:when>
					<c:otherwise>
						<c:set var="result" value="${ i }"/>
						<c:set var="classType" value=""/>
					</c:otherwise>
				</c:choose>
				<li class="${ classType }"><c:out value="${ result }"/></li>
			</c:forEach>
			<%--
				Add a list item (i.e. <li>) containing each of the numbers from 1 to 100.
				
				if the number is divisible by 3, show "Fizz!" instead and style the item using the "fizz" class
				
				if the number is divisible by 5, show "Buzz!" instead and style the item using the "buzz" class
				
				if the number is divisible by both 3 and 5, show "FizzBuzz!" instead  and style the item using the "fizzbuzz" class
				
				see exercise1-fizzbuzz.png for example output
			 --%>
		</ul>
	</body>
</html>