<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
	<head>
		<title>Exercise 2 - Fibonacci 25</title>
		<style>
			li {
				list-style-type: none;
			}
		</style>
	</head>
	<body>
		<h1>Exercise 2 - Fibonacci 25</h1>
			<c:set var="nextNum" value="1"/>
		<ul>
			<c:forEach var="i" begin="1" end="25">
				<c:set var="fibNum" value="${ fibNum + oldNum }"/>
				<c:if test="${ i == 1 }">
					<c:set var="fibNum" value="${ nextNum }"/>
				</c:if>
				<li>
					<c:out value="${ fibNum }"/>
				</li>
				<c:set var="oldNum" value="${ nextNum }"/>
				<c:set var="nextNum" value="${ fibNum }"/>
			</c:forEach>
			<%--
				Add a list item (i.e. <li>) for each of the first 25 numbers in the Fibonacci sequence
				
				See exercise2-fibonacci.png for example output
			 --%>
		</ul>
	</body>
</html>