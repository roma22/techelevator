<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
	<head>
		<title>Exercise 3 - Echo</title>
		<style>
			li {
				list-style-type: none;
			}
		</style>
	</head>
	<body>
		<h1>Exercise 3 - Echo</h1>
		<c:set var="words" value="${param.word}"/>
		<c:set var="counts" value="${param.count}"/>
		<c:set var="size" value="${ counts }"/>
		<ul>
			<c:forEach var="i" begin="1" end="${ counts }">
				<div style="font-size: ${ size }px;">
					<c:out value="${ words }"/><br>
				</div>
				<c:set var="size" value="${ size - 1 }"/>
			</c:forEach>
		<%--
			Given two query string parameters, "word" and "count":
			
			Add a number of list items equal to "count".  Each list item should contain the value passed in "word".
			
			The font size of the first list item should be equal to "count".  The font size of each subsequent list
			item should be decreased by 1.
			 
			See exercise3-echo.png for example output
		 --%>
		</ul>
		
	</body>
</html>