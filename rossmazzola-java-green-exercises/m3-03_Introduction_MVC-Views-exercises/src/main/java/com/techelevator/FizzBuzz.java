package com.techelevator;

public class FizzBuzz
{
	public String[] fizzBuzz()
	{
		String[] result = new String[100];

		for (Integer i = 1; i <= 100; i++)
		{
			if (i % 15 == 0)
			{
				result[i - 1] = "FizzBuzz";
				continue;
			}
			if (i % 3 == 0)
			{
				result[i - 1] = "Fizz";
			}
			else if (i % 5 == 0)
			{
				result[i - 1] = "Buzz";
			}
			else
			{
				result[i - 1] = i.toString();
			}
		}
		return result;
	}
}
