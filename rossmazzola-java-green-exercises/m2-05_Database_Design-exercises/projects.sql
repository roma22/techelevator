BEGIN TRANSACTION;

DROP TABLE IF EXISTS project_employee;
DROP TABLE IF EXISTS employees;
DROP TABLE IF EXISTS job_titles;
DROP TABLE IF EXISTS projects;
DROP TABLE IF EXISTS departments;

CREATE TABLE departments
(
        department_id serial primary key,
        name varchar(100) not null
);

CREATE TABLE projects
(
        project_id serial primary key,
        name varchar(100) not null,
        start_date date
);

CREATE TABLE job_titles
(
        job_title_id serial primary key,
        name varchar(100) not null
);

CREATE TABLE employees
(
        employee_id serial primary key,
        last_name varchar(100) not null,
        first_name varchar(100) not null,
        gender char(1) not null,
        birth_date date not null,
        hire_date date not null,
        department_id int not null,
        job_title_id int not null,
        
        constraint fk_employees_department_id foreign key (department_id) references departments (department_id),
        constraint fk_employees_job_title_id foreign key (job_title_id) references job_titles (job_title_id),
        constraint chk_gender check (gender IN ('M', 'F', 'O'))
);

CREATE TABLE project_employee
(
        project_employee_id serial primary key,
        project_id int not null,
        employee_id int not null,
        
        constraint fk_project_employee_project_id foreign key (project_id) references projects (project_id),
        constraint fk_project_employee_employee_id foreign key (employee_id) references employees (employee_id)
);

COMMIT TRANSACTION;

--

INSERT INTO departments (name)
VALUES ('Old Department');

INSERT INTO departments (name)
VALUES ('Best Department');

INSERT INTO departments (name)
VALUES ('New Department');

--

INSERT INTO projects (name, start_date)
VALUES ('Old Project', '1999-01-02');

INSERT INTO projects (name, start_date)
VALUES ('Cool Project', '2009-01-02');

INSERT INTO projects (name, start_date)
VALUES ('Fun Project', '2018-01-02');

INSERT INTO projects (name, start_date)
VALUES ('New Project', '2019-01-02');

--

INSERT INTO job_titles (name)
VALUES ('Manager');

INSERT INTO job_titles (name)
VALUES ('Analyst');

INSERT INTO job_titles (name)
VALUES ('Intern');

--

INSERT INTO employees (job_title_id, last_name, first_name, gender, birth_date, hire_date, department_id)
VALUES (1, 'Bat', 'Alice', 'F', '1979-01-01', '1999-01-01', 1);

INSERT INTO employees (job_title_id, last_name, first_name, gender, birth_date, hire_date, department_id)
VALUES (1, 'Cat', 'Bob', 'M', '1979-02-01', '1999-01-01', 2);

INSERT INTO employees (job_title_id, last_name, first_name, gender, birth_date, hire_date, department_id)
VALUES (1, 'Dat', 'Charlie', 'F', '1979-03-01', '1999-01-01', 3);

INSERT INTO employees (job_title_id, last_name, first_name, gender, birth_date, hire_date, department_id)
VALUES (2, 'Eat', 'Django', 'M', '1989-01-01', '2009-01-01', 1);

INSERT INTO employees (job_title_id, last_name, first_name, gender, birth_date, hire_date, department_id)
VALUES (2, 'Fat', 'Eliza', 'F', '1989-02-02', '2009-01-01', 2);

INSERT INTO employees (job_title_id, last_name, first_name, gender, birth_date, hire_date, department_id)
VALUES (2, 'Gat', 'Frank', 'M', '1989-03-03', '2009-01-01', 3);

INSERT INTO employees (job_title_id, last_name, first_name, gender, birth_date, hire_date, department_id)
VALUES (3, 'Hat', 'George', 'M', '1999-04-01', '2019-01-01', 1);

INSERT INTO employees (job_title_id, last_name, first_name, gender, birth_date, hire_date, department_id)
VALUES (3, 'Eat', 'Hayden', 'O', '1999-04-01', '2019-01-01', 2);

--

INSERT INTO project_employee (project_id, employee_id)
VALUES (1, 1);

INSERT INTO project_employee (project_id, employee_id)
VALUES (1, 4);

INSERT INTO project_employee (project_id, employee_id)
VALUES (1, 7);

INSERT INTO project_employee (project_id, employee_id)
VALUES (2, 2);

INSERT INTO project_employee (project_id, employee_id)
VALUES (2, 5);

INSERT INTO project_employee (project_id, employee_id)
VALUES (2, 8);

INSERT INTO project_employee (project_id, employee_id)
VALUES (3, 2);

INSERT INTO project_employee (project_id, employee_id)
VALUES (3, 5);

INSERT INTO project_employee (project_id, employee_id)
VALUES (4, 3);

INSERT INTO project_employee (project_id, employee_id)
VALUES (4, 6);

--

SELECT employees.last_name, employees.first_name, employees.gender, employees.birth_date, employees.hire_date,
        job_titles.name AS job_title, projects.name AS project, projects.start_date AS start_date, departments.name AS department
FROM employees
JOIN job_titles ON job_titles.job_title_id = employees.job_title_id
JOIN project_employee ON project_employee.employee_id = employees.employee_id
JOIN projects ON projects.project_id = project_employee.project_id
JOIN departments ON departments.department_id = employees.department_id;
