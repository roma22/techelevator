<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="common/header.jspf" %>
<section id="main-content">

    <h1>Toy Department</h1>

    <div class="producttable">
	<div id="rowhead">
	    <ul>
		<li>Name</li>
		<li>Rating</li>
		<li>Mfr</li>
		<li>Price</li>
		<li>wt. (in lbs)</li>
	    </ul>
	</div>
	<c:forEach var="product" items="${ productList }">
	    <div id="product">
		<div id="status">
		    <c:url var="imgUrl" value="/img/${ product.imageName }" />
		    <a href="productDetail?productId=${ product.productId }">
			<img src="${ imgUrl }" />
		    </a>
		
		    <c:if test="${ product.topSeller }">
			<div id="bestseller">
			    <c:out value="BEST SELLER!" />
			</div>
		    </c:if>
		</div>

		<div id="info">
		    <div id="name">
			<c:out value="${ product.name }" />
		    </div>

		    <fmt:formatNumber var="rating" value="${ product.averageRating }" maxFractionDigits="0" />
		    <c:url var="ratingUrl" value="/img/${ rating }-star.png" />
		    <div id="rating">
			<img src="${ ratingUrl }" />
		    </div>

		    <div id="manufacturer">
			<c:out value="${ product.manufacturer }" />
		    </div>

		    <fmt:formatNumber var="price" value="${ product.price }" type="currency" />
		    <div id="price">
			<c:out value="${ price }" />
		    </div>

		    <fmt:formatNumber var="weight" value="${ product.weightInLbs }" maxFractionDigits="1" />
		    <div id="weight">
			<c:out value="${ weight }" />
		    </div>
		</div>
	    </div>
	</c:forEach>
    </div>
</section>
<%@ include file="common/footer.jspf" %>