package com.techelevator;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class FizzWriter
{
	public static void main(String[] args) throws IOException
	{
//		try
//		{
//			FileWriter fizzBuzzFile = new FileWriter("FizzBuzz.txt");
//			PrintWriter fizzBuzzPrint = new PrintWriter(fizzBuzzFile);
//			fizzBuzzPrint.print(fizzBuzz());
//			fizzBuzzPrint.close();
//		}
//		catch (Exception e)
//		{
			System.out.println(fizzBuzz());
//		}
	}

	public static String fizzBuzz()
	{
		String[] result = new String[100];

		for (Integer i = 1; i <= 100; i++)
		{
			if (i % 15 == 0)
			{
				result[i - 1] = "FizzBuzz";
			}
			if (i % 3 == 0)
			{
				result[i - 1] = "Fizz";
			}
			else if (i % 5 == 0)
			{
				result[i - 1] = "Buzz";
			}
			else
			{
				result[i - 1] = i.toString();
			}
		}
		String output = String.join("\n", result);
		return output;
	}
}
