package com.techelevator;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.techelevator.model.Review;
import com.techelevator.model.ReviewDao;

@Controller
public class ReviewController
{
	@Autowired
	private ReviewDao reviewDao;

	@RequestMapping("/createReview")
	public String displayCreateReview(HttpSession session)
	{
		return "createReview";
	}

	@RequestMapping(path="/greeting", method=RequestMethod.POST)
	public String postReviewAndRedirect(@ModelAttribute Review post)
	{
		reviewDao.save(post);
		return "redirect:/greeting";
	}
}
