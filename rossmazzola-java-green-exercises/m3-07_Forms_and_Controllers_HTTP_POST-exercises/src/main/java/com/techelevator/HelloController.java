package com.techelevator;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.techelevator.model.ReviewDao;

@Controller
public class HelloController
{
	@Autowired
	private ReviewDao reviewDao;
	
	@RequestMapping("/")
	public String displayGreeting(HttpServletRequest request)
	{
		request.setAttribute("reviews", reviewDao.getAllReviews());
		return "greeting";
	}
	
	@RequestMapping(path="/greeting", method=RequestMethod.GET)
	public String redirectGreeting(HttpServletRequest request)
	{
		request.setAttribute("reviews", reviewDao.getAllReviews());
		return "greeting";
	}
}
