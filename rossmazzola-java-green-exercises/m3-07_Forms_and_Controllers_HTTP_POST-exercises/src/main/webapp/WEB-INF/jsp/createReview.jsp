<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:set var="pageTitle" value="Leave A Review" />

<%@include file="common/header.jspf"%>
<div class="createReview">

	<h1>Write a Review</h1>
	<c:url var="imgStar" value="img/star.png" />

	<c:url value="/greeting" var="formAction" />
	<form method="POST" action="${ formAction }">
		<div id="group">
			<label for="username">Your Name:</label> <input type="text" name="username" id="username" />
		</div>

		<div id="group">
			<label for="title">Review Title:</label> <input type="text" name="title" id="title" />
		</div>

		<div id="group">

			<label for="rating">Rating:</label>
			<div id="stars">
				<c:forEach var="starRating" begin="1" end="5">
					<div id="star">
						<img src="${ imgStar }"> <input type="radio" name="rating" value="${ starRating }">
						<div id="starValues">
							<c:out value="${ starRating }" />
						</div>
					</div>
				</c:forEach>
			</div>
		</div>

		<div id="group">
			<div id="review">
				<label for="text">Review:</label>
				<textarea rows="9" cols="80" name="text" id="text"></textarea>
			</div>
		</div>

		<input type="submit" value="Submit" id="button" />
	</form>
</div>
<%@include file="common/footer.jspf"%>