<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<c:set var="pageTitle" value="Greetings!" />
<%@include file="common/header.jspf"%>
<div class="greeting">
	<h1>Squirrel Cigar Parties</h1>
	<hr />
	<c:url var="imgCover" value="img/forDummies.png" />
	<img src="${ imgCover }">
	<p id="copy">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et
		dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
		consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
		Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

	<h2>Reviews</h2>
	<c:url var="imgStar" value="img/star.png" />
	<hr />
	<div id="reviews">
		<c:forEach var="review" items="${ reviews }">
			<div id="review">
				<div id="heading">
					<div id="title">
						<c:out value="${ review.title }" />
					</div>

					<div id="username">
						<c:out value="(${ review.username })" />
					</div>
				</div>

				<div id="date">
					<fmt:parseDate var="parseDate" value="${ review.dateSubmitted }" type="both" pattern="yyyy-MM-dd'T'HH:mm" />
					<fmt:formatDate var="formatDate" value="${ parseDate }" pattern="M/d/yyyy" />
					<c:out value="${ formatDate }" />
				</div>

				<div id="rating">
					<c:forEach var="star" begin="1" end="${ review.rating }">
						<img src="${ imgStar }">
					</c:forEach>
				</div>

				<div id="text">
					<c:out value="${ review.text }" />
				</div>
			</div>
		</c:forEach>
	</div>
</div>
<%@include file="common/footer.jspf"%>