// read the tasks from data.json
fetch('data.json')
    .then((response) =>
    {
        // get the JSON from the response
        return response.json();
    })
    .then((todos) =>
    {
        // when the JSON data is returned log the result
        console.log(todos);
        // Test to see if the browser supports the HTML template element by checking
        // for the presence of the template element's content attribute.
        if ('content' in document.createElement('template'))
        {
            const list = document.querySelector("ul");
            todos.forEach((todo) =>
            {
                // each todo in our array
                const tmpl = document.getElementById('tasks').content.cloneNode(true);
                tmpl.querySelector("li").insertAdjacentHTML('afterbegin', todo.task);
                if (todo.completed)
                {
                    const circleCheck = tmpl.querySelector('.fa-check-circle');
                    circleCheck.className += " completed";
                }
                list.appendChild(tmpl);
            });
        }
        else
        {
            // Find another way to add list items to your list 
            // the HTML template element is not supported.
            console.error('Your browser does not support templates');
        }
    })
    .catch((err) => { console.error(err) });
