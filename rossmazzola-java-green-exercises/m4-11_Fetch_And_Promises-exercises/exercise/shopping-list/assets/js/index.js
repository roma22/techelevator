let items = [];

function loadItems()
{
    // read the tasks from data.json
    fetch('assets/data/shopping-list.json')
        .then((response) =>
        {
            // get the JSON from the response
            return response.json();
        })
        .then((data) =>
        {
            items = data;
            displayItems();
        })
        .catch((err) => { console.error(err) });
}
function displayItems()
{
    // when the JSON data is returned log the result
    console.log(items);
    // Test to see if the browser supports the HTML template element by checking
    // for the presence of the template element's content attribute.
    if ('content' in document.createElement('template'))
    {
        const list = document.querySelector("ul");
        items.forEach((item) =>
        {
            // each item in our array
            const tmpl = document.getElementById('shopping-list-item-template').content.cloneNode(true);
            tmpl.querySelector("li").insertAdjacentHTML('afterbegin', item.name);
            if (item.completed)
            {
                const circleCheck = tmpl.querySelector('.fa-check-circle');
                circleCheck.className += " completed";
            }
            list.appendChild(tmpl);
        });
    }
    else
    {
        // Find another way to add list items to your list 
        // the HTML template element is not supported.
        console.error('Your browser does not support templates');
    }
}

const button = document.querySelector(".loadingButton");
button.addEventListener("click", callLoadItems);

function callLoadItems()
{
    if (items == 0)
    {
        loadItems();
        clicked = true;
    }
    if (clicked)
    {
        button.addEventListener("click", magicWord);
        clicked = false;
    }
}

function magicWord()
{
    document.querySelector(".shopping-list").insertAdjacentHTML('beforebegin', '<div class="magicWord">' +
        '<iframe width="560" height="315" src="https://www.youtube.com/embed/fmz-K2hLwSI?autoplay=1&enable_js=1" ' +
        'frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" ' +
        'allowfullscreen></iframe></div>');
    button.removeEventListener("click", magicWord);
}
