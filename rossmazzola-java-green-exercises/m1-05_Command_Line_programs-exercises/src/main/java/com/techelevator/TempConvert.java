package com.techelevator;

import java.util.Scanner;

public class TempConvert {

	public static void main(String[] args) {

		try (Scanner scans = new Scanner(System.in))
		{
			boolean shouldWeLoop = true;
			
			while (shouldWeLoop)
			{
				System.out.print("Please enter the temperature: ");
				String inputTemperature = scans.nextLine();
				
				if(!inputTemperature.matches("^-?\\d+$"))
				{
					System.out.println("Integers only, please.");
					continue;
				}
				
				int temperature = Integer.parseInt(inputTemperature);
				
				System.out.print("Is the temperature in (C)elsius or (F)ahrenheit: ");
				String unitOfMeasurment = scans.nextLine();
				
				if (unitOfMeasurment.equalsIgnoreCase("F"))
					// convert Fahrehneit to Celsius
				{
					double tempCelsius = (temperature - 32) / 1.8;
					long celsiusRound = Math.round(tempCelsius);
					System.out.println(temperature + "F is " + celsiusRound + "C");
				}
				else if (unitOfMeasurment.equalsIgnoreCase("C"))
					// convert Celsius to Fahrenheit
				{
					double tempFahrenheit = (temperature * 1.8) + 32;
					long fahrenheitRound = Math.round(tempFahrenheit);
					System.out.println(temperature + "C is " + fahrenheitRound + "F");
				}
				else
				{
					System.out.println("Please try again.");
					continue;
				}
				
				System.out.print("Do you want to continue? (Y)es or (N)o: ");
				String answer = scans.nextLine();

				if (answer.equalsIgnoreCase("N"))
				{
					shouldWeLoop = false;
				}
				else if (!answer.equalsIgnoreCase("Y"))
				{
					System.out.println("Please try again.");
					continue;
				}
			}
			System.out.println("Thanks for using this program.");
		}
	}

}
