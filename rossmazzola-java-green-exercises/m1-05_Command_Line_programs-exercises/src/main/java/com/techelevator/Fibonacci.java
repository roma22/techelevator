
package com.techelevator;

import java.util.Scanner;

public class Fibonacci {

	public static void main(String[] args) {
		
		try(Scanner scans = new Scanner(System.in))
		{
			boolean shouldWeLoop = true;
			
			while (shouldWeLoop)
			{
				System.out.print("Please enter the Fibonacci number: ");
				String inputFibNum = scans.nextLine();
				
				if (!inputFibNum.matches("^\\d+$"))
				{
					System.out.println("Whole numbers only, please.");
					continue;
				}
				
				int valueFibNum = Integer.parseInt(inputFibNum);
				int oldFibNum = 0;
				int nextFibNum = 0;
				
				for (int i = 0; i < valueFibNum; i += oldFibNum)
				{
					System.out.print((i == 0 ? "" : ", ") + i);

					oldFibNum = nextFibNum;
					nextFibNum = i;
					
					if (i == 0)
					{
						i++;
					}
				}
				
				System.out.print("\nDo you want to continue? (Y)es or (N)o: ");
				String answer = scans.nextLine();
				
				if (answer.equalsIgnoreCase("N"))
				{
					shouldWeLoop = false;
				}
				else if (!answer.equalsIgnoreCase("Y"))
				{
					System.out.println("Please try again.");
					continue;
				}

			}
			System.out.println("Thanks for using this program.");
		}
		
	}

}
