
package com.techelevator;

import java.util.Scanner;

public class DecimalToBinary {

	public static void main(String[] args) {
		
		try (Scanner scans = new Scanner(System.in))
		{
			boolean shouldWeLoop = true;
			
			while (shouldWeLoop)
			{
				System.out.print("Please enter in a series of decimal values (separated by spaces): ");
				String inputDecimal = scans.nextLine();
				
				if (!inputDecimal.matches("^(\\d+\\s*)+$"))
				{
					System.out.println("Whole numbers only, please.");
					continue;
				}
				
				String[] storeInputDecimal = inputDecimal.split("\\s+");
				
				for (int i = 0; i < storeInputDecimal.length; i++)
				{
					long outputDecimal = Long.parseLong(storeInputDecimal[i]);
					
					String outputBinary = DecToBin.myBinaryString(outputDecimal);
					
					System.out.println(storeInputDecimal[i] + " in binary is " + outputBinary);
				}
				
				System.out.print("Do you want to continue? (Y)es or (N)o: ");
				String answer = scans.nextLine();
				
				if (answer.equalsIgnoreCase("N"))
				{
					shouldWeLoop = false;
				}
				else if (!answer.equalsIgnoreCase("Y"))
				{
					System.out.println("Please try again.");
					continue;
				}
			}
			System.out.println("Thanks for using this program.");
		}
	}

}

class DecToBin
{
	public static String myBinaryString(long valueDecimal)
	{
		String valueBinary = "";
		long remainder;
		
		while (valueDecimal != 0)
		{
			remainder = valueDecimal % 2;
			valueDecimal /= 2;
			
			if (remainder == 1)
			{
				valueBinary = "1" + valueBinary;
			}
			else
			{
				valueBinary = "0" + valueBinary;
			}
		}
		return valueBinary;
	}
}
