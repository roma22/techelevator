package com.techelevator;

import java.util.Scanner;

public class LinearConvert {

	public static void main(String[] args) {

		try (Scanner scans = new Scanner(System.in))
		{
			boolean shouldWeLoop = true;
			
			while (shouldWeLoop)
			{
				System.out.print("Please enter the length: ");
				String inputDistance = scans.nextLine();
				
				if(!inputDistance.matches("^\\d+$"))
				{
					System.out.println("Whole numbers only, please.");
					continue;
				}
				
				int distance = Integer.parseInt(inputDistance);
				
				System.out.print("Is the measurement in (m)eters or (f)eet: ");
				String unitOfMeasurment = scans.nextLine();
				
				if (unitOfMeasurment.equalsIgnoreCase("F"))
					// convert feet to meters
				{
					double distMeters = distance * 0.3048F;
					long metersRound = Math.round(distMeters);
					System.out.println(distance + "f is " + metersRound + "m");
				}
				else if (unitOfMeasurment.equalsIgnoreCase("M"))
					// convert meters to feet
				{
					double distFeet = distance * 3.280399F;
					long feetRound = Math.round(distFeet);
					System.out.println(distance + "m is " + feetRound + "f");
				}
				else
				{
					System.out.println("Please Try again.");
					continue;
				}
				
				System.out.print("Do you want to continue? (Y)es or (N)o: ");
				String answer = scans.nextLine();
				
				if (answer.equalsIgnoreCase("N"))
				{
					shouldWeLoop = false;
				}
				else if (!answer.equalsIgnoreCase("Y"))
				{
					System.out.println("Please try again.");
					continue;
				}
			}
			System.out.println("Thanks for using this program.");
		}
	}

}
