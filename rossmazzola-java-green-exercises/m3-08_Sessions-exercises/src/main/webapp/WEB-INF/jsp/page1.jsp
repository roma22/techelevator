<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<c:set var="pageTitle" value="Greetings!" />
<%@include file="common/header.jspf"%>
<div class="page1">
	<h1>What is your favorite color?</h1>
	<hr />
	<c:url var="formAction" value="/page1" />
	<form action="${formAction}" method="POST">
		<label for="color">Color:</label>
		<input type="text" name="color" id="color" />
		<input type="submit" value="Next >>>" id="button" />
	</form>
</div>
<%@include file="common/footer.jspf"%>
