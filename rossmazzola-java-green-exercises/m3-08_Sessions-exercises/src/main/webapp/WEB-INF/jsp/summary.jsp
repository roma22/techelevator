<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<c:set var="pageTitle" value="Greetings!" />
<%@include file="common/header.jspf"%>
<div class="summary">
	<h1>Summary</h1>
	<hr />
	<div>Favorite Color: <c:out value="${ favoriteThings.color }"/></div>
	<div>Favorite Food: <c:out value="${ favoriteThings.food }"/></div>
	<div>Favorite Season: <c:out value="${ favoriteThings.season }"/></div>
</div>
<%@include file="common/footer.jspf"%>
