<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<c:set var="pageTitle" value="Greetings!" />
<%@include file="common/header.jspf"%>
<div class="page3">
	<h1>What is your favorite season?</h1>
	<hr />
	<c:url var="formAction" value="/page3" />
	<form action="${ formAction }" method="POST">
		<label for="season">Season:</label>
		<input type="radio" name="season" value="spring" id="season" />
		Spring
		<input type="radio" name="season" value="summer" id="season" />
		Summer
		<input type="radio" name="season" value="fall" id="season" />
		Fall
		<input type="radio" name="season" value="winter" id="season" checked/>
		Winter
		<input type="submit" value="Next >>>" id="button" />
	</form>
</div>
<%@include file="common/footer.jspf"%>
