<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<c:set var="pageTitle" value="Greetings!" />
<%@include file="common/header.jspf"%>
<div class="page2">
	<h1>What is your favorite food?</h1>
	<hr />
	<c:url var="formAction" value="/page2" />
	<form action="${ formAction }" method="POST">
		<label for="food">Food:</label>
		<input type="text" name="food" id="food" />
		<input type="submit" value="Next >>>" id="button" />
	</form>
</div>
<%@include file="common/footer.jspf"%>
