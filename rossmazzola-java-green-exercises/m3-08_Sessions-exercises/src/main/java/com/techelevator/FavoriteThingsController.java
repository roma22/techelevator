package com.techelevator;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.techelevator.model.FavoriteThings;

@Controller
@SessionAttributes("favoriteThings")
public class FavoriteThingsController
{
	@RequestMapping(path = "/page1", method = RequestMethod.GET)
	public String showPage1()
	{
		return "page1";
	}

	@RequestMapping(path = "/page1", method = RequestMethod.POST)
	public String submitPage1(FavoriteThings favoriteThings)
	{
		return "redirect:page2";
	}

	@RequestMapping(path = "/page2", method = RequestMethod.GET)
	public String showPage2()
	{
		return "page2";
	}

	@RequestMapping(path = "/page2", method = RequestMethod.POST)
	public String submitPage2(FavoriteThings favoriteThings)
	{
		return "redirect:page3";
	}

	@RequestMapping(path = "/page3", method = RequestMethod.GET)
	public String showPage3()
	{
		return "page3";
	}

	@RequestMapping(path = "/page3", method = RequestMethod.POST)
	public String submitPage3(FavoriteThings favoriteThings)
	{
		return "redirect:summary";
	}

	@RequestMapping(path = "/summary", method = RequestMethod.GET)
	public String showSummary(FavoriteThings favoriteThings)
	{
		return "summary";
	}
}
