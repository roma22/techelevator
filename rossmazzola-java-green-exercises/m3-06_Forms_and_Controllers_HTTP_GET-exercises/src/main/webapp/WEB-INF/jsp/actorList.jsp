<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:set var="pageTitle" value="All Actors List"/>

<%@include file="common/header.jspf"%>
<div class="actorsearch">
<h1>Actor Search</h1>

<c:url value="/actorList" var="formAction" />
<form method="GET" action="${ formAction }">
    <label for="lastName">Last Name:</label>
    <input type="text" name="lastName"/>
    <input type="submit" value="Submit"/>
</form>

<table class="table">
    <tr>
	<th>Name</th>
    </tr>
    <c:forEach items="${ actors }" var="actor">
	<tr>
	    <td><c:out value="${ actor.firstName } ${ actor.lastName }"/></td>
	</tr>
    </c:forEach>
</table>
</div>
<%@include file="common/footer.jspf"%>