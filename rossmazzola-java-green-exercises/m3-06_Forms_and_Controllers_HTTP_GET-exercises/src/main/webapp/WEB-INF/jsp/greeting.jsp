<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:set var="pageTitle" value="Greetings!"/>
<%@include file="common/header.jspf"%>

<h1>Hello <c:out value="${param.name}" />!</h1>

<%@include file="common/footer.jspf"%>