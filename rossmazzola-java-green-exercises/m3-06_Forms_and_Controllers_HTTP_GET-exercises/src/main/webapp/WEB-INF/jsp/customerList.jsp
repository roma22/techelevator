<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:set var="pageTitle" value="All Customers List" />

<%@include file="common/header.jspf"%>
<div class="customersearch">
	<h1>Customer Search</h1>

	<c:url value="/customerList" var="formAction" />
	<form method="GET" action="${ formAction }">
		<div id="group">
			<label for="name">Name:</label> <input type="text" name="name" />
		</div>

		<div id="group">
			<label for="sort">Sort:</label> <select name="sort" id="sort">
				<option value="last_name">Last Name</option>
				<option value="email">Email</option>
				<option value="active">Active</option>
			</select>
		</div>
		<input type="submit" value="Search" id="button" />
	</form>

	<table class="table">
		<tr>
			<th>Name</th>
			<th>Email</th>
			<th>Active</th>
		</tr>
		<c:forEach items="${ customers }" var="customer">
			<tr>
				<td><c:out value="${ customer.firstName } ${ customer.lastName }" /></td>
				<td><c:out value="${ customer.email }" /></td>
				<c:choose>
					<c:when test="${ customer.active }">
						<c:set var="active" value="Yes" />
					</c:when>
					<c:otherwise>
						<c:set var="active" value="No" />
					</c:otherwise>
				</c:choose>
				<td><c:out value="${ active }" /></td>
			</tr>
		</c:forEach>
	</table>
</div>
<%@include file="common/footer.jspf"%>