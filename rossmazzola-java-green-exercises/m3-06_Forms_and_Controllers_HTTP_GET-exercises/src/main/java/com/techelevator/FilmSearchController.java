package com.techelevator;

import com.techelevator.dao.FilmDao;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * FilmSearchController
 */
@Controller
public class FilmSearchController
{
	@Autowired
	FilmDao filmDao;

	@RequestMapping("/filmSearch")
	public String showFilmSearchForm()
	{
		return null;
	}

	@RequestMapping("/filmList")
	public String searchFilms(HttpServletRequest request)
	{
		String genre = request.getParameter("genre");
		int minLength = 0;
		int maxLength = 0;

		if (request.getParameter("minLength").matches("\\d+"))
		{
			minLength = Integer.parseInt(request.getParameter("minLength"));
		}
		if (request.getParameter("maxLength").matches("\\d+"))
		{
			maxLength = Integer.parseInt(request.getParameter("maxLength"));
		}
		request.setAttribute("films", filmDao.getFilmsBetween(genre, minLength, maxLength));
		return "filmList";
	}
}
