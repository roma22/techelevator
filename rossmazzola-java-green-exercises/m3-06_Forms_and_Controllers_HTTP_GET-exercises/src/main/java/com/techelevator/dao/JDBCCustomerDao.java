package com.techelevator.dao;

import com.techelevator.dao.model.Customer;

import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;

/**
 * JDBCCustomerDao
 */
@Component
public class JDBCCustomerDao implements CustomerDao
{

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public JDBCCustomerDao(DataSource dataSource)
    {
	this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public List<Customer> searchAndSortCustomers(String search, String sort)
	{
		List<Customer> matchingCustomers = new ArrayList<>();
		String sortColumn = "last_name";
		if (sort.equals("email"))
		{
			sortColumn = "email";
		}
		if (sort.equals("active"))
		{
			sortColumn = "active";
		}
		
		String customerSearchSql = "SELECT first_name, last_name, email, active " + "FROM customer "
				+ "WHERE first_name ILIKE ? " + "OR last_name ILIKE ? " + "order by " + sortColumn;
		SqlRowSet results = jdbcTemplate.queryForRowSet(customerSearchSql, "%" + search + "%", "%" + search + "%");
		while (results.next())
		{
			matchingCustomers.add(mapRowToCustomer(results));
		}
		return matchingCustomers;
	}

	private Customer mapRowToCustomer(SqlRowSet results)
	{
		Customer customer = new Customer();
		customer.setFirstName(results.getString("first_name"));
		customer.setLastName(results.getString("last_name"));
		customer.setEmail(results.getString("email"));
		if (results.getInt("active") == 0)
		{
			customer.setActive(false);
		}
		else
		{
			customer.setActive(true);
		}
		return customer;
	}
}
