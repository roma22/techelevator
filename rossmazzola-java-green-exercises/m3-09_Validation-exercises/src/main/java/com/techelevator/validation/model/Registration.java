package com.techelevator.validation.model;

import java.time.LocalDate;
import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.format.annotation.DateTimeFormat;

public class Registration
{
	@NotBlank(message = "First name is required")
	@Length(max = 20, message = "First name must be 20 characters or less")
	private String firstName;

	@NotBlank(message = "Last name is required")
	@Length(max = 20, message = "Last name must be 20 characters or less")
	private String lastName;

	@NotBlank(message = "Email address is required")
	@Email(message = "Invalid email")
	private String email;
	private String confirmEmail;

	@NotBlank(message = "Password is required")
	@Length(min = 8, message = "Password minimum is 8 characters")
	private String password;
	private String confirmPassword;

	@NotNull(message = "Birthday is required")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
//	@Pattern(regexp = "^(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d$", message = "The value is not valid for Birthday.")
	private LocalDate birthday;

	@NotNull(message = "# of Tickets is required")
	@Min(value = 1, message = "# of Tickets must be between 1 and 10")
	@Max(value = 10, message = "# of Tickets must be between 1 and 10")
	private Integer tickets;

	private boolean emailMatching;

	@AssertTrue(message = "Email address does not match")
	public boolean isEmailMatching()
	{
		if (email != null)
		{
			return email.equals(confirmEmail);
		}
		else
		{
			return false;
		}
	}

	private boolean passwordMatching;

	@AssertTrue(message = "Passwords do not match")
	public boolean isPasswordMatching()
	{
		if (password != null)
		{
			return password.equals(confirmPassword);
		}
		else
		{
			return false;
		}
	}

	public String getFirstName()
	{
		return firstName;
	}

	public void setFirstName(String firstName)
	{
		this.firstName = firstName;
	}

	public String getLastName()
	{
		return lastName;
	}

	public void setLastName(String lastName)
	{
		this.lastName = lastName;
	}

	public String getEmail()
	{
		return email;
	}

	public void setEmail(String email)
	{
		this.email = email;
	}

	public String getConfirmEmail()
	{
		return confirmEmail;
	}

	public void setConfirmEmail(String confirmEmail)
	{
		this.confirmEmail = confirmEmail;
	}

	public String getPassword()
	{
		return password;
	}

	public void setPassword(String password)
	{
		this.password = password;
	}

	public String getConfirmPassword()
	{
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword)
	{
		this.confirmPassword = confirmPassword;
	}

	public LocalDate getBirthday()
	{
		return birthday;
	}

	public void setBirthday(LocalDate birthday)
	{
		this.birthday = birthday;
	}

	public Integer getTickets()
	{
		return tickets;
	}

	public void setTickets(Integer tickets)
	{
		this.tickets = tickets;
	}
}
