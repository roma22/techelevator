<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:set var="pageTitle" value="Confirmation"/>
<%@include file="common/header.jspf" %>
<div class="confirmation">
	<h2>Confirmation</h2>

	<p>You have successfully <c:out value="${ login } ${ register }"/></p>
</div>
<%@include file="common/footer.jspf" %>
