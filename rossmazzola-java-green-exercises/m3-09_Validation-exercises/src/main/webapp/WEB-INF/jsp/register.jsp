<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var="pageTitle" value="New User"/>
<%@include file="common/header.jspf" %>
<div class="register">
	<h2>New User Registration</h2>

	<c:url var="formAction" value="/register"/>
	<form:form method="POST" action="${ formAction }" modelAttribute="registration">
		<form:errors path="*" style="color: red"/>
		<div class="formGroup">
			<label for="firstName">First Name</label>
			<form:input path="firstName" placeholder=" enter first name"/>
		</div>
		<div class="formGroup">
			<label for="lastName">Last Name</label>
			<form:input path="lastName" placeholder=" enter last name"/>
		</div>
		<div class="formGroup">
			<label for="email">Email</label>
			<form:input type="email" path="email" placeholder=" enter email"/>
		</div>
		<div class="formGroup">
			<label for="confirmEmail">Confirm Email</label>
			<form:input type="email" path="confirmEmail" placeholder=" confirm email address"/>
		</div>
		<div class="formGroup">
			<label for="password">Password</label>
			<form:password path="password" placeholder=" enter password"/>
		</div>
		<div class="formGroup">
			<label for="confirmPassword">Confirm Password</label>
			<form:password path="confirmPassword" placeholder=" confirm password"/>
		</div>
		<div class="formGroup">
			<label for="birthday">Birthday</label>
			<form:input type="date" path="birthday" placeholder=" enter birth date"/>
		</div>
		<div class="formGroup">
			<label for="tickets"># of Tickets</label>
			<form:input path="tickets" placeholder=" enter number of tickets"/>
		</div>
		<input type="submit" value="Submit" class="button"/>
	</form:form>
</div>
<%@include file="common/footer.jspf" %>
