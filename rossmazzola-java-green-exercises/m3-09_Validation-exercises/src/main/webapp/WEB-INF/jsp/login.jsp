<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var="pageTitle" value="Login"/>
<%@include file="common/header.jspf" %>
<div class="login">
	<h2>Login</h2>

	<c:url var="formAction" value="/login"/>
	<form:form method="POST" action="${ formAction }" modelAttribute="login">
		<form:errors path="*" style="color: red"/>
		<div class="formGroup">
			<label for="email">Email</label>
			<form:input type="email" path="email" placeholder=" enter email"/>
		</div>
		<div class="formGroup">
			<label for="password">Password</label>
			<form:password path="password" placeholder=" enter password"/>
		</div>
		<input type="submit" value="Submit" class="button"/>
	</form:form>
</div>
<%@include file="common/footer.jspf" %>
