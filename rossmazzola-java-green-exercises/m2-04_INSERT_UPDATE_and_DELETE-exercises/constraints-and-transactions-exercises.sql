-- Write queries to return the following:
-- The following changes are applied to the "pagila" database.**

-- 1. Add actors, Hampton Avenue, and Lisa Byway to the actor table.

INSERT INTO actor (first_name, last_name)
VALUES ('HAMPTON', 'AVENUE'), ('LISA', 'BYWAY');

-- 2. Add "Euclidean PI", "The epic story of Euclid as a pizza delivery boy in 
-- ancient Greece", to the film table. The movie was released in 2008 in English. 
-- Since its an epic, the run length is 3hrs and 18mins. There are no special 
-- features, the film speaks for itself, and doesn't need any gimmicks.	

INSERT INTO film (title, description, release_year, language_id, length)
VALUES ('EUCLIDEAN PI', 'The epic story of Euclid as a pizza delivery boy in ancient Greece', '2008', '1', '198');

-- 3. Hampton Avenue plays Euclid, while Lisa Byway plays his slightly 
-- overprotective mother, in the film, "Euclidean PI". Add them to the film.

INSERT INTO film_actor (film_id, actor_id)
VALUES ('1001', '201'), ('1001', '202');

-- 4. Add Mathmagical to the category table.

INSERT INTO category (name)
VALUES ('Mathmagical');

-- 5. Assign the Mathmagical category to the following films, "Euclidean PI", 
-- "EGG IGBY", "KARATE MOON", "RANDOM GO", and "YOUNG LANGUAGE"

INSERT INTO film_category (film_id, category_id)
VALUES ('1001', '17');

UPDATE film_category SET category_id = '17'
WHERE film_id IN ('274', '494', '714', '996');

-- 6. Mathmagical films always have a "G" rating, adjust all Mathmagical films 
-- accordingly.
-- (5 rows affected)

UPDATE film
SET rating = 'G'
FROM film_category
JOIN category ON category.category_id = film_category.category_id
WHERE film_category.film_id = film.film_id
AND category.name = 'Mathmagical';

SELECT category.name, film.rating
FROM film
JOIN film_category ON film_category.film_id = film.film_id
JOIN category ON category.category_id = film_category.category_id
WHERE category.name = 'Mathmagical';

-- 7. Add a copy of "Euclidean PI" to all the stores.

INSERT INTO inventory (film_id, store_id)
VALUES ('1001', '1'), ('1001', '2');

-- 8. The Feds have stepped in and have impounded all copies of the pirated film, 
-- "Euclidean PI". The film has been seized from all stores, and needs to be 
-- deleted from the film table. Delete "Euclidean PI" from the film table. 
-- (Did it succeed? Why?)

DELETE FROM film
WHERE title = 'EUCLIDEAN PI';
--failed due to [Code: 0, SQL State: 23503]  ERROR: update or delete on table "film"
--violates foreign key constraint "film_actor_film_id_fkey" on table "film_actor"
--Detail: Key (film_id)=(1001) is still referenced from table "film_actor".

-- 9. Delete Mathmagical from the category table. 
-- (Did it succeed? Why?)

DELETE FROM category
WHERE name = 'Mathmagical';
--failed due to [Code: 0, SQL State: 23503]  ERROR: update or delete on table "category"
--violates foreign key constraint "film_category_category_id_fkey" on table "film_category"
--Detail: Key (category_id)=(17) is still referenced from table "film_category".

-- 10. Delete all links to Mathmagical in the film_category tale. 
-- (Did it succeed? Why?)

DELETE FROM film_category
WHERE film_id = '1001' AND category_id = '17';
--succeeded due to no foreign key constraints

-- 11. Retry deleting Mathmagical from the category table, followed by retrying
-- to delete "Euclidean PI". 
-- (Did either deletes succeed? Why?)

DELETE FROM category
WHERE name = 'Mathmagical';

DELETE FROM film
WHERE title = 'EUCLIDEAN PI';
--both still failed due to foreign key constraints

-- 12. Check database metadata to determine all constraints of the film id, and 
-- describe any remaining adjustments needed before the film "Euclidean PI" can 
-- be removed from the film table.

SELECT constraint_name
FROM INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE
WHERE table_name = 'film';
--all foreign key dependencies must be removed
--film_id keys must be removed from film_actor and inventory
