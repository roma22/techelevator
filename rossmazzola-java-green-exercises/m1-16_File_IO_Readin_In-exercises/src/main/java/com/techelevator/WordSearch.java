package com.techelevator;

import java.io.File;
import java.util.Scanner;

public class WordSearch
{

	public static void main(String[] args)
	{
		try (Scanner scans = new Scanner(System.in))
		{
			boolean shouldLoop = true;

			while (shouldLoop)
			{
				System.out.print("File to search for: ");
				String inputFile = scans.nextLine();

				File text = new File(inputFile);

				System.out.print("Word to search for: ");
				String inputWord = scans.nextLine();

				System.out.print("Case sensitive? Y or N ");
				String inputCase = scans.nextLine();

				try (Scanner alice = new Scanner(text))
				{
					for (int i = 1; alice.hasNextLine(); i++)
					{
						String line = alice.nextLine();

						if (inputCase.equalsIgnoreCase("Y"))
						{
							if (line.contains(inputWord))
							{
								System.out.println(i + ")" + " " + line);
							}
						}
						else if (inputCase.equalsIgnoreCase("N"))
						{
							if (line.toLowerCase().contains(inputWord.toLowerCase()))
							{
								System.out.println(i + ")" + " " + line);
							}
						}
					}
					System.out.println("");
				}
				catch (Exception e)
				{
					System.out.println("\nPlease try again.\n");
				}
			}
		}
	}
}
