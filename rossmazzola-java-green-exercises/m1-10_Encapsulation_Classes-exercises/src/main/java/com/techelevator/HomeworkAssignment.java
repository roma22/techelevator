package com.techelevator;

public class HomeworkAssignment
{
	private int totalMarks;
	private int possibleMarks;
	private String submitterName;

	public HomeworkAssignment(int possibleMarks)
	{
		this.possibleMarks = possibleMarks;
	}

	public int getTotalMarks()
	{
		return totalMarks;
	}

	public void setTotalMarks(int totalMarks)
	{
		this.totalMarks = totalMarks;
	}

	public String getSubmitterName()
	{
		return submitterName;
	}

	public void setSubmitterName(String submitterName)
	{
		this.submitterName = submitterName;
	}

	public int getPossibleMarks()
	{
		return possibleMarks;
	}

	public String getLetterGrade()
	{
		double numberGrade = (double)totalMarks / (double)possibleMarks;

		if (numberGrade >= 0.90)
		{
			return "A";
		}
		else if (numberGrade >= 0.80)
		{
			return "B";
		}
		else if (numberGrade >= 0.70)
		{
			return "C";
		}
		else if (numberGrade >= 0.60)
		{
			return "D";
		}
		else
		{
			return "F";
		}
	}
}
