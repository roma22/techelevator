package com.techelevator;

public class Exercises {

	public static void main(String[] args) {

        /*
        1. 4 birds are sitting on a branch. 1 flies away. How many birds are left on
        the branch?
        */

		// ### EXAMPLE:
		int initialNumberOfBirds = 4;
		int birdsThatFlewAway = 1;
		int remainingNumberOfBirds = initialNumberOfBirds - birdsThatFlewAway;

        /*
        2. There are 6 birds and 3 nests. How many more birds are there than
        nests?
        */

		// ### EXAMPLE:
		int numberOfBirds = 6;
		int numberOfNests = 3;
		int numberOfExtraBirds = numberOfBirds - numberOfNests;

        /*
        3. 3 raccoons are playing in the woods. 2 go home to eat dinner. How
        many raccoons are left in the woods?
        */
		int raccoonsPlaying = 3;
		int raccoonsWentEating = 2;
		int raccoonsLeftInWoods = raccoonsPlaying - raccoonsWentEating;
		
        /*
        4. There are 5 flowers and 3 bees. How many less bees than flowers?
        */
		int flowersQuantity = 5;
		int beesQuantity = 3;
		int beesDeficit = flowersQuantity - beesQuantity;
		
        /*
        5. 1 lonely pigeon was eating breadcrumbs. Another pigeon came to eat
        breadcrumbs, too. How many pigeons are eating breadcrumbs now?
        */
		int pigeonsEating = 1;
		int pigeonsAdded = 1;
		int pigeonsEatingTotal = pigeonsEating + pigeonsAdded;
		
        /*
        6. 3 owls were sitting on the fence. 2 more owls joined them. How many
        owls are on the fence now?
        */
		int owlsSitting = 3;
		int owlsAdded = 2;
		int owlsSittingTotal = owlsSitting + owlsAdded;
		
        /*
        7. 2 beavers were working on their home. 1 went for a swim. How many
        beavers are still working on their home?
        */
		int beaversBusy = 2;
		int beaversWentSwimming = 1;
		int beaversStillBusy = beaversBusy - beaversWentSwimming;
		
        /*
        8. 2 toucans are sitting on a tree limb. 1 more toucan joins them. How
        many toucans in all?
        */
		int toucansSitting = 2;
		int toucansAdded = 1;
		int toucansSittingTotal = toucansSitting + toucansAdded;
		
        /*
        9. There are 4 squirrels in a tree with 2 nuts. How many more squirrels
        are there than nuts?
        */
		int squirrelsQuantity = 4;
		int nutsQuantity = 2;
		int squirrelsSurplus = squirrelsQuantity - nutsQuantity;
		
        /*
        10. Mrs. Hilt found a quarter, 1 dime, and 2 nickels. How much money did
        she find?
        */
		final int QUARTER_CENTS = 25;
		final int DIME_CENTS = 10;
		final int NICKEL_CENTS = 5;
		int nickelsFoundQuantity = 2;
		int moneyFoundTotalInCents = QUARTER_CENTS + DIME_CENTS + (NICKEL_CENTS * nickelsFoundQuantity);
		
        /*
        11. Mrs. Hilt's favorite first grade classes are baking muffins. Mrs. Brier's
        class bakes 18 muffins, Mrs. MacAdams's class bakes 20 muffins, and
        Mrs. Flannery's class bakes 17 muffins. How many muffins does first
        grade bake in all?
        */
		int muffinsFromClassBrier = 18;
		int muffinsFromClassMacAdams = 20;
		int muffinsFromClassFlannery = 17;
		int muffinsFromClassTotal = muffinsFromClassBrier + muffinsFromClassMacAdams + muffinsFromClassFlannery;
		
        /*
        12. Mrs. Hilt bought a yoyo for 24 cents and a whistle for 14 cents. How
        much did she spend in all for the two toys?
        */
		int costPerYoyoInCents = 24;
		int costPerWhistleInCents = 14;
		int costTotalInCents = costPerYoyoInCents + costPerWhistleInCents;
		
        /*
        13. Mrs. Hilt made 5 Rice Krispie Treats. She used 8 large marshmallows
        and 10 mini marshmallows.How many marshmallows did she use
        altogether?
        */
		int marshmallowsUsedLarge = 8;
		int marshmallowsUsedMini = 10;
		int marshmallowsUsedTotal = marshmallowsUsedLarge + marshmallowsUsedMini;
		
        /*
        14. At Mrs. Hilt's house, there was 29 inches of snow, and Brecknock
        Elementary School received 17 inches of snow. How much more snow
        did Mrs. Hilt's house have?
        */
		int snowAtHouseInInches = 29;
		int snowAtSchoolInInches = 17;
		int snowAtHouseSurplusInInches = snowAtHouseInInches - snowAtSchoolInInches;
		
        /*
        15. Mrs. Hilt has $10. She spends $3 on a toy truck and $2 on a pencil
        case. How much money does she have left?
        */
		int moneyHiltHasInitialInDollars = 10;
		int costPerToyTruckInDollars = 3;
		int costPerPencilCaseInDollars = 2;
		int moneyHiltHasRemaining = moneyHiltHasInitialInDollars - costPerToyTruckInDollars - costPerPencilCaseInDollars;
		
        /*
        16. Josh had 16 marbles in his collection. He lost 7 marbles. How many
        marbles does he have now?
        */
		int marblesJoshHasInitial = 16;
		int marblesJoshHasLost = 7;
		int marblesJoshHasRemaining = marblesJoshHasInitial - marblesJoshHasLost;
		
        /*
        17. Megan has 19 seashells. How many more seashells does she need to
        find to have 25 seashells in her collection?
        */
		int seashellsQuantityInitial = 19;
		int seashellsQuantityGoal = 25;
		int seashellsQuantityNeeded = seashellsQuantityGoal - seashellsQuantityInitial;
        
		/*
        18. Brad has 17 balloons. 8 balloons are red and the rest are green. How
        many green balloons does Brad have?
        */
		int balloonsQuantityTotal = 17;
		int balloonsQuantityRed = 8;
		int balloonsQuantityGreen = balloonsQuantityTotal - balloonsQuantityRed;
        
		/*
        19. There are 38 books on the shelf. Marta put 10 more books on the shelf.
        How many books are on the shelf now?
        */
		int booksOnShelfInitial = 38;
		int booksOnShelfAdded = 10;
		int booksOnShelfTotal = booksOnShelfInitial + booksOnShelfAdded;
		
        /*
        20. A bee has 6 legs. How many legs do 8 bees have?
        */
		int legsOnBee = 6;
		int beeQuantity = 8;
		int legsOnBeeTotal = legsOnBee * beeQuantity;
		
        /*
        21. Mrs. Hilt bought an ice cream cone for 99 cents. How much would 2 ice
        cream cones cost?
        */
		int iceCreamConeCostInCents = 99;
		int iceCreamConeQuantity = 2;
		int iceCreamConeCostTotalInCents = iceCreamConeCostInCents * iceCreamConeQuantity;

		/*
        22. Mrs. Hilt wants to make a border around her garden. She needs 125
        rocks to complete the border. She has 64 rocks. How many more rocks
        does she need to complete the border?
        */
		int rocksQuantityGoal = 125;
		int rocksQuantityInitial = 64;
		int rocksQuantityNeeded = rocksQuantityGoal - rocksQuantityInitial;
		
        /*
        23. Mrs. Hilt had 38 marbles. She lost 15 of them. How many marbles does
        she have left?
        */
		int marblesHiltHasInitial = 38;
		int marblesHiltHasLost = 15;
		int marblesHiltHasRemaining = marblesHiltHasInitial - marblesHiltHasLost;
	
        /*
        24. Mrs. Hilt and her sister drove to a concert 78 miles away. They drove 32
        miles and then stopped for gas. How many miles did they have left to drive?
        */
		int tripTotalInMiles = 78;
		int tripPartialInMiles = 32;
		int tripRemainingInMiles = tripTotalInMiles - tripPartialInMiles;
		
        /*
        25. Mrs. Hilt spent 1 hour and 30 minutes shoveling snow on Saturday
        morning and 45 minutes shoveling snow on Saturday afternoon. How
        much total time did she spend shoveling snow?
        */
		int timeShovelingSnowInMorningInMinutes = 90;
		int timeShovelingSnowInAfternoonInMinutes = 45;
		int timeShovelingSnowTotalInMinutes = timeShovelingSnowInMorningInMinutes + timeShovelingSnowInAfternoonInMinutes;
		
        /*
        26. Mrs. Hilt bought 6 hot dogs. Each hot dog cost 50 cents. How much
        money did she pay for all of the hot dogs?
        */
		int hotDogQuantity = 6;
		int costPerHotDogInCents = 50;
		int hotDogCostTotalInCents = hotDogQuantity * costPerHotDogInCents;
		
        /*
        27. Mrs. Hilt has 50 cents. A pencil costs 7 cents. How many pencils can
        she buy with the money she has?
        */
		int moneyHiltHasInitialInCents = 50;
		int costPerPencilInCents = 7;
		int pencilsHiltCanBuyQuantity = moneyHiltHasInitialInCents / costPerPencilInCents;
		
        /*
        28. Mrs. Hilt saw 33 butterflies. Some of the butterflies were red and others
        were orange. If 20 of the butterflies were orange, how many of them
        were red?
        */
		int butterfliesTotal = 33;
		int butterfliesOrange = 20;
		int butterfliesRed = butterfliesTotal - butterfliesOrange;
		
        /*
        29. Kate gave the clerk $1.00. Her candy cost 54 cents. How much change
        should Kate get back?
        */
		int paymentTotalInCents = 100;
		int paymentCostInCents = 54;
		int paymentChangeInCents = paymentTotalInCents - paymentCostInCents;
		
        /*
        30. Mark has 13 trees in his backyard. If he plants 12 more, how many trees
        will he have?
        */
		int treesQuantityInitial = 13;
		int treesQuantityAdded = 12;
		int treesQuantityTotal = treesQuantityInitial + treesQuantityAdded;
		
        /*
        31. Joy will see her grandma in two days. How many hours until she sees
        her?
        */
		final int HOURS_IN_DAY = 24;
		int daysUntilSeeGrandma = 2;
		int hoursUntilSeeGrandma = HOURS_IN_DAY * daysUntilSeeGrandma;
		
        /*
        32. Kim has 4 cousins. She wants to give each one 5 pieces of gum. How
        much gum will she need?
        */
		int cousinsQuantity = 4;
		int gumPiecesPerCousin = 5;
		int gumPiecesTotal = cousinsQuantity * gumPiecesPerCousin;
		
        /*
        33. Dan has $3.00. He bought a candy bar for $1.00. How much money is
        left?
        */
		int moneyDanHasInitialInDollars = 3;
		int costPerCandyBarInDollars = 1;
		int moneyDanHasRemainingInDollars = moneyDanHasInitialInDollars - costPerCandyBarInDollars;
		
        /*
        34. 5 boats are in the lake. Each boat has 3 people. How many people are
        on boats in the lake?
        */
		int boatsInLakeQuantity = 5;
		int boatsPeopleCapacity = 3;
		int boatsPeopleCapacityTotal = boatsInLakeQuantity * boatsPeopleCapacity;
		
        /*
        35. Ellen had 380 legos, but she lost 57 of them. How many legos does she
        have now?
        */
		int legosQuantityInitial = 380;
		int legosQuantityLost = 57;
		int legosQuantityRemaining = legosQuantityInitial - legosQuantityLost;
		
        /*
        36. Arthur baked 35 muffins. How many more muffins does Arthur have to
        bake to have 83 muffins?
        */
		int muffinsQuantity = 35;
		int muffinsQuantityGoal = 83;
		int muffinsQuantityNeeded = muffinsQuantityGoal - muffinsQuantity;
		
        /*
        37. Willy has 1400 crayons. Lucy has 290 crayons. How many more
        crayons does Willy have then Lucy?
        */
		int crayonsWillyHas = 1400;
		int crayonsLucyHas = 290;
		int crayonsWillyHasSurplus = crayonsWillyHas - crayonsLucyHas;
		
        /*
        38. There are 10 stickers on a page. If you have 22 pages of stickers, how
        many stickers do you have?
        */
		int stickersPerPage = 10;
		int pagesQuantity = 22;
		int stickersTotal = stickersPerPage * pagesQuantity;
		
        /*
        39. There are 96 cupcakes for 8 children to share. How much will each
        person get if they share the cupcakes equally?
        */
		int cupcakesQuantity = 96;
		int childrenQuantity = 8;
		int cupcakesPerChild = cupcakesQuantity / childrenQuantity;
		
        /*
        40. She made 47 gingerbread cookies which she will distribute equally in
        tiny glass jars. If each jar is to contain six cookies each, how many
        cookies will not be placed in a jar?
        */
		int gingerbreadCookiesQuantity = 47;
		int gingerbreadCookiesPerJar = 6;
		int gingerbreadCookiesOutsideJar = gingerbreadCookiesQuantity % gingerbreadCookiesPerJar;
		
        /*
        41. She also prepared 59 croissants which she plans to give to her 8
        neighbors. If each neighbor received and equal number of croissants,
        how many will be left with Marian?
        */
		int croissantsInitial = 59;
		int neighborsQuantity = 8;
		int croissantsRemaining = croissantsInitial % neighborsQuantity;
		
        /*
        42. Marian also baked oatmeal cookies for her classmates. If she can
        place 12 cookies on a tray at a time, how many trays will she need to
        prepare 276 oatmeal cookies at a time?
        */
		int oatmealCookiesQuantity = 276;
		int oatmealCookiesPerTray = 12;
		int trayQuantityNeeded = oatmealCookiesQuantity / oatmealCookiesPerTray;
		
        /*
        43. Marian’s friends were coming over that afternoon so she made 480
        bite-sized pretzels. If one serving is equal to 12 pretzels, how many
        servings of bite-sized pretzels was Marian able to prepare?
        */
		int pretzelsQuantity = 480;
		int pretzelsPerServing = 12;
		int servingsQuantity = pretzelsQuantity / pretzelsPerServing;
		
        /*
        44. Lastly, she baked 53 lemon cupcakes for the children living in the city
        orphanage. If two lemon cupcakes were left at home, how many
        boxes with 3 lemon cupcakes each were given away?
        */
		int lemonCupcakesQuantity = 53;
		int lemonCupcakesLeftAtHome = 2;
		int lemonCupcakesPerBox = 3;
		int lemonCupcakesBoxesTotal = (lemonCupcakesQuantity - lemonCupcakesLeftAtHome) / lemonCupcakesPerBox;
		
        /*
        45. Susie's mom prepared 74 carrot sticks for breakfast. If the carrots
        were served equally to 12 people, how many carrot sticks were left
        uneaten?
        */
		int carrotSticksQuantity = 74;
		int peopleServedEqually = 12;
		int carrotSticksRemaining = carrotSticksQuantity % peopleServedEqually;
		
        /*
        46. Susie and her sister gathered all 98 of their teddy bears and placed
        them on the shelves in their bedroom. If every shelf can carry a
        maximum of 7 teddy bears, how many shelves will be filled?
        */
		int teddyBearsQuantity = 98;
		int teddyBearsPerShelf = 7;
		int shelvesFilledQuantity = teddyBearsQuantity / teddyBearsPerShelf;
		
        /*
        47. Susie’s mother collected all family pictures and wanted to place all of
        them in an album. If an album can contain 20 pictures, how many
        albums will she need if there are 480 pictures?
        */
		int picturesPerAlbum = 20;
		int picturesQuantity = 480;
		int albumQuantityNeeded = picturesQuantity / picturesPerAlbum;
		
        /*
        48. Joe, Susie’s brother, collected all 94 trading cards scattered in his
        room and placed them in boxes. If a full box can hold a maximum of 8
        cards, how many boxes were filled and how many cards are there in
        the unfilled box?
        */
		int tradingCardsQuantity = 94;
		int tradingCardsPerBox = 8;
		int boxesFilledQuantity = tradingCardsQuantity / tradingCardsPerBox;
		int tradingCardsInUnfilledBox = tradingCardsQuantity % tradingCardsPerBox;
		
        /*
        49. Susie’s father repaired the bookshelves in the reading room. If he has
        210 books to be distributed equally on the 10 shelves he repaired,
        how many books will each shelf contain?
        */
		int booksQuantity = 210;
		int shelvesQuantity = 10;
		int booksPerShelf = booksQuantity / shelvesQuantity;
		
        /*
        50. Cristina baked 17 croissants. If she planned to serve this equally to
        her seven guests, how many will each have?
        */
		int croissantsCristinaHasInitial = 17;
		int guestsServedEqually = 7;
		int croissantsPerGuest = croissantsCristinaHasInitial / guestsServedEqually;
		
        /*
            CHALLENGE PROBLEMS
        */

        /*
        Bill and Jill are house painters. Bill can paint a 12 x 14 room in 2.15 hours, while Jill averages
        1.90 hours. How long will it take the two painter working together to paint 5 12 x 14 rooms?
        Hint: Calculate the hourly rate for each painter, combine them, and then divide the total walls in feet by the combined hourly rate of the painters.
        Challenge: How many days will it take the pair to paint 623 rooms assuming they work 8 hours a day?.
        */
		float roomsPerHourBill = 1 / 2.15F;
		float roomsPerHourJill = 1 / 1.90F;
		float roomsPerHourCombined = roomsPerHourBill + roomsPerHourJill;
		float hoursUntilFiveRooms = 5 / roomsPerHourCombined;
		float roomsPerEightHourDay = 8 * roomsPerHourCombined;
		float daysUntilComplete = 623 / roomsPerEightHourDay;
		
        /*
        Create and assign variables to hold your first name, last name, and middle initial. Using concatenation,
        build an additional variable to hold your full name in the order of last name, first name, middle initial. The
        last and first names should be separated by a comma followed by a space, and the middle initial must end
        with a period.
        Example: "Hopper, Grace B."
        */
		String firstName = "Ross";
		String middleInitial = "A";
		String lastName = "Mazzola";
		String fullName = lastName + ", " + firstName + " " + middleInitial + ".";
		
        /*
        The distance between New York and Chicago is 800 miles, and the train has already travelled 537 miles.
        What percentage of the trip has been completed?
        Hint: The percent completed is the miles already travelled divided by the total miles.
        Challenge: Display as an integer value between 0 and 100 using casts.
        */
		float currentDistance = 537;
		float totalDistance = 800;
		float percentDistance = (currentDistance / totalDistance) * 100;
		int percentDistanceInteger = (int)percentDistance;
		
		//printout
		System.out.println(" ");
		System.out.println(" ");
		System.out.println(hoursUntilFiveRooms + " hours");
		System.out.println(daysUntilComplete + " days");
		System.out.println(fullName);
		System.out.println(percentDistanceInteger + "%");
		
	}

}
