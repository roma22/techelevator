// add pageTitle
pageTitle = 'My Shopping List';

// add groceries
groceries = [
  { id: 1, item: 'Absolution', completed: false },
  { id: 2, item: 'CD', completed: false },
  { id: 3, item: 'Effectiveness', completed: false },
  { id: 4, item: 'Ghost Pepper', completed: false },
  { id: 5, item: 'iJump?', completed: false },
  { id: 6, item: 'Klein Bottle', completed: false },
  { id: 7, item: 'Mnemonic', completed: false },
  { id: 8, item: 'Opossum', completed: false },
  { id: 9, item: 'QR Code', completed: false },
  { id: 10, item: 'String', completed: false }
];

/**
 * This function will get a reference to the title and set its text to the value
 * of the pageTitle variable that was set above.
 */
function setPageTitle()
{
  const heading = document.getElementById('title');
  heading.innerText = pageTitle;
}

/**
 * This function will loop over the array of groceries that was set above and add them to the DOM.
 */
function displayGroceries()
{
  const ul = document.querySelector('.shopping-list ul');

  groceries.forEach(grocery =>
  {
    const li = document.createElement('li');
    li.innerText = grocery.item;
    ul.appendChild(li);
  });
}

/**
 * This function will be called when the button is clicked. You will need to get a reference
 * to every list item and add the class completed to each one
 */
function markCompleted()
{
  let liAll = document.querySelectorAll('.shopping-list li');

  liAll.forEach(li =>
  {
    li.classList.add('completed');
  })
}

setPageTitle();

displayGroceries();

// Don't worry too much about what is going on here, we will cover this when we discuss events.
document.addEventListener('DOMContentLoaded', () =>
{
  // When the DOM Content has loaded attach a click listener to the button
  const button = document.querySelector('.btn');
  button.addEventListener('click', markCompleted);
});
