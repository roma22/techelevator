package com.techelevator;

import org.junit.*;
import static org.junit.Assert.*;

public class KataRomanNumeralsTest
{
	KataRomanNumerals testing;

	@Before
	public void setUp()
	{
		testing = new KataRomanNumerals();
	}

	@Test
	public void convertToRoman_ShouldReturnI_WhenValueIs1Test()
	{
		assertEquals("I", testing.convertToRoman(1));
	}

	@Test
	public void convertToRoman_ShouldReturnII_WhenValueIs2Test()
	{
		assertEquals("II", testing.convertToRoman(2));
	}

	@Test
	public void convertToRoman_ShouldReturnIII_WhenValueIs3Test()
	{
		assertEquals("III", testing.convertToRoman(3));
	}

	@Test
	public void convertToRoman_ShouldReturnIV_WhenValueIs4Test()
	{
		assertEquals("IV", testing.convertToRoman(4));
	}

	@Test
	public void convertToRoman_ShouldReturnV_WhenValueIs5Test()
	{
		assertEquals("V", testing.convertToRoman(5));
	}

	@Test
	public void convertToRoman_ShouldReturnVI_WhenValueIs6Test()
	{
		assertEquals("VI", testing.convertToRoman(6));
	}

	@Test
	public void convertToRoman_ShouldReturnVII_WhenValueIs7Test()
	{
		assertEquals("VII", testing.convertToRoman(7));
	}

	@Test
	public void convertToRoman_ShouldReturnVIII_WhenValueIs8Test()
	{
		assertEquals("VIII", testing.convertToRoman(8));
	}

	@Test
	public void convertToRoman_ShouldReturnIX_WhenValueIs9Test()
	{
		assertEquals("IX", testing.convertToRoman(9));
	}

	@Test
	public void convertToRoman_ShouldReturnX_WhenValueIs10Test()
	{
		assertEquals("X", testing.convertToRoman(10));
	}

	@Test
	public void convertToRoman_ShouldReturnXIX_WhenValueIs19Test()
	{
		assertEquals("XIX", testing.convertToRoman(19));
	}

	@Test
	public void convertToRoman_ShouldReturnXLIII_WhenValueIs43Test()
	{
		assertEquals("XLIII", testing.convertToRoman(43));
	}

	@Test
	public void convertToRoman_ShouldReturnXLIV_WhenValueIs44Test()
	{
		assertEquals("XLIV", testing.convertToRoman(44));
	}

	@Test
	public void convertToRoman_ShouldReturnXLV_WhenValueIs45Test()
	{
		assertEquals("XLV", testing.convertToRoman(45));
	}

	@Test
	public void convertToRoman_ShouldReturnXLVI_WhenValueIs46Test()
	{
		assertEquals("XLVI", testing.convertToRoman(46));
	}

	@Test
	public void convertToRoman_ShouldReturnXLIX_WhenValueIs49Test()
	{
		assertEquals("XLIX", testing.convertToRoman(49));
	}

	@Test
	public void convertToRoman_ShouldReturnXCIII_WhenValueIs93Test()
	{
		assertEquals("XCIII", testing.convertToRoman(93));
	}

	@Test
	public void convertToRoman_ShouldReturnXCIV_WhenValueIs94Test()
	{
		assertEquals("XCIV", testing.convertToRoman(94));
	}

	@Test
	public void convertToRoman_ShouldReturnXCV_WhenValueIs95Test()
	{
		assertEquals("XCV", testing.convertToRoman(95));
	}

	@Test
	public void convertToRoman_ShouldReturnXCVI_WhenValueIs96Test()
	{
		assertEquals("XCVI", testing.convertToRoman(96));
	}

	@Test
	public void convertToRoman_ShouldReturnXCIX_WhenValueIs99Test()
	{
		assertEquals("XCIX", testing.convertToRoman(99));
	}

	@Test
	public void convertToRoman_ShouldReturnCXI_WhenValueIs111Test()
	{
		assertEquals("CXI", testing.convertToRoman(111));
	}

	@Test
	public void convertToRoman_ShouldReturnCDXCIX_WhenValueIs499Test()
	{
		assertEquals("CDXCIX", testing.convertToRoman(499));
	}

	@Test
	public void convertToRoman_ShouldReturnCMXCIX_WhenValueIs999Test()
	{
		assertEquals("CMXCIX", testing.convertToRoman(999));
	}

	@Test
	public void convertToRoman_ShouldReturnM_WhenValueIs1000Test()
	{
		assertEquals("M", testing.convertToRoman(1000));
	}

	@Test
	public void convertToRoman_ShouldReturn_WhenValueIs1012Test()
	{
		assertEquals("MXII", testing.convertToRoman(1012));
	}

	@Test
	public void convertToRoman_ShouldReturn_WhenValueIs1098Test()
	{
		assertEquals("MXCVIII", testing.convertToRoman(1098));
	}

	@Test
	public void convertToRoman_ShouldReturn_WhenValueIs1109Test()
	{
		assertEquals("MCIX", testing.convertToRoman(1109));
	}

	@Test
	public void convertToRoman_ShouldReturn_WhenValueIs1123Test()
	{
		assertEquals("MCXXIII", testing.convertToRoman(1123));
	}

	@Test
	public void convertToRoman_ShouldReturn_WhenValueIs1210Test()
	{
		assertEquals("MCCX", testing.convertToRoman(1210));
	}

	@Test
	public void convertToRoman_ShouldReturn_WhenValueIs1234Test()
	{
		assertEquals("MCCXXXIV", testing.convertToRoman(1234));
	}

	@Test
	public void convertToRoman_ShouldReturn_WhenValueIs1321Test()
	{
		assertEquals("MCCCXXI", testing.convertToRoman(1321));
	}

	@Test
	public void convertToRoman_ShouldReturn_WhenValueIs1345Test()
	{
		assertEquals("MCCCXLV", testing.convertToRoman(1345));
	}

	@Test
	public void convertToRoman_ShouldReturn_WhenValueIs1432Test()
	{
		assertEquals("MCDXXXII", testing.convertToRoman(1432));
	}

	@Test
	public void convertToRoman_ShouldReturn_WhenValueIs1456Test()
	{
		assertEquals("MCDLVI", testing.convertToRoman(1456));
	}

	@Test
	public void convertToRoman_ShouldReturn_WhenValueIs1543Test()
	{
		assertEquals("MDXLIII", testing.convertToRoman(1543));
	}

	@Test
	public void convertToRoman_ShouldReturn_WhenValueIs1567Test()
	{
		assertEquals("MDLXVII", testing.convertToRoman(1567));
	}

	@Test
	public void convertToRoman_ShouldReturn_WhenValueIs1654Test()
	{
		assertEquals("MDCLIV", testing.convertToRoman(1654));
	}

	@Test
	public void convertToRoman_ShouldReturn_WhenValueIs1678Test()
	{
		assertEquals("MDCLXXVIII", testing.convertToRoman(1678));
	}

	@Test
	public void convertToRoman_ShouldReturn_WhenValueIs1765Test()
	{
		assertEquals("MDCCLXV", testing.convertToRoman(1765));
	}

	@Test
	public void convertToRoman_ShouldReturn_WhenValueIs1789Test()
	{
		assertEquals("MDCCLXXXIX", testing.convertToRoman(1789));
	}

	@Test
	public void convertToRoman_ShouldReturn_WhenValueIs1876Test()
	{
		assertEquals("MDCCCLXXVI", testing.convertToRoman(1876));
	}

	@Test
	public void convertToRoman_ShouldReturn_WhenValueIs1890Test()
	{
		assertEquals("MDCCCXC", testing.convertToRoman(1890));
	}

	@Test
	public void convertToRoman_ShouldReturn_WhenValueIs1901Test()
	{
		assertEquals("MCMI", testing.convertToRoman(1901));
	}

	@Test
	public void convertToRoman_ShouldReturn_WhenValueIs1987Test()
	{
		assertEquals("MCMLXXXVII", testing.convertToRoman(1987));
	}

	@Test
	public void convertToRoman_ShouldReturn_WhenValueIs1999Test()
	{
		assertEquals("MCMXCIX", testing.convertToRoman(1999));
	}

	@Test
	public void convertToRoman_ShouldReturnMM_WhenValueIs2000Test()
	{
		assertEquals("MM", testing.convertToRoman(2000));
	}

	@Test
	public void convertToRoman_ShouldReturn_WhenValueIs2111Test()
	{
		assertEquals("MMCXI", testing.convertToRoman(2111));
	}

	@Test
	public void convertToRoman_ShouldReturn_WhenValueIs2222Test()
	{
		assertEquals("MMCCXXII", testing.convertToRoman(2222));
	}

	@Test
	public void convertToRoman_ShouldReturn_WhenValueIs2333Test()
	{
		assertEquals("MMCCCXXXIII", testing.convertToRoman(2333));
	}

	@Test
	public void convertToRoman_ShouldReturn_WhenValueIs2444Test()
	{
		assertEquals("MMCDXLIV", testing.convertToRoman(2444));
	}

	@Test
	public void convertToRoman_ShouldReturn_WhenValueIs2555Test()
	{
		assertEquals("MMDLV", testing.convertToRoman(2555));
	}

	@Test
	public void convertToRoman_ShouldReturn_WhenValueIs2666Test()
	{
		assertEquals("MMDCLXVI", testing.convertToRoman(2666));
	}

	@Test
	public void convertToRoman_ShouldReturn_WhenValueIs2777Test()
	{
		assertEquals("MMDCCLXXVII", testing.convertToRoman(2777));
	}

	@Test
	public void convertToRoman_ShouldReturn_WhenValueIs2888Test()
	{
		assertEquals("MMDCCCLXXXVIII", testing.convertToRoman(2888));
	}

	@Test
	public void convertToRoman_ShouldReturn_WhenValueIs2999Test()
	{
		assertEquals("MMCMXCIX", testing.convertToRoman(2999));
	}

	/*
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * BREAK BETWEEN CONVERSION SYSTEMS
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 */

	@Test
	public void convertToDigit_ShouldReturn1_WhenValueIsITest()
	{
		assertEquals(new Integer(1), testing.convertToDigit("I"));
	}

	@Test
	public void convertToDigit_ShouldReturn2_WhenValueIsIITest()
	{
		assertEquals(new Integer(2), testing.convertToDigit("II"));
	}

	@Test
	public void convertToDigit_ShouldReturn3_WhenValueIsIIITest()
	{
		assertEquals(new Integer(3), testing.convertToDigit("III"));
	}

	@Test
	public void convertToDigit_ShouldReturn4_WhenValueIsIVTest()
	{
		assertEquals(new Integer(4), testing.convertToDigit("IV"));
	}

	@Test
	public void convertToDigit_ShouldReturn5_WhenValueIsVTest()
	{
		assertEquals(new Integer(5), testing.convertToDigit("V"));
	}

	@Test
	public void convertToDigit_ShouldReturn6_WhenValueIsVITest()
	{
		assertEquals(new Integer(6), testing.convertToDigit("VI"));
	}

	@Test
	public void convertToDigit_ShouldReturn7_WhenValueIsVIITest()
	{
		assertEquals(new Integer(7), testing.convertToDigit("VII"));
	}

	@Test
	public void convertToDigit_ShouldReturn8_WhenValueIsVIIITest()
	{
		assertEquals(new Integer(8), testing.convertToDigit("VIII"));
	}

	@Test
	public void convertToDigit_ShouldReturn9_WhenValueIsIXTest()
	{
		assertEquals(new Integer(9), testing.convertToDigit("IX"));
	}

	@Test
	public void convertToDigit_ShouldReturn10_WhenValueIsXTest()
	{
		assertEquals(new Integer(10), testing.convertToDigit("X"));
	}

	@Test
	public void convertToDigit_ShouldReturn19_WhenValueIsXIXTest()
	{
		assertEquals(new Integer(19), testing.convertToDigit("XIX"));
	}

	@Test
	public void convertToDigit_ShouldReturn43_WhenValueIsXLIIITest()
	{
		assertEquals(new Integer(43), testing.convertToDigit("XLIII"));
	}

	@Test
	public void convertToDigit_ShouldReturn44_WhenValueIsXLIVTest()
	{
		assertEquals(new Integer(44), testing.convertToDigit("XLIV"));
	}

	@Test
	public void convertToDigit_ShouldReturn45_WhenValueIsXLVTest()
	{
		assertEquals(new Integer(45), testing.convertToDigit("XLV"));
	}

	@Test
	public void convertToDigit_ShouldReturn46_WhenValueIsXLVITest()
	{
		assertEquals(new Integer(46), testing.convertToDigit("XLVI"));
	}

	@Test
	public void convertToDigit_ShouldReturn49_WhenValueIsXLIXTest()
	{
		assertEquals(new Integer(49), testing.convertToDigit("XLIX"));
	}

	@Test
	public void convertToDigit_ShouldReturn93_WhenValueIsXCIIITest()
	{
		assertEquals(new Integer(93), testing.convertToDigit("XCIII"));
	}

	@Test
	public void convertToDigit_ShouldReturn94_WhenValueIsXCIVTest()
	{
		assertEquals(new Integer(94), testing.convertToDigit("XCIV"));
	}

	@Test
	public void convertToDigit_ShouldReturn95_WhenValueIsXCVTest()
	{
		assertEquals(new Integer(95), testing.convertToDigit("XCV"));
	}

	@Test
	public void convertToDigit_ShouldReturn96_WhenValueIsXCVITest()
	{
		assertEquals(new Integer(96), testing.convertToDigit("XCVI"));
	}

	@Test
	public void convertToDigit_ShouldReturn99_WhenValueIsXCIXTest()
	{
		assertEquals(new Integer(99), testing.convertToDigit("XCIX"));
	}

	@Test
	public void convertToDigit_ShouldReturn111_WhenValueIsCXITest()
	{
		assertEquals(new Integer(111), testing.convertToDigit("CXI"));
	}

	@Test
	public void convertToDigit_ShouldReturn499_WhenValueIsCDXCIXTest()
	{
		assertEquals(new Integer(499), testing.convertToDigit("CDXCIX"));
	}

	@Test
	public void convertToDigit_ShouldReturn999_WhenValueIsCMXCIXTest()
	{
		assertEquals(new Integer(999), testing.convertToDigit("CMXCIX"));
	}
	
	@Test
	public void convertToDigit_ShouldReturn1000_WhenValueIsMTest()
	{
		assertEquals(new Integer(1000), testing.convertToDigit("M"));
	}
	
	@Test
	public void convertToDigit_ShouldReturn1135_WhenValueIsMTest()
	{
		assertEquals(new Integer(1135), testing.convertToDigit("MCXXXV"));
	}
	
	@Test
	public void convertToDigit_ShouldReturn1246_WhenValueIsMTest()
	{
		assertEquals(new Integer(1246), testing.convertToDigit("MCCXLVI"));
	}
	
	@Test
	public void convertToDigit_ShouldReturn1357_WhenValueIsMTest()
	{
		assertEquals(new Integer(1357), testing.convertToDigit("MCCCLVII"));
	}
	
	@Test
	public void convertToDigit_ShouldReturn1579_WhenValueIsMTest()
	{
		assertEquals(new Integer(1579), testing.convertToDigit("MDLXXIX"));
	}
	
	@Test
	public void convertToDigit_ShouldReturn2000_WhenValueIsMMTest()
	{
		assertEquals(new Integer(2000), testing.convertToDigit("MM"));
	}
	
	@Test
	public void convertToDigit_ShouldReturn2135_WhenValueIsMMTest()
	{
		assertEquals(new Integer(2135), testing.convertToDigit("MMCXXXV"));
	}

	@Test
	public void convertToDigit_ShouldReturn2246_WhenValueIsMMTest()
	{
		assertEquals(new Integer(2246), testing.convertToDigit("MMCCXLVI"));
	}

	@Test
	public void convertToDigit_ShouldReturn2357_WhenValueIsMMTest()
	{
		assertEquals(new Integer(2357), testing.convertToDigit("MMCCCLVII"));
	}

	@Test
	public void convertToDigit_ShouldReturn2579_WhenValueIsMMTest()
	{
		assertEquals(new Integer(2579), testing.convertToDigit("MMDLXXIX"));
	}

	@Test
	public void convertToDigit_ShouldReturn2987_WhenValueIsMMTest()
	{
		assertEquals(new Integer(2987), testing.convertToDigit("MMCMLXXXVII"));
	}
}
