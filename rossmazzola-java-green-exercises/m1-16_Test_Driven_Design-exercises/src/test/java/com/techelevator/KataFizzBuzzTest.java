package com.techelevator;

import org.junit.*;
import static org.junit.Assert.*;

public class KataFizzBuzzTest
{
	KataFizzBuzz testing;

	@Before
	public void setUp()
	{
		testing = new KataFizzBuzz();
	}

	@Test
	public void fizzBuzz_ShouldReturnEmptyString_WhenValueIsLessThanOneTest()
	{
		assertEquals("", testing.fizzBuzz(0));
		assertEquals("", testing.fizzBuzz(-1));
	}

	@Test
	public void fizzBuzz_ShouldReturnEmptyString_WhenValueIsMoreThan100Test()
	{
		assertEquals("", testing.fizzBuzz(101));
		assertEquals("", testing.fizzBuzz(102));
	}

	@Test
	public void fizzBuzz_ShouldReturnFizzBuzz_WhenValueIsMultipleOfThreeAndFiveTest()
	{
		assertEquals("FizzBuzz", testing.fizzBuzz(15));
		assertEquals("FizzBuzz", testing.fizzBuzz(30));
	}

	@Test
	public void fizzBuzz_ShouldReturnFizz_WhenValueIsMultipleOfThreeTest()
	{
		assertEquals("Fizz", testing.fizzBuzz(3));
		assertEquals("Fizz", testing.fizzBuzz(6));
	}

	@Test
	public void fizzBuzz_ShouldReturnBuzz_WhenValueIsMultipleOfFiveTest()
	{
		assertEquals("Buzz", testing.fizzBuzz(5));
		assertEquals("Buzz", testing.fizzBuzz(10));
	}

	@Test
	public void fizzBuzz_ShouldReturnInputValue_WhenValueIsNotMultipleOfThreeOrFiveTest()
	{
		assertEquals("1", testing.fizzBuzz(1));
		assertEquals("2", testing.fizzBuzz(2));
		assertEquals("7", testing.fizzBuzz(7));
	}
}
