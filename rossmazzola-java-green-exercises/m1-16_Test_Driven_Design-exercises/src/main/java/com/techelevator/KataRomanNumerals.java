package com.techelevator;

import java.util.HashMap;
import java.util.Map;

public class KataRomanNumerals
{
	private Map<Integer, String> romanMap;
	private Map<String, Integer> digitMap;

	public KataRomanNumerals()
	{
		romanMap = new HashMap<Integer, String>();
		digitMap = new HashMap<String, Integer>();
	}

	public String convertToRoman(int number)
	{
		String romanFormat = "";
		loadRomanMap();

		int position = 1000;
		int evenlyDividedTimes = number / position;
		int remainder = number;

		for (int i = 0; i < 3; i++)
		{
			evenlyDividedTimes = remainder / position;

			if (evenlyDividedTimes > 0)
			{
				romanFormat += romanMap.get(new Integer(position * evenlyDividedTimes));
			}
			remainder = remainder % position;
			position /= 10;
		}

		if (remainder != 0)
		{
			romanFormat += romanMap.get(new Integer(remainder));
		}

		return romanFormat;
	}

	public Integer convertToDigit(String value)
	{
		String[] chars = value.split("|");
		Integer digitFormat = 0;
		loadDigitMap();

		for (int i = 0; i < chars.length; i++)
		{
			if (i + 1 < chars.length)
			{
				if (digitMap.get(chars[i]) >= digitMap.get(chars[i + 1]))
				{
					digitFormat += digitMap.get(chars[i]);
				}
				else
				{
					digitFormat += digitMap.get(chars[i + 1]) - digitMap.get(chars[i]);
					++i;
				}
			}
			else
			{
				digitFormat += digitMap.get(chars[i]);
				++i;
			}
		}
		return digitFormat;
	}

	private void loadRomanMap()
	{
		romanMap.put(new Integer(1), "I"); // 1
		romanMap.put(new Integer(2), "II"); // 1
		romanMap.put(new Integer(3), "III"); // 1
		romanMap.put(new Integer(4), "IV"); // 1
		romanMap.put(new Integer(5), "V"); // 1
		romanMap.put(new Integer(6), "VI"); // 1
		romanMap.put(new Integer(7), "VII"); // 1
		romanMap.put(new Integer(8), "VIII"); // 1
		romanMap.put(new Integer(9), "IX"); // 1

		romanMap.put(new Integer(10), "X"); // 10
		romanMap.put(new Integer(20), "XX"); // 10
		romanMap.put(new Integer(30), "XXX"); // 10
		romanMap.put(new Integer(40), "XL"); // 10
		romanMap.put(new Integer(50), "L"); // 10
		romanMap.put(new Integer(60), "LX"); // 10
		romanMap.put(new Integer(70), "LXX"); // 10
		romanMap.put(new Integer(80), "LXXX"); // 10
		romanMap.put(new Integer(90), "XC"); // 10

		romanMap.put(new Integer(100), "C"); // 100
		romanMap.put(new Integer(200), "CC"); // 100
		romanMap.put(new Integer(300), "CCC"); // 100
		romanMap.put(new Integer(400), "CD"); // 100
		romanMap.put(new Integer(500), "D"); // 100
		romanMap.put(new Integer(600), "DC"); // 100
		romanMap.put(new Integer(700), "DCC"); // 100
		romanMap.put(new Integer(800), "DCCC"); // 100
		romanMap.put(new Integer(900), "CM"); // 100

		romanMap.put(new Integer(1000), "M"); // 1000
		romanMap.put(new Integer(2000), "MM"); // 1000
		romanMap.put(new Integer(3000), "MMM"); // 1000
	}

	private void loadDigitMap()
	{
		digitMap.put("I", new Integer(1)); // 1
		digitMap.put("V", new Integer(5)); // 1
		digitMap.put("X", new Integer(10)); // 10
		digitMap.put("L", new Integer(50)); // 10
		digitMap.put("C", new Integer(100)); // 100
		digitMap.put("D", new Integer(500)); // 100
		digitMap.put("M", new Integer(1000)); // 1000
	}

}
