package com.techelevator;

public class KataFizzBuzz
{
	public String fizzBuzz(int number)
	{
		Integer numFB = number;
		String strFB = numFB.toString();

		if (number < 1 || number > 100)
		{
			strFB = "";
		}
		else if (number % 15 == 0)
		{
			strFB = "FizzBuzz";
		}
		else if (number % 3 == 0)
		{
			strFB = "Fizz";
		}
		else if (number % 5 == 0)
		{
			strFB = "Buzz";
		}
		return strFB;
	}

}
