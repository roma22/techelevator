package com.techelevator.dog;

public class Dog
{

	private boolean sleeping;

	public String makeSound()
	{
		return sleeping ? "Zzzzz..." : "Woof!";
	}

	public void sleep()
	{
		sleeping = true;
	}

	public void wakeUp()
	{
		sleeping = false;
	}

	public boolean isSleeping()
	{
		return sleeping;
	}

}
