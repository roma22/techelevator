package com.techelevator.product;

public class Product
{

	String name = "";
	double price = 0;
	double weightInOunces = 0;

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public double getPrice()
	{
		return price;
	}

	public void setPrice(double price)
	{
		this.price = price;
	}

	public double getWeightInOunces()
	{
		return weightInOunces;
	}

	public void setWeightInOunces(double weightInOunces)
	{
		this.weightInOunces = weightInOunces;
	}

}
