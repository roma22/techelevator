package com.techelevator;

public class SalaryWorker implements IWorker {

	private String firstName;
	private String lastName;
	private double annualSalary;

	SalaryWorker(String firstName, String lastName, double annualSalary) {

		this.annualSalary = annualSalary;
		this.firstName = firstName;
		this.lastName = lastName;
	}

	@Override
	public double calculateWeeklyPay(int hoursWorked) {

		return (annualSalary / 52);
	}

	public double getAnnualSalary() {

		return annualSalary;
	}

	@Override
	public String firstName() {
		// TODO Auto-generated method stub
		return firstName;
	}

	@Override
	public String lastName() {
		// TODO Auto-generated method stub
		return lastName;
	}
}
