package com.techelevator;

public class HourlyWorker implements IWorker {

	private double hourlyRate;
	private String firstName;
	private String lastName;

	HourlyWorker(String firstName, String lastName, double hourlyRate) {

		this.hourlyRate = hourlyRate;
		this.firstName = firstName;
		this.lastName = lastName;

	}

	@Override
	public double calculateWeeklyPay(int hoursWorked) {

		double pay = hourlyRate * hoursWorked;

		if (hoursWorked > 40) {

			int overtime = hoursWorked - 40;
			pay = pay + (hourlyRate * overtime * .5);

		}
		return pay;
	}

	public double getHourlyRate() {

		return hourlyRate;
	}

	@Override
	public String firstName() {
		// TODO Auto-generated method stub
		return firstName;
	}

	@Override
	public String lastName() {
		// TODO Auto-generated method stub
		return lastName;
	}

}
