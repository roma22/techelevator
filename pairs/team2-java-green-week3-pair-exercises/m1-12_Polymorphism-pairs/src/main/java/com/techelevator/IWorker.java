package com.techelevator;

public interface IWorker {

	String firstName();

	String lastName();

	double calculateWeeklyPay(int hoursWorked);

}
