/*Following the approach discussed in the lecture, create a List that represents a company's payroll and holds a collection of workers in it. The objective will be to:

output each employee, the number of hours they've worked (you can use a random number generator), and the amount they will be paid for the week
at the end show the sum of total hours worked and total weekly payroll*/

package com.techelevator;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.math.RoundingMode;

public class Program {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// String lastName = "a;
		// String firstName = "b";

		List<IWorker> payroll = new ArrayList<IWorker>();
		Random rand = new Random();

		SalaryWorker minnie = new SalaryWorker("Minnie", "Mouse", 54000);
		VolunteerWorker goofy = new VolunteerWorker("George (Goofy)", "Geef");
		HourlyWorker daisy = new HourlyWorker("Daisy", "Duck", 15.00);
		HourlyWorker mickey = new HourlyWorker("Mickey", "Mouse", 10.00);

		payroll.add(mickey);
		payroll.add(goofy);
		payroll.add(daisy);
		payroll.add(minnie);

		System.out.format("%-20s %-22s %s %s", "Employee", "Hours Worked", "Pay", "\n");
		System.out.println("===================================================");

		int totalHours = 0;
		BigDecimal totalPay = new BigDecimal("0");

		for (IWorker var : payroll) {
			int hours = rand.nextInt(115);
			totalHours += hours;

			BigDecimal pay = new BigDecimal(var.calculateWeeklyPay(hours)).setScale(2, BigDecimal.ROUND_HALF_EVEN);
			totalPay = totalPay.add(pay);

			System.out.format("%-20s %-22s %s %s", var.lastName() + ", " + var.firstName(), hours, pay, "\n");

		}
		System.out.println("\nTotal Hours: " + totalHours);
		System.out.println("Total Pay: " + "$" + totalPay);
	}

}
