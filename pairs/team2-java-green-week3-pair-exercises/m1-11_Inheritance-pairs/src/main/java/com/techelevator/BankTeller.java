package com.techelevator;

import java.math.BigDecimal;

public class BankTeller extends BankAccount
{

	public static void main(String[] args)
	{
		BankAccount checkingAccount = new CheckingAccount();

		BankAccount savingsAccount = new SavingsAccount();

		BankCustomer jayGatsby = new BankCustomer();

		jayGatsby.addAccount(checkingAccount);
		jayGatsby.addAccount(savingsAccount);

		BigDecimal amountToDeposit = new BigDecimal("1000.00");

		BigDecimal amountToWithdraw = new BigDecimal("100.00");

		BigDecimal amountToTransfer = new BigDecimal("100.00");

		System.out.println("---DEPOSITS---");

		checkingAccount.deposit(amountToDeposit);
		System.out.println("Checking Account after Deposit: " + checkingAccount.getBalance());

		savingsAccount.deposit(amountToDeposit);
		System.out.println("Savings Account after Deposit: " + savingsAccount.getBalance());

		System.out.println("\n---WITHDRAWLS---");

		checkingAccount.withdraw(amountToWithdraw);
		System.out.println("Checking Account after Withdraw: " + checkingAccount.getBalance());

		savingsAccount.withdraw(amountToWithdraw);
		System.out.println("Savings Account after Withdraw: " + savingsAccount.getBalance());

		System.out.println("\n---TRANSFERS---");

		checkingAccount.transfer(savingsAccount, amountToTransfer);
		System.out.println("Checking Account after Transfer: " + checkingAccount.getBalance());

		savingsAccount.transfer(checkingAccount, amountToTransfer);
		System.out.println("Savings Account after Transfer: " + savingsAccount.getBalance());

		System.out.println("\n---ACCOUNTS---");

		System.out.println(String.format("Jay Gatsby has %s accounts.", jayGatsby.getAccounts().size()));

		System.out.println("\n---WHOISVIP---");

		System.out.println("Jay Gatsby: " + jayGatsby.getIsVIP());

	}

}
