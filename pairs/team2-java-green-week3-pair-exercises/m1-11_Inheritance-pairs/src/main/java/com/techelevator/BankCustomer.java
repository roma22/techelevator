package com.techelevator;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class BankCustomer
{
	private String name;
	private String address;
	private String phoneNumber;
	private BigDecimal accountSum = new BigDecimal("0");
	private List<BankAccount> accounts = new ArrayList<BankAccount>();

	public BankCustomer()
	{

	}
	
	public void addAccount(BankAccount newAccount)
	{
		accounts.add(newAccount);
	}

	public BigDecimal getAccountSum()
	{
		for(BankAccount var : accounts)
		{
			accountSum = var.getBalance().add(accountSum);
		}
		return accountSum;
	}
	
	public boolean getIsVIP()
	{
		BigDecimal amountVIP = new BigDecimal("25000");
		
		if(getAccountSum().compareTo(amountVIP) == -1)
		{
			return false;
		}
		else
		{
			return true;
		}
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getAddress()
	{
		return address;
	}

	public void setAddress(String address)
	{
		this.address = address;
	}

	public String getPhoneNumber()
	{
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber)
	{
		this.phoneNumber = phoneNumber;
	}

	public List<BankAccount> getAccounts()
	{
		return accounts;
	}

}
