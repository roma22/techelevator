package com.techelevator;

import java.math.BigDecimal;

public class CheckingAccount extends BankAccount
{
	private BigDecimal overdraftFee= new BigDecimal("10.00");
	
	public CheckingAccount()
	{

	}
	
	public BigDecimal withdraw(BigDecimal amountToWithdraw)
	{
		if (super.getBalance().compareTo(new BigDecimal("-100.00")) == -1 ||
				super.getBalance().compareTo(new BigDecimal("-100.00")) == 0)
		{
			return getBalance();
		}
		else if (super.getBalance().compareTo(amountToWithdraw) == -1)
		{
			super.withdraw(amountToWithdraw.add(overdraftFee));
		}
		else if (super.getBalance().compareTo(amountToWithdraw) == 1 ||
				super.getBalance().compareTo(amountToWithdraw) == 0)
		{
			super.withdraw(amountToWithdraw);
		}
		return getBalance();
	}

}
