 package com.techelevator;

import java.math.BigDecimal;

public class SavingsAccount extends BankAccount
{
	private BigDecimal serviceCharge = new BigDecimal("2.00");

	public SavingsAccount()
	{

	}

	public BigDecimal withdraw(BigDecimal amountToWithdraw)
	{
		if (super.getBalance().compareTo(amountToWithdraw) == -1)
		{
			return getBalance();
		}
		else if (super.getBalance().compareTo(new BigDecimal("150.00")) == -1)
		{
			super.withdraw(amountToWithdraw.add(serviceCharge));
		}
		else if (super.getBalance().compareTo(new BigDecimal("150.00")) == 1 ||
				super.getBalance().compareTo(new BigDecimal("150.00")) == 0)
		{
			super.withdraw(amountToWithdraw);
		}
		return getBalance();
	}

}
