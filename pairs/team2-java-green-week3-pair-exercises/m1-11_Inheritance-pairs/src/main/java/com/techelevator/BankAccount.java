package com.techelevator;

import java.math.BigDecimal;

public class BankAccount
{
	private String accountNumber;
	private BigDecimal balance = new BigDecimal("0.00");

	public BankAccount()
	{

	}

	public BigDecimal deposit(BigDecimal amountToDeposit)
	{
		this.balance = balance.add(amountToDeposit);
		return balance;
	}

	public BigDecimal withdraw(BigDecimal amountToWithdraw)
	{
		this.balance = balance.subtract(amountToWithdraw);
		return balance;
	}

	public void transfer(BankAccount destinationAccount, BigDecimal transferAmount)
	{
		this.balance = withdraw(transferAmount);
		destinationAccount.balance = destinationAccount.deposit(transferAmount);
	}

	public String getAccountNumber()
	{
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber)
	{
		this.accountNumber = accountNumber;
	}

	public BigDecimal getBalance()
	{
		return balance;
	}

	private void setBalance(BigDecimal balance)
	{
		this.balance = balance;
	}

}
