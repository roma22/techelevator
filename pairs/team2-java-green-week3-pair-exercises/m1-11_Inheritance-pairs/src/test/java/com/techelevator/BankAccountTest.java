package com.techelevator;

import static org.junit.Assert.*;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;

public class BankAccountTest
{
	BankAccount testing;
	BankAccount testingTransfer;

	@Before
	public void setUp() throws Exception
	{
		testing = new BankAccount();
		testing.deposit(new BigDecimal("1000"));
	}

	@Test
	public void deposit_ShouldDecreaseByOne_WhenNegativeOneIsDepositedTest()
	{
		assertEquals(new BigDecimal("999.00"), testing.deposit(new BigDecimal("-1.00")));
	}

	@Test
	public void deposit_ShouldIncreaseByZero_WhenZeroIsDepositedTest()
	{
		assertEquals(new BigDecimal("1000.00"), testing.deposit(new BigDecimal("0.00")));
	}

	@Test
	public void deposit_ShouldIncreaseBy50Cents_WhenPositiveFiftyCentsIsDepositedTest()
	{
		assertEquals(new BigDecimal("1000.50"), testing.deposit(new BigDecimal("0.50")));
	}

	@Test
	public void deposit_ShouldIncreaseByOne_WhenPositiveOneIsDepositedTest()
	{
		assertEquals(new BigDecimal("1001.00"), testing.deposit(new BigDecimal("1.00")));
	}

	@Test
	public void withdraw_ShouldIncreaseByOne_WhenNegativeOneIsWithdrawnTest()
	{
		assertEquals(new BigDecimal("1001.00"), testing.withdraw(new BigDecimal("-1.00")));
	}

	@Test
	public void withdraw_ShouldDecreaseByZero_WhenZeroIsWithdrawnTest()
	{
		assertEquals(new BigDecimal("1000.00"), testing.withdraw(new BigDecimal("0.00")));
	}

	@Test
	public void withdraw_ShouldDecreaseBy50Cents_WhenPositiveFiftyCentsIsWithdrawnTest()
	{
		assertEquals(new BigDecimal("999.50"), testing.withdraw(new BigDecimal("0.50")));
	}

	@Test
	public void withdraw_ShouldDecreaseByOne_WhenPositiveOneisWithdrawnTest()
	{
		assertEquals(new BigDecimal("999.00"), testing.withdraw(new BigDecimal("1.00")));
	}

	@Test
	public void transfer_ShouldDecreaseDestinationAccountByOne_WhenNegativeOneIsTransferredTest()
	{
		// Arrange
		testingTransfer = new BankAccount();

		// Act
		testingTransfer.deposit(new BigDecimal("1000"));
		testing.transfer(testingTransfer, new BigDecimal("-1.00"));

		// Assert
		assertEquals(new BigDecimal("999.00"), testingTransfer.getBalance());
		assertEquals(new BigDecimal("1001.00"), testing.getBalance());
	}

	@Test
	public void transfer_ShouldIncreaseDestinationAccountByZero_WhenZeroIsTransferredTest()
	{
		testingTransfer = new BankAccount();
		testingTransfer.deposit(new BigDecimal("1000"));

		testing.transfer(testingTransfer, new BigDecimal("0.00"));

		assertEquals(new BigDecimal("1000.00"), testingTransfer.getBalance());
		assertEquals(new BigDecimal("1000.00"), testing.getBalance());
	}

	@Test
	public void transfer_ShouldIncreaseDestinationAccountBy50Cents_WhenPositive50CentsIsTransferredTest()
	{
		testingTransfer = new BankAccount();
		testingTransfer.deposit(new BigDecimal("1000"));

		testing.transfer(testingTransfer, new BigDecimal("0.50"));

		assertEquals(new BigDecimal("1000.50"), testingTransfer.getBalance());
		assertEquals(new BigDecimal("999.50"), testing.getBalance());
	}

	@Test
	public void transfer_ShouldIncreaseDestinationAccountByOne_WhenPositiveOneIsTransferredTest()
	{
		testingTransfer = new BankAccount();
		testingTransfer.deposit(new BigDecimal("1000"));

		testing.transfer(testingTransfer, new BigDecimal("1.00"));

		assertEquals(new BigDecimal("1001.00"), testingTransfer.getBalance());
		assertEquals(new BigDecimal("999.00"), testing.getBalance());
	}
}
