package com.techelevator;

import static org.junit.Assert.*;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;

public class CheckingAccountTest
{
	@Before
	public void setUp() throws Exception
	{

	}

	@Test
	public void withdraw_ShouldSucceedAsNormal_WhenWithdrawAmountIsLessThanAccountBalanceTest()
	{
		CheckingAccount testing = new CheckingAccount();

		testing.deposit(new BigDecimal("1000.00"));

		assertEquals(new BigDecimal("990.00"), testing.withdraw(new BigDecimal("10.00")));
	}

	@Test
	public void withdraw_ShouldResultInOverdraftFee_WhenAccountIsOverdrawnTest()
	{
		CheckingAccount testingOverdraftFee = new CheckingAccount();

		testingOverdraftFee.deposit(new BigDecimal("1.00"));

		assertEquals(new BigDecimal("-19.00"), testingOverdraftFee.withdraw(new BigDecimal("10.00")));
	}

	@Test
	public void withdraw_ShouldFail_WhenBalanceIsEqualToNegative100Test()
	{
		CheckingAccount testingNegative100 = new CheckingAccount();

		testingNegative100.deposit(new BigDecimal("-100.00"));

		assertEquals(new BigDecimal("-100.00"), testingNegative100.withdraw(new BigDecimal("10.00")));
	}

	@Test
	public void withdraw_ShouldFail_WhenBalanceIsBelowNegative100Test()
	{
		CheckingAccount testingNegative101 = new CheckingAccount();

		testingNegative101.deposit(new BigDecimal("-101.00"));

		assertEquals(new BigDecimal("-101.00"), testingNegative101.withdraw(new BigDecimal("10.00")));
	}

	@Test
	public void withdraw_ShouldSucceedButResultInOverdraftFee_WhenBalanceIsAboveNegative100Test()
	{
		CheckingAccount testingNegative99 = new CheckingAccount();

		testingNegative99.deposit(new BigDecimal("-99.00"));

		assertEquals(new BigDecimal("-119.00"), testingNegative99.withdraw(new BigDecimal("10.00")));
	}
}
