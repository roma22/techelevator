package com.techelevator;

import static org.junit.Assert.*;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;

public class SavingsAccountTest
{
	@Before
	public void setUp() throws Exception
	{

	}

	@Test
	public void withdraw_ShouldSucceedAsNormal_WhenWithdrawAmountIsLessThanAccountBalanceTest()
	{
		SavingsAccount testing = new SavingsAccount();

		testing.deposit(new BigDecimal("1000.00"));

		assertEquals(new BigDecimal("990.00"), testing.withdraw(new BigDecimal("10.00")));
	}

	@Test
	public void withdraw_ShouldFail_WhenWithdrawAmountIsMoreThanAccountBalanceTest()
	{
		SavingsAccount testingWithdrawFail = new SavingsAccount();

		testingWithdrawFail.deposit(new BigDecimal("1.00"));

		assertEquals(new BigDecimal("1.00"), testingWithdrawFail.withdraw(new BigDecimal("2.00")));
	}

	@Test
	public void withdraw_ShouldNotResultInServiceCharge_WhenBalanceIsAbove150Test()
	{
		SavingsAccount testingAbove150 = new SavingsAccount();

		testingAbove150.deposit(new BigDecimal("151.00"));

		assertEquals(new BigDecimal("150.00"), testingAbove150.withdraw(new BigDecimal("1.00")));
	}

	@Test
	public void withdraw_ShouldResultInServiceCharge_WhenBalanceIsEqualTo150Test()
	{
		SavingsAccount testingEqualTo150 = new SavingsAccount();

		testingEqualTo150.deposit(new BigDecimal("150.00"));

		assertEquals(new BigDecimal("149.00"), testingEqualTo150.withdraw(new BigDecimal("1.00")));
	}

	@Test
	public void withdraw_ShouldResultInServiceCharge_WhenBalanceIsBelow150Test()
	{
		SavingsAccount testingBelow150 = new SavingsAccount();

		testingBelow150.deposit(new BigDecimal("149.00"));

		assertEquals(new BigDecimal("146.00"), testingBelow150.withdraw(new BigDecimal("1.00")));
	}
}
