package com.techelevator;

import static org.junit.Assert.*;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;

public class BankCustomerTest
{
	BankCustomer customer;

	@Before
	public void setUp() throws Exception
	{
		customer = new BankCustomer();
	}

	@Test
	public void addAccount_ShouldAddNewCheckingAccountToCustomerTest()
	{
		CheckingAccount checking = new CheckingAccount();

		customer.addAccount(checking);

		assertEquals(1, customer.getAccounts().size());
	}

	@Test
	public void addAccount_ShouldAddNewSavingsAccountToCustomerTest()
	{
		SavingsAccount savings = new SavingsAccount();

		customer.addAccount(savings);

		assertEquals(1, customer.getAccounts().size());
	}

	@Test
	public void addAccount_ShouldAddNewCheckingAndSavingsAccountToCustomerTest()
	{
		CheckingAccount checking = new CheckingAccount();
		SavingsAccount savings = new SavingsAccount();

		customer.addAccount(checking);
		customer.addAccount(savings);

		assertEquals(2, customer.getAccounts().size());
	}

	@Test
	public void getIsVIP_ShouldReturnTrue_WhenTotalAccountSumIsAtLeast25000Test()
	{
		BankAccount account = new BankAccount();

		account.deposit(new BigDecimal("25000.00"));
		customer.addAccount(account);

		assertTrue(customer.getIsVIP());
	}

	@Test
	public void getIsVIP_ShouldReturnFalse_WhenTotalAccountSumIsNotAtLeast25000Test()
	{
		BankAccount account = new BankAccount();

		account.deposit(new BigDecimal("24999.99"));
		customer.addAccount(account);

		assertFalse(customer.getIsVIP());
	}

}
