<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>

<html>
<head>
<meta name="viewport" content="width=device-width" />
<title>Recipe Table View</title>
<link rel="stylesheet" href="css/site.css" />
</head>
<body>
	<header>
		<h1>MVC Exercises - Views Part 2: Models</h1>
	</header>
	<nav>
		<ul>
			<li><a href="recipeTiles">Tile Layout</a></li>
			<li><a href="recipeTable">Table Layout</a></li>
		</ul>

	</nav>
	<section id="main-content">

		<!-- Use the request attribute "recipes" (List<Recipe>) -->
		<h1>Ross's "Recipes"</h1>
		<div class="recipeTable">
			<div id="imgs">
				<c:url var="imageUrl" value="img/recipe0.jpg" />
				<div id="img1">
					<img src="${ imageUrl }" />
				</div>

				<c:url var="imageUrl2" value="img/recipe1.jpg" />
				<div id="img2">
					<img src="${ imageUrl2 }" />
				</div>

				<c:url var="imageUrl3" value="img/recipe2.jpg" />
				<div id="img3">
					<img src="${ imageUrl3 }" />
				</div>
			</div>
			<table>
				<tr>
					<th>Name</th>
					<c:forEach var="recipe" items="${recipes}">
						<td>${recipe.name}</td>
					</c:forEach>
				</tr>

				<tr>
					<th>Type</th>
					<c:forEach var="recipe" items="${recipes}">
						<td>${recipe.recipeType}</td>
					</c:forEach>
				</tr>
				<tr>
					<th>Cook Time</th>
					<c:forEach var="recipe" items="${recipes}">
						<td>${recipe.cookTimeInMinutes}</td>
					</c:forEach>
				</tr>
				<tr>
					<th>Ingredients</th>
					<c:forEach var="recipe" items="${recipes}">
						<td>${recipe.ingredients.size()}Ingredients</td>
					</c:forEach>
				</tr>
				<tr>
					<th>Rating</th>
					<c:forEach var="recipe" items="${recipes}">
						<td><fmt:formatNumber var="rating" value="${ recipe.averageRating }" maxFractionDigits="0" /> <c:url
								var="ratingUrl" value="/img/${ rating }-star.png" />
							<div id="rating">
								<img src="${ ratingUrl }" />
							</div></td>
					</c:forEach>
				</tr>

			</table>
		</div>
	</section>
</body>
</html>