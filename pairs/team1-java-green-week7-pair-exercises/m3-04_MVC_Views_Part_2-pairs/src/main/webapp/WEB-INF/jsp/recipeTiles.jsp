<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>

<html>
<head>
<meta name="viewport" content="width=device-width" />
<title>Recipe Tile View</title>
<link rel="stylesheet" href="css/site.css" />
</head>
<body>
	<header>
		<h1>MVC Exercises - Views Part 2: Models</h1>
	</header>
	<nav>
		<ul>
			<li><a href="recipeTiles">Tile Layout</a></li>
			<li><a href="recipeTable">Table Layout</a></li>
		</ul>

	</nav>
	<section id="main-content">
	<h1>Recipes</h1>
		<div class="recipetiles">
			<c:forEach var="recipe" items="${recipes}">
				<div id="item">
					<div id="img">
						<c:url var="imageUrl" value="img/recipe${ recipe.recipeId }.jpg" />
						<img src="${ imageUrl }" />
					</div>

					<div id="info">
						<div id="name">
							<c:out value="${recipe.name}" />
						</div>
						<div id="bottom">
							<fmt:formatNumber var="rating" value="${ recipe.averageRating }" maxFractionDigits="0" />
							<c:url var="ratingUrl" value="/img/${ rating }-star.png" />
							<div id="rating">
								<img src="${ ratingUrl }" />
							</div>

							<div id="ingredients">
								<c:out value="${recipe.ingredients.size()} ingredients" />
							</div>
						</div>
					</div>
				</div>
			</c:forEach>

		</div>
	</section>
</body>
</html>