package com.techelevator;

import java.io.File;
import java.util.Scanner;

public class Replacer
{
	public String searchReplacer(File text, String inputWord, String inputReplace)
	{
		String lineToFile = "";

		try (Scanner alice = new Scanner(text))
		{
			String newLine;

			while (alice.hasNextLine())
			{
				String line = alice.nextLine();

				if (line.contains(inputWord))
				{
					newLine = line.replaceAll(inputWord, inputReplace);
					System.out.println(newLine);
					lineToFile += newLine + "\n";
				}
				else
				{
					System.out.println(line);
					lineToFile += line + "\n";
				}
			}
			System.out.println("");
		}
		catch (Exception e)
		{
			System.out.println("\nReplace failed.\n");
		}
		return lineToFile;
	}
}
