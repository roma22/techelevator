package com.techelevator;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class WriteToFile
{
	public void write() throws IOException
	{
		try (Scanner scans = new Scanner(System.in))
		{
			boolean shouldLoop = true;

			while (shouldLoop)
			{
				System.out.print("File to search for: ");
				String inputFile = scans.nextLine();

				File text = new File(inputFile);

				if (!text.exists())
				{
					System.out.println("Sorry, " + text + " does not exist.");
					System.exit(1);
				}

				System.out.print("File to output to: ");
				String outputFile = scans.nextLine();

				File outputText = new File(outputFile);

				if (outputText.exists() && !outputText.canWrite())
				{
					System.out.println("Sorry, " + outputText + " has invalid permissions.");
					System.exit(1);
				}

				FileWriter newTextFile = new FileWriter(outputFile);
				PrintWriter newTextPrint = new PrintWriter(newTextFile);

				System.out.print("Word to search for: ");
				String inputWord = scans.nextLine();

				System.out.print("Replace found word with: ");
				String inputReplace = scans.nextLine();

				newTextPrint.print(new Replacer().searchReplacer(text, inputWord, inputReplace));
				newTextPrint.close();

				break;
			}
		}
	}
}
