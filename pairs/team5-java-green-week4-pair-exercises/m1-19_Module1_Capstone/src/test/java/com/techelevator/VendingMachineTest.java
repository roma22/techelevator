package com.techelevator;

import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class VendingMachineTest
{
	VendingMachine testing;

	@Before
	public void setUp() throws Exception
	{
		testing = new VendingMachine();
	}

	@Test
	public void populateStock_ShouldAddFullStock_WhenVendingMachineStarts_CheckNameTest()
	{
		testing.populateStock();

		assertEquals("Potato Crisps", testing.getStock().get("A1").get(0).getItemName());

		assertEquals("Stackers", testing.getStock().get("A2").get(0).getItemName());

		assertEquals("Grain Waves", testing.getStock().get("A3").get(0).getItemName());

		assertEquals("Cloud Popcorn", testing.getStock().get("A4").get(0).getItemName());

		assertEquals("Moonpie", testing.getStock().get("B1").get(0).getItemName());

		assertEquals("Cowtales", testing.getStock().get("B2").get(0).getItemName());

		assertEquals("Wonka Bar", testing.getStock().get("B3").get(0).getItemName());

		assertEquals("Crunchie", testing.getStock().get("B4").get(0).getItemName());

		assertEquals("Cola", testing.getStock().get("C1").get(0).getItemName());

		assertEquals("Dr. Salt", testing.getStock().get("C2").get(0).getItemName());

		assertEquals("Mountain Melter", testing.getStock().get("C3").get(0).getItemName());

		assertEquals("Heavy", testing.getStock().get("C4").get(0).getItemName());

		assertEquals("U-Chews", testing.getStock().get("D1").get(0).getItemName());

		assertEquals("Little League Chew", testing.getStock().get("D2").get(0).getItemName());

		assertEquals("Chiclets", testing.getStock().get("D3").get(0).getItemName());

		assertEquals("Triplemint", testing.getStock().get("D4").get(0).getItemName());
	}

	@Test
	public void populateStock_ShouldAddFullStock_WhenVendingMachineStarts_CheckPriceTest()
	{
		testing.populateStock();

		assertEquals(new BigDecimal("3.05"), testing.getStock().get("A1").get(0).getItemPrice());

		assertEquals(new BigDecimal("1.45"), testing.getStock().get("A2").get(0).getItemPrice());

		assertEquals(new BigDecimal("2.75"), testing.getStock().get("A3").get(0).getItemPrice());

		assertEquals(new BigDecimal("3.65"), testing.getStock().get("A4").get(0).getItemPrice());

		assertEquals(new BigDecimal("1.80"), testing.getStock().get("B1").get(0).getItemPrice());

		assertEquals(new BigDecimal("1.50"), testing.getStock().get("B2").get(0).getItemPrice());

		assertEquals(new BigDecimal("1.50"), testing.getStock().get("B3").get(0).getItemPrice());

		assertEquals(new BigDecimal("1.75"), testing.getStock().get("B4").get(0).getItemPrice());

		assertEquals(new BigDecimal("1.25"), testing.getStock().get("C1").get(0).getItemPrice());

		assertEquals(new BigDecimal("1.50"), testing.getStock().get("C2").get(0).getItemPrice());

		assertEquals(new BigDecimal("1.50"), testing.getStock().get("C3").get(0).getItemPrice());

		assertEquals(new BigDecimal("1.50"), testing.getStock().get("C4").get(0).getItemPrice());

		assertEquals(new BigDecimal("0.85"), testing.getStock().get("D1").get(0).getItemPrice());

		assertEquals(new BigDecimal("0.95"), testing.getStock().get("D2").get(0).getItemPrice());

		assertEquals(new BigDecimal("0.75"), testing.getStock().get("D3").get(0).getItemPrice());

		assertEquals(new BigDecimal("0.75"), testing.getStock().get("D4").get(0).getItemPrice());
	}
	
	@Test
	public void feedMoney_ShouldAddOneDollarToBalance_WhenUserFeedsOneDollarTest()
	{
		testing.feedMoney(new BigDecimal("1"));
		
		assertEquals(new BigDecimal("1.00"), testing.getBalance());
	}
	
	@Test
	public void feedMoney_ShouldAddTwoDollarsToBalance_WhenUserFeedsTwoDollarsTest()
	{
		testing.feedMoney(new BigDecimal("2"));
		
		assertEquals(new BigDecimal("2.00"), testing.getBalance());
	}
	
	@Test
	public void feedMoney_ShouldAddFiveDollarsToBalance_WhenUserFeedsFiveDollarsTest()
	{
		testing.feedMoney(new BigDecimal("5"));
		
		assertEquals(new BigDecimal("5.00"), testing.getBalance());
	}
	
	@Test
	public void feedMoney_ShouldAddTenDollarsToBalance_WhenUserFeedsTenDollarsTest()
	{
		testing.feedMoney(new BigDecimal("10"));
		
		assertEquals(new BigDecimal("10.00"), testing.getBalance());
	}
	
	@Test
	public void itemPurchase_ShouldReduceMachineBalanceByItemCost_WhenItemIsDispensedTest()
	{
		testing.populateStock();		
		testing.feedMoney(new BigDecimal("10"));		
		testing.itemPurchase("C4");
		
		assertEquals(new BigDecimal("8.50"), testing.getBalance());
	}
	
	@Test
	public void itemPurchase_ShouldRemoveItemFromList_WhenItemIsDispensedTest()
	{
		testing.populateStock();
		testing.feedMoney(new BigDecimal("10"));		
		testing.itemPurchase("C4");
		
		assertEquals(4, testing.getStock().get("C4").size());
	}
	
	@Test
	public void itemPurchase_ShouldNotDispenseItem_WhenMachineBalanceIsLessThanItemCostTest()
	{
		testing.populateStock();
		testing.itemPurchase("C4");
		
		assertEquals(5, testing.getStock().get("C4").size());
	}
	
	@Test
	public void giveChange_ShouldResetMachineBalance_WhenCoinsAreDispensedTest()
	{
		testing.feedMoney(new BigDecimal("10"));		
		testing.giveChange(new BigDecimal("10.00"));
		
		assertEquals(new BigDecimal("0.00"), testing.getBalance());
	}
	
	@Test
	public void soldOut_ShouldReturnTrue_WhenSingleItemStockIsZeroTest()
	{
		List<Item> items = new ArrayList<Item>();
		
		assertTrue(testing.soldOut(items));
	}
	
	@Test
	public void soldOut_ShouldReturnFalse_WhenSingleItemStockIsAboveZeroTest()
	{
		List<Item> items = new ArrayList<Item>();
		
		items.add(new Drink("Heavy", new BigDecimal("1.50")));
		
		assertFalse(testing.soldOut(items));
	}
}
