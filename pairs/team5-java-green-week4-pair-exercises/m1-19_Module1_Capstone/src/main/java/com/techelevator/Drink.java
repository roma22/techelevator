package com.techelevator;

import java.math.BigDecimal;

public class Drink extends Item
{
	protected Drink(String name, BigDecimal price)
	{
		super(name, price);
	}

	@Override
	protected String consume()
	{
		return "\nGlug Glug, Yum!";
	}

	@Override
	public String getItemName()
	{
		return name;
	}
	
	@Override
	public BigDecimal getItemPrice()
	{
		return price;
	}
}
