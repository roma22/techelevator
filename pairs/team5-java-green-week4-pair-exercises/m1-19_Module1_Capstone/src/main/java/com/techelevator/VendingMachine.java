package com.techelevator;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class VendingMachine
{
	private VendingFileIO fileIO = new VendingFileIO();
	private BigDecimal balance = new BigDecimal("0.00");
	private Map<String, List<Item>> stock = new TreeMap<String, List<Item>>();

	protected void populateStock()
	{
		for (String[] items : fileIO.importStock())
		{
			List<Item> singleItemStock = new ArrayList<Item>();

			for (int i = 0; i < 5; i++)
			{
				switch (items[3])
				{
				case "Chip":
					singleItemStock.add(new Chip(items[1], new BigDecimal(items[2])));
					break;
				case "Candy":
					singleItemStock.add(new Candy(items[1], new BigDecimal(items[2])));
					break;
				case "Drink":
					singleItemStock.add(new Drink(items[1], new BigDecimal(items[2])));
					break;
				case "Gum":
					singleItemStock.add(new Gum(items[1], new BigDecimal(items[2])));
					break;
				default:
					singleItemStock.add(new Item(items[1], new BigDecimal(items[2])));
					break;
				}
			}
			stock.put(items[0], singleItemStock);
		}
	}

	public void feedMoney(BigDecimal feedAmount)
	{
		fileIO.logTransaction("FEED MONEY:", balance, balance.add(feedAmount));

		balance = balance.add(feedAmount);
		System.out.println("\nCurrent Money Provided: $" + balance);
	}

	public void itemPurchase(String key)
	{
		Item singleItem = stock.get(key).get(0);

		if (balance.compareTo(singleItem.getItemPrice()) != -1)
		{
			fileIO.logTransaction(singleItem.getItemName() + " " + key, balance,
					balance.subtract(singleItem.getItemPrice()));

			balance = balance.subtract(singleItem.getItemPrice());

			System.out.println(singleItem.consume());

			stock.get(key).remove(0);
		}
		else
		{
			System.out.println("\nNot enough money.");
		}
		System.out.println("\nCurrent Money Provided: $" + balance);
	}

	protected void giveChange(BigDecimal changeAmount)
	{
		fileIO.logTransaction("GIVE CHANGE:", balance, balance.subtract(changeAmount));

		int balanceChange = balance.multiply(new BigDecimal(100)).intValue();
		int quarters = balanceChange / 25;
		int quarterRemainder = balanceChange % 25;
		int dimes = quarterRemainder / 10;
		int dimeRemainder = quarterRemainder % 10;
		int nickels = dimeRemainder / 5;

		System.out.println("\nChange Given:");
		System.out.println("Quarters: " + quarters + "\n" + "Dimes: " + dimes + "\n" + "Nickels: " + nickels);

		balance = balance.subtract(changeAmount);
		System.out.println("\nCurrent Money Provided: $" + balance);
	}

	public BigDecimal getBalance()
	{
		return balance;
	}

	public Map<String, List<Item>> getStock()
	{
		return stock;
	}

	public boolean soldOut(List<Item> items)
	{
		return items.isEmpty();
	}
}
