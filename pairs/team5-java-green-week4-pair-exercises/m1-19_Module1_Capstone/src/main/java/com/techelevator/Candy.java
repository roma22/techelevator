package com.techelevator;

import java.math.BigDecimal;

public class Candy extends Item
{
	protected Candy(String name, BigDecimal price)
	{
		super(name, price);
	}

	@Override
	protected String consume()
	{
		return "\nMunch Munch, Yum!";
	}

	@Override
	public String getItemName()
	{
		return name;
	}
	
	@Override
	public BigDecimal getItemPrice()
	{
		return price;
	}
}
