package com.techelevator;

import java.math.BigDecimal;

public class Item
{
	String name;
	BigDecimal price;

	protected Item(String name, BigDecimal price)
	{
		this.name = name;
		this.price = price;
	}

	protected String consume()
	{
		return "\nMmm, junk food!";
	}

	public String getItemName()
	{
		return name;
	}

	public BigDecimal getItemPrice()
	{
		return price;
	}
}
