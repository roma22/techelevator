package com.techelevator;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

public class VendingFileIO
{
	protected void logTransaction(String line, BigDecimal before, BigDecimal after)
	{
		String dateTime = new SimpleDateFormat("MM/dd/YYYY hh:mm:ss a").format(new Date());

		try
		{
			FileWriter vendingLog = new FileWriter("Log.txt", true);
			PrintWriter vendingPrint = new PrintWriter(vendingLog);
			vendingPrint.format("%-23s %-22s %-10s %s %s", dateTime, line, "$" + before, "$" + after, "\n");
			vendingPrint.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	protected List<String[]> importStock()
	{
		List<String[]> stockList = new ArrayList<String[]>();
		File text = new File("vendingmachine.csv");

		try (Scanner inventory = new Scanner(text))
		{
			while (inventory.hasNextLine())
			{
				String[] itemLine = inventory.nextLine().split("[|]");
				stockList.add(itemLine);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return stockList;
	}
}
