package com.techelevator;

import com.techelevator.view.Menu;

public class VendingMachineCLI
{
	private static final String MAIN_MENU_OPTION_DISPLAY_ITEMS = "Display Vending Machine Items";
	private static final String MAIN_MENU_OPTION_PURCHASE = "Purchase";
	private static final String[] MAIN_MENU_OPTIONS = {MAIN_MENU_OPTION_DISPLAY_ITEMS, MAIN_MENU_OPTION_PURCHASE};
	private static final String PURCHASE_MENU_OPTION_FEED_MONEY = "Feed Money";
	private static final String PURCHASE_MENU_OPTION_SELECT_PRODUCT = "Select Product";
	private static final String PURCHASE_MENU_OPTION_FINISH_TRANSACTION = "Finish Transaction";
	private static final String[] PURCHASE_MENU_OPTIONS = {PURCHASE_MENU_OPTION_FEED_MONEY,
			PURCHASE_MENU_OPTION_SELECT_PRODUCT, PURCHASE_MENU_OPTION_FINISH_TRANSACTION};

	private Menu menu;

	public VendingMachineCLI(Menu menu)
	{
		this.menu = menu;
	}

	public void run()
	{
		VendingMachine machine = new VendingMachine();
		machine.populateStock();

		while (true)
		{
			String choice = (String)menu.getChoiceFromOptions(MAIN_MENU_OPTIONS);

			switch (choice)
			{
			case MAIN_MENU_OPTION_DISPLAY_ITEMS:
				menu.displayItems(machine);
				break;
			case MAIN_MENU_OPTION_PURCHASE:
				while (true)
				{
					choice = (String)menu.getChoiceFromOptions(PURCHASE_MENU_OPTIONS);

					switch (choice)
					{
					case PURCHASE_MENU_OPTION_FEED_MONEY:
						menu.optionFeed(machine);
						continue;
					case PURCHASE_MENU_OPTION_SELECT_PRODUCT:
						menu.optionSelect(machine);
						continue;
					case PURCHASE_MENU_OPTION_FINISH_TRANSACTION:
						machine.giveChange(machine.getBalance());
						break;
					}
					break;
				}
			}
		}
	}

	public static void main(String[] args)
	{
		Menu menu = new Menu(System.in, System.out);
		VendingMachineCLI cli = new VendingMachineCLI(menu);
		cli.run();
	}
}
