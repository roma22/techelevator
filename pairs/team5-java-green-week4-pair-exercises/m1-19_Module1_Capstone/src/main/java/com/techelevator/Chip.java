package com.techelevator;

import java.math.BigDecimal;

public class Chip extends Item
{
	protected Chip(String name, BigDecimal price)
	{
		super(name, price);
	}

	@Override
	protected String consume()
	{
		return "\nCrunch Crunch, Yum!";
	}

	@Override
	public String getItemName()
	{
		return name;
	}
	
	@Override
	public BigDecimal getItemPrice()
	{
		return price;
	}
}
