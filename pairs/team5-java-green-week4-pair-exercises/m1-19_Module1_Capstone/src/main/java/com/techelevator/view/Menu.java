package com.techelevator.view;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.Scanner;

import com.techelevator.Item;
import com.techelevator.VendingMachine;

public class Menu
{
	private PrintWriter out;
	private Scanner in;

	public Menu(InputStream input, OutputStream output)
	{
		this.out = new PrintWriter(output);
		this.in = new Scanner(input);
	}

	public Object getChoiceFromOptions(Object[] options)
	{
		Object choice = null;
		while (choice == null)
		{
			displayMenuOptions(options);
			choice = getChoiceFromUserInput(options);
		}
		return choice;
	}

	private Object getChoiceFromUserInput(Object[] options)
	{
		Object choice = null;
		String userInput = in.nextLine();
		try
		{
			int selectedOption = Integer.valueOf(userInput);
			if (selectedOption > 0 && selectedOption <= options.length)
			{
				choice = options[selectedOption - 1];
			}
		}
		catch (NumberFormatException e)
		{
			// eat the exception, an error message will be displayed below since choice will
			// be null
		}
		if (choice == null)
		{
			out.println("\n*** " + userInput + " is not a valid option ***\n");
		}
		return choice;
	}

	private void displayMenuOptions(Object[] options)
	{
		out.println();
		for (int i = 0; i < options.length; i++)
		{
			int optionNum = i + 1;
			out.println(optionNum + ") " + options[i]);
		}
		out.print("\nPlease choose an option >>> ");
		out.flush();
	}

	public void displayItems(VendingMachine machine)
	{
		Object[] allItemKeys = machine.getStock().keySet().toArray();

		System.out.println("");
		System.out.format("%-5s %-20s %-8s %-7s %-7s %s", "Key", "Name", "Price", "Type", "Quantity", "\n");
		System.out.println("====================================================");

		for (Object key : allItemKeys)
		{
			if (machine.soldOut(machine.getStock().get(key)))
			{
				System.out.format("%-5s %29s %s", key, "SOLD OUT", "\n");
			}
			else
			{
				Item singleItem = machine.getStock().get(key).get(0);

				String name = singleItem.getItemName();
				BigDecimal price = singleItem.getItemPrice();
				String type = singleItem.getClass().getSimpleName();
				Integer quantity = machine.getStock().get(key).size();
				
				System.out.format("%-5s %-20s %-8s %-7s %-7s %s", key, name, "$" + price, type, quantity, "\n");
			}
		}
		System.out.println("\nCurrent Money Provided: $" + machine.getBalance());
	}

	public void optionFeed(VendingMachine machine)
	{
		System.out.print("\nPlease enter dollar amount: ");
		String input = in.nextLine();

		if (input.matches("1|2|5|10"))
		{
			Integer inputValue = Integer.parseInt(input);
			machine.feedMoney(BigDecimal.valueOf(inputValue));
		}
		else
		{
			System.out.println("\nOnly $1, $2, $5, or $10.");
		}
	}

	public void optionSelect(VendingMachine machine)
	{
		displayItems(machine);

		System.out.print("\nPlease enter item key: ");
		String itemKey = in.nextLine().toUpperCase();

		if (machine.getStock().containsKey(itemKey) && !machine.soldOut(machine.getStock().get(itemKey)))
		{
			machine.itemPurchase(itemKey);
		}
		else
		{
			System.out.println("\nYou have entered an invalid input.");
		}
	}
}
