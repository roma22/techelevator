package com.techelevator;

import java.math.BigDecimal;

public class Gum extends Item
{
	protected Gum(String name, BigDecimal price)
	{
		super(name, price);
	}

	@Override
	protected String consume()
	{
		return "\nChew Chew, Yum!";
	}

	@Override
	public String getItemName()
	{
		return name;
	}

	@Override
	public BigDecimal getItemPrice()
	{
		return price;
	}
}
