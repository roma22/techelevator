package com.techelevator;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Scanner;

public class WordCount
{
	public static void main(String[] args)
	{
		File text = new File("alices_adventures_in_wonderland.txt");

		try (Scanner alice = new Scanner(text))
		{
			String line = "";

			while (alice.hasNextLine())
			{
				line += alice.nextLine() + " ";
			}

			String[] words = line.split("(\\s+)");
			String[] sentences = line.split("[.]|[!]|[?]");

			System.out.println("Word Count: " + words.length);
			System.out.println("Sentence Count: " + sentences.length);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
}
