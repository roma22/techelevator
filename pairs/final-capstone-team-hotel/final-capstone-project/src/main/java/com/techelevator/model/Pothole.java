package com.techelevator.model;

import java.time.LocalDate;
import java.util.Date;

import org.hibernate.validator.constraints.NotBlank;

public class Pothole {
	
	private int id;
	private String landMark;
	private String additionalInformation;
	@NotBlank
	private String address;
	private int severity;
	private LocalDate reportedDate;
	private LocalDate inspectedDate;
	private LocalDate repairedDate;
	private Float lat;
	private Float lng;
	private String image;
	
	
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getLandMark() {
		return landMark;
	}
	public void setLandMark(String landMark) {
		this.landMark = landMark;
	}
	public String getAdditionalInformation() {
		return additionalInformation;
	}
	public void setAdditionalInformation(String additionalInformation) {
		this.additionalInformation = additionalInformation;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public int getSeverity() {
		return severity;
	}
	public void setSeverity(int severity) {
		this.severity = severity;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public LocalDate getReportedDate() {
		return reportedDate;
	}
	public void setReportedDate(LocalDate reportedDate) {
		this.reportedDate = reportedDate;
	}
	public LocalDate getInspectedDate() {
		return inspectedDate;
	}
	public void setInspectedDate(LocalDate inspectedDate) {
		this.inspectedDate = inspectedDate;
	}
	public LocalDate getRepairedDate() {
		return repairedDate;
	}
	public void setRepairedDate(LocalDate repairedDate) {
		this.repairedDate = repairedDate;
	}

	public Float getLat() {
		return lat;
	}
	public void setLat(Float lat) {
		this.lat = lat;
	}
	public Float getLng() {
		return lng;
	}
	public void setLng(Float lng) {
		this.lng = lng;
	}
	
}
