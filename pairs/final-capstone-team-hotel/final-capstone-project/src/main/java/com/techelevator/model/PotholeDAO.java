package com.techelevator.model;

import java.util.List;

public interface PotholeDAO {

	
	public List<Pothole> getAllPotholes();
	public int createPothole(String address, String landmark, String additionalInformation, int severity);
	public void save(Pothole pothole);
	public void deletePothole(Pothole pothole);
	public void updatePotholeSeverity(Pothole pothole);
	public void updateInspectionDate(Pothole pothole);
	public void updateRepairDate(Pothole pothole);
	
}
