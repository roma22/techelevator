package com.techelevator.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.techelevator.model.Pothole;
import com.techelevator.model.PotholeDAO;

@Controller
public class PotholeController {

	private static final String UPLOAD_DIRECTORY = "/img";

	String testVar = "";
	@Autowired
	ServletContext servletContext;
	@Autowired
	private PotholeDAO potholeDAO;

	@RequestMapping(path = { "/", "/homepage" }, method = RequestMethod.GET)
	public String goToHomePage(ModelMap map, Pothole pothole) {
		map.addAttribute("potholes", potholeDAO.getAllPotholes());

		//
		// THIS CONVERTS TO JSON AND SENDS TO JSP
		//
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String json = gson.toJson(potholeDAO.getAllPotholes());
		map.addAttribute("latLng", json);
		testVar = "";
		return "homepage";
	}

	@RequestMapping(path = { "/create" }, method = RequestMethod.GET)
	public String goToCreatePothole(ModelMap map, Pothole pothole) {
		map.addAttribute("testVar", testVar);
		return "create";
	}

	@RequestMapping(path = "/create", method = RequestMethod.POST)
	public String createPothole(@Valid @ModelAttribute("create") Pothole pothole, BindingResult bindingresult,
			ModelMap map, RedirectAttributes flash) {

		if (!bindingresult.hasErrors()) {
			pothole.setImage(testVar);
			potholeDAO.save(pothole);
			flash.addFlashAttribute("viewAlertNewPothole", true);
			testVar = "";
		}
		testVar = "";
		return "redirect:homepage";
	}

	@RequestMapping(path = "/delete", method = RequestMethod.POST)
	public String deletePothole(@ModelAttribute("delete") Pothole pothole, ModelMap map) {
		potholeDAO.deletePothole(pothole);
		return "redirect:homepage";
	}

	@RequestMapping(path = "/updateSeverity", method = RequestMethod.POST)
	public String updateSeverity(@ModelAttribute("updateSeverity") Pothole pothole, ModelMap map) {
		potholeDAO.updatePotholeSeverity(pothole);
		return "redirect:homepage#ph" + pothole.getId();
	}

	@RequestMapping(path = "/inspect", method = RequestMethod.POST)
	public String updateInspectionDate(@ModelAttribute("inspect") Pothole pothole, ModelMap map) {
		potholeDAO.updateInspectionDate(pothole);
		return "redirect:homepage#ph" + pothole.getId();

	}

	@RequestMapping(path = "/repair", method = RequestMethod.POST)
	public String updateRepairDate(@ModelAttribute("repair") Pothole pothole, ModelMap map) {
		potholeDAO.updateRepairDate(pothole);
		return "redirect:homepage#ph" + pothole.getId();

	}

	@RequestMapping("uploadform")
	public String uploadForm() {
		return ("uploadform");
	}

	@RequestMapping("/blank")
	public String blackIframe() {
		return ("blank");
	}

	@RequestMapping(value = "/savefile", method = RequestMethod.POST)
	public String saveimage(@RequestParam CommonsMultipartFile file, HttpSession session, RedirectAttributes flash) throws Exception {

		ServletContext context = session.getServletContext();
		String path = context.getRealPath(UPLOAD_DIRECTORY);
		String filename = file.getOriginalFilename();
		filename = FilenameUtils.removeExtension(filename);
		System.out.println(path + " " + filename);
		testVar = filename;
		byte[] bytes = file.getBytes();
		BufferedOutputStream stream = new BufferedOutputStream(
				new FileOutputStream(new File(path + File.separator + filename)));
		stream.write(bytes);
		stream.flush();
		stream.close();
		
		return "redirect:blank";

	}
	
	@RequestMapping(path="/image/{imageName}", method=RequestMethod.GET)
	@ResponseBody
	public byte[] getImage(@PathVariable(value="imageName") String imageName) {
		String imagePath = getServerContextPath() + File.separator + imageName;
		File image = new File(imagePath);
		try {
			return Files.readAllBytes(image.toPath());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		
	}
	
	private String getServerContextPath() {
		return servletContext.getRealPath("/") + "img";
	}
	
	

}
