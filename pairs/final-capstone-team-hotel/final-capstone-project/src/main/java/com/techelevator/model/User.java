package com.techelevator.model;

import java.util.List;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

public class User {
	private UserDAO dao;
	
	@NotBlank(message="Email is required.")
	@Email(message="Must be a valid email.")
	private String userName;
	
	@Pattern.List({
		@Pattern(regexp=".*[a-z].*", message="Must have a lower case character."),
		@Pattern(regexp=".*[A-Z].*", message="Must have a upper case character.")
	})
	@Size(min=10, message="Password too short, must be at least 10 characters.")
	@NotBlank(message="May not be empty.")
	private String password;
	
	private String role;
	
	private String confirmPassword;
	
	private boolean passwordMatching;
	
	@AssertTrue(message="Passwords must match.")
	public boolean isPasswordMatching() {
		if(password != null) {
			return password.equals(confirmPassword);
		} else {
			return false;
		}
	}
	
	public String getUserName() {
		return userName;
	}
	/**
	 * @return the role
	 */
	public String getRole() {
		return role;
	}
	/**
	 * @param role the role to set
	 */
	public void setRole(String role) {
		this.role = role;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getConfirmPassword() {
		return confirmPassword;
	}
	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}
	
		
}
