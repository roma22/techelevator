package com.techelevator.model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;


@Component
public class JDBCPotholeDAO implements PotholeDAO{
	
	private JdbcTemplate jdbcTemplate;
	
	public JDBCPotholeDAO (DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public List<Pothole> getAllPotholes() {
		List<Pothole> potholes = new ArrayList <Pothole>();
		String selectsql = "SELECT * FROM pothole";
		
		SqlRowSet results = jdbcTemplate.queryForRowSet(selectsql); 
		while(results.next()) {
			Pothole p = mapRowToPothole(results);
			potholes.add(p);
		}
				
		return potholes;
	}

	@Override
	public int createPothole(String address, String landmark, String additionalInformation, int severity) {
		String insertSQL = "Insert into pothole (id, address, landmark, severity ,additional_information, reported_date) Values (DEFAULT, ?, ?, ?, ?, NOW()) returning severity";
		SqlRowSet results = jdbcTemplate.queryForRowSet(insertSQL, address, landmark, severity, additionalInformation);
		
		results.next();
		int potholeID = results.getInt(1);
		return potholeID;
	}
	
	private Pothole mapRowToPothole(SqlRowSet results) {
		Pothole pothole = new Pothole();
		
		pothole.setAddress(results.getString("address"));
		pothole.setAdditionalInformation(results.getString("additional_information"));
		pothole.setLandMark(results.getString("landmark"));
		pothole.setSeverity(results.getInt("severity"));
		pothole.setId(results.getInt("id"));
		pothole.setReportedDate(results.getDate("reported_date").toLocalDate());
		pothole.setLat(results.getFloat("lat"));
		pothole.setLng(results.getFloat("lng"));
		pothole.setImage(results.getString("image"));
		
		
		if (results.getDate("repaired_date") !=null){
			pothole.setRepairedDate(results.getDate("repaired_date").toLocalDate());
		}
		
		if (results.getDate("inspected_date") !=null){
		pothole.setInspectedDate(results.getDate("inspected_date").toLocalDate());
		}
		
		return pothole;
	}
	
	@Override
	public void save(Pothole pothole) {
		String sqlInsertPothole = "INSERT INTO pothole (address, landmark, image, severity ,additional_information, lat, lng) VALUES (?, ?, ?, ?, ?, ?, ?)";
		jdbcTemplate.update(sqlInsertPothole, pothole.getAddress(), pothole.getLandMark(), pothole.getImage(), pothole.getSeverity(), pothole.getAdditionalInformation(), pothole.getLat(), pothole.getLng());
	}
	
	@Override
	public void deletePothole(Pothole pothole) {
		int potholeId = pothole.getId();
		String deletesql = "DELETE FROM pothole WHERE id = " + potholeId;
		jdbcTemplate.update(deletesql);
	}

	@Override
	public void updatePotholeSeverity(Pothole pothole) {
		int potholeId = pothole.getId();
		String updatePotholeSeverity = "UPDATE pothole SET severity = ? WHERE id = " + potholeId;
		jdbcTemplate.update(updatePotholeSeverity, pothole.getSeverity());
		

	}

	@Override
	public void updateInspectionDate(Pothole pothole) {
		int potholeId = pothole.getId();
		Date update = java.sql.Date.valueOf(LocalDate.now());
		String updateInspectionDate = "UPDATE pothole SET inspected_date = ? WHERE id = " + potholeId;
		jdbcTemplate.update(updateInspectionDate, update);
		
	}

	@Override
	public void updateRepairDate(Pothole pothole) {
		int potholeId = pothole.getId();
		Date repair = java.sql.Date.valueOf(LocalDate.now());
		String updateRepairDate = "UPDATE pothole SET repaired_date = ? WHERE id = " + potholeId;
		jdbcTemplate.update(updateRepairDate, repair);
		
	}
	
	

}
