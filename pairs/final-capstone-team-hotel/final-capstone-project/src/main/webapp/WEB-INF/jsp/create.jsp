<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ include file="common/header.jspf"%>

<div class="wholePage">

	<div class="mainContent">
		<div id="displayMap">
			<div id="googleMap" style="width: 100%; height: 100%;"></div>
		</div>

		<c:choose>
			<c:when test="${currentUser.role == ('normaluser' || 'employee')}">
				<div id="displaySubmission">
					<div>
						<h3>Submit New Pothole</h3>
					</div>
					<div id="potholeSubmitForm">
						<c:url var="potholeSubmission" value="/create" />
						<form:form method="POST" action="${potholeSubmission}" modelAttribute="create">
							<form:errors path="*" style="color: red" />
							<div class="potholeSubmission">
								<label for="address" id="addressLabel">Street: </label>
								<input type="text" id="address" name="address" style="border: 1px solid gray"/>
							</div>

							<div class="potholeSubmission">
								<label for="additionalInformation" id="detailsLabel">Details: </label>
								<textarea rows="2" cols="22" id="additionalInformation" name="additionalInformation"></textarea>
							</div>

							<div class="potholeSubmission">
								<iframe width="258" height="80" src="uploadform" frameBorder="0" scrolling="no"></iframe>
							</div>
							<div class="potholeSubmission">
								<button type="submit" class="btn submitNewButton">Submit</button>
								<a href="/capstone/homepage"><button type="button" class="btn">Close Form</button></a>
							</div>
							<input type="hidden" id="input1" name="lat">
							<input type="hidden" id="input2" name="lng">
						</form:form>
					</div>
				</div>
			</c:when>
			<c:otherwise>
				<div id="pleaseLogin">
					<h3>Please login or register to submit new potholes.</h3>
				</div>
			</c:otherwise>
		</c:choose>
	</div>
</div>
<script>
	var markers = [];

	function myMap() {
		var mapProp = {
			center : new google.maps.LatLng(40.0150, -82.9700),
			zoom : 11,
		};
		var geocoder = new google.maps.Geocoder;
		var infowindow = new google.maps.InfoWindow;
		var map = new google.maps.Map(document.getElementById("googleMap"),
				mapProp);

		google.maps.event.addListener(map, 'click', function(event) {
			placeMarker(map, event.latLng, geocoder, infowindow);
		});
	}

	function placeMarker(map, location, geocoder, infowindow) {
		var marker = new google.maps.Marker({
			position : location,
			map : map,
			icon: {                             
				  url: "https://image.flaticon.com/icons/svg/497/497715.svg",
				  anchor: new google.maps.Point(15, 15),
				    scaledSize: new google.maps.Size(20, 20)
				  }
		});

		document.getElementById('input1').value = location.lat();
		document.getElementById('input2').value = location.lng();

		geocoder.geocode({'location': location}, function(results, status) {
		    if (status === 'OK') {
		      if (results[0]) {
		        document.getElementById('address').value = results[1].formatted_address.split(',', 1);
		        infowindow.setContent(results[1].formatted_address);
		        infowindow.open(map, marker);
		      } else {
		        window.alert('No results found');
		      }
		    } else {
		      window.alert('Geocoder failed due to: ' + status);
		    }
		  });
		
		latLng = location.latLng;
		map.addListener("click", function(event) {
			if (marker && marker.setMap) {
				marker.setMap(null);

			}

			marker = new google.maps.Marker({
				map : map,
				position : latLng
			});

			document.getElementById('input1').value = event.latLng.lat();
			document.getElementById('input2').value = event.latLng.lng();
		})
	}
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDE3HcSM1KcA7Bw3oLvDnmT4bOL6AIw5_s&callback=myMap"></script>
<%@ include file="common/footer.jspf"%>