<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@include file="common/header.jspf"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<c:forEach var="pothole" items="${potholes}">
	<div>
		<c:url var="deletePothole" value="/delete" />

		<form:form method="POST" action="${deletePothole}" modelAttribute="delete">
			<c:out value="${pothole.address}" />
			<c:out value="${pothole.landMark}" />
			<c:out value="${pothole.additionalInformation}" />
			<c:out value="${pothole.severity}" />
			<c:out value="${pothole.id}" />

			<input type="hidden" value="${pothole.id}" name="id" />
			<input type="submit" value="Delete" />
		</form:form>
	</div>
</c:forEach>

<c:url var="potholeSubmission" value="/create" />
<form:form method="POST" action="${potholeSubmission}" modelAttribute="create">
	<div class="pothole-submission">
		<label for="address">Address: </label>
		<input type="text" id="address" name="address" />
	</div>

	<div class="pothole-submission">
		<label for="landMark">Land Mark: </label>
		<input type="text" id="landMark" name="landMark" />
	</div>

	<div class="pothole-submission">
		<label for="additionalInformation">Additional Information: </label>
		<input type="text" id="additionalInformation" name="additionalInformation" />
	</div>

	<div class="pothole-submission">
		<label for="severity">Pothole Severity</label>
		<select>
			<option value="1">1</option>
			<option value="2">2</option>
			<option value="3">3</option>
			<option value="4">4</option>
			<option value="5">5</option>
		</select>
	</div>

	<input type="submit" value="Submit">

</form:form>
<%@include file="common/footer.jspf"%>