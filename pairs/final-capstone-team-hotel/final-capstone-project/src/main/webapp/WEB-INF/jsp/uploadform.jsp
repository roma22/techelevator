<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html>
<head>
<title>Attach Image</title>
</head>
<body>
	<div class="form-group">

		<b>Attach Image</b>

		<form:form method="post" action="savefile" enctype="multipart/form-data" id="imgForm">
			<div>
				<input name="file" id="fileToUpload" type="file" required />
			</div>
			<div>
				<input type="submit" value="Upload">
			</div>
		</form:form>
</body>
</html>
