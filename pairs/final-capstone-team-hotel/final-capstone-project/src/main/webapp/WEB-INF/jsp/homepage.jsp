<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ include file="common/header.jspf"%>

<div id="homePage">
	<div class="mainContent">
		<div id="displayList">
			<div>
				<h3>Pothole List</h3>
				<div id="searchInput"><input type="text" id="myInput" onkeyup="myFunction()" onclick="myFunction()" placeholder="Search..."></div>
			</div>
				
			<div id="myUL">
				<c:forEach var="pothole" items="${potholes}">
					<div class="li">
						<div class="displayListInner" >
							<div class="innerListTop">
								<div>
									<b>Street: </b>
									<a id="ph${pothole.id}" style="cursor: pointer; color: black"><c:out value="${pothole.address}" /></a>
								</div>

								<div>
									<b>Details: </b>
									<c:out value="${pothole.additionalInformation}" />
								</div>
							</div>

							<div class="innerListBottom">
								<div>
									<b>Reported: </b>
									<c:out value="${pothole.reportedDate}" />
								</div>

								<div>
									<b>Inspected: </b>
									<c:out value="${pothole.inspectedDate}" />
								</div>

								<div>
									<b>Repaired: </b>
									<c:out value="${pothole.repairedDate}" />
								</div>
								<c:if test="${pothole.image != ''}">
									<div>
										<c:url var="imgUrl" value="/image/${pothole.image}" />
										<img style="width: 300px; height: 200px; border-radius: 7px;"src="${imgUrl}" />
									</div>
								</c:if>
							</div>

							<c:if test="${currentUser.role == 'employee'}">
								<div class="displayUpdate">
									<div class="displayUpdateInner">
										<div class="updateInput">
											<div class="inspectedDate">
												<c:url var="inspectPothole" value="/inspect" />
												<form:form method="POST" action="${inspectPothole}" modelAttribute="inspect">
													<input type="hidden" value="${pothole.id}" name="id" />
													<button type="submit" class="btn inspectedButton">Inspected</button>
												</form:form>
											</div>

											<div class="deleteOption">
												<c:url var="deletePothole" value="/delete" />
												<form:form method="POST" action="${deletePothole}" modelAttribute="delete">
													<input type="hidden" value="${pothole.id}" name="id" />
													<button type="submit" class="btn deleteButton">Delete Pothole</button>
												</form:form>
											</div>
										</div>

										<c:if test="${pothole.inspectedDate != null}">
											<div class="severityUpdate">
												<c:url var="updatePothole" value="/updateSeverity" />
												<form:form method="POST" action="${updatePothole}" modelAttribute="updateSeverity">
													<input type="hidden" value="${pothole.id}" name="id" />

													<c:choose>
														<c:when test="${pothole.severity == 5}">
															<c:set var="five" value="selected" />
															<c:set var="four" value="" />
															<c:set var="three" value="" />
															<c:set var="two" value="" />
															<c:set var="one" value="" />
															<c:set var="zero" value="" />
														</c:when>
														<c:when test="${pothole.severity == 4}">
															<c:set var="five" value="" />
															<c:set var="four" value="selected" />
															<c:set var="three" value="" />
															<c:set var="two" value="" />
															<c:set var="one" value="" />
															<c:set var="zero" value="" />
														</c:when>
														<c:when test="${pothole.severity == 3}">
															<c:set var="five" value="" />
															<c:set var="four" value="" />
															<c:set var="three" value="selected" />
															<c:set var="two" value="" />
															<c:set var="one" value="" />
															<c:set var="zero" value="" />
														</c:when>
														<c:when test="${pothole.severity == 2}">
															<c:set var="five" value="" />
															<c:set var="four" value="" />
															<c:set var="three" value="" />
															<c:set var="two" value="selected" />
															<c:set var="one" value="" />
															<c:set var="zero" value="" />
														</c:when>
														<c:when test="${pothole.severity == 1}">
															<c:set var="five" value="" />
															<c:set var="four" value="" />
															<c:set var="three" value="" />
															<c:set var="two" value="" />
															<c:set var="one" value="selected" />
															<c:set var="zero" value="" />
														</c:when>
														<c:otherwise>
															<c:set var="five" value="" />
															<c:set var="four" value="" />
															<c:set var="three" value="" />
															<c:set var="two" value="" />
															<c:set var="one" value="" />
															<c:set var="zero" value="selected" />
														</c:otherwise>
													</c:choose>

													<div class="severity-input">
														<button type="submit" class="btn updateButton">Update Severity</button>
														<div class="severity-field">

															<div class="custom-select" style="width: 100px;">
																<select name="severity">
																	<option class="zero" value="0" ${zero}>Severity</option>
																	<option class="one" value="1" ${one}>1</option>
																	<option class="two" value="2" ${two}>2</option>
																	<option class="three" value="3" ${three}>3</option>
																	<option class="four" value="4" ${four}>4</option>
																	<option class="five" value="5" ${five}>5</option>
																</select>
															</div>
														</div>
													</div>
												</form:form>
											</div>

											<c:if test="${pothole.severity > 0}">
												<div class="repairedDate">
													<c:url var="repairPothole" value="/repair" />
													<form:form method="POST" action="${repairPothole}" modelAttribute="repair">
														<input type="hidden" value="${pothole.id}" name="id" />
														<button type="submit" class="btn repairedButton">Repaired</button>
													</form:form>
												</div>
											</c:if>
										</c:if>
									</div>
								</div>
							</c:if>
						</div>
					</div>
				</c:forEach>
			</div>
		</div>

		<div id="displayMap">
			<div id="googleMap"></div>
		</div>
	</div>
</div>

<script>

	function myMap() {
		var mapProp = {
			center : new google.maps.LatLng(40.0150, -82.9700),
			zoom : 11,
		};
		var map = new google.maps.Map(document.getElementById("googleMap"),
				mapProp);

		/* google.maps.event.addListener(map, 'click', function(event) {
			placeMarker(map, event.latLng);
		}); */

		/* function placeMarker(map, location) {
			var marker = new google.maps.Marker({
				position : location,
				map : map,
				icon: {                             
					  url: "https://image.flaticon.com/icons/svg/497/497715.svg" ,
					  anchor: new google.maps.Point(15, 15),
					    scaledSize: new google.maps.Size(20, 20)
					  }

			});
			document.getElementById('input1').value = location.lat();
			document.getElementById('input2').value = location.lng(); */

		//
		// THIS GETS COORDINATES OUT OF JSON AND MAPS THEM
		//
		var phList = ${latLng};
		phList.forEach(function(pothole) {
			var phContent = pothole.address;
			var infowindow =  new google.maps.InfoWindow({content: phContent});

			var listMarker = new google.maps.Marker({
				position : new google.maps.LatLng(pothole.lat, pothole.lng),
				map : map,
				icon: {                             
					  url: "https://image.flaticon.com/icons/svg/497/497715.svg",
					  anchor: new google.maps.Point(15, 15),
					    scaledSize: new google.maps.Size(20, 20)
					  },
				title: 'ph'+pothole.id.toString()
			});
			
			listMarker.addListener('click', function() {
				infowindow.open(map, listMarker);
				document.getElementById('myInput').value = phContent;
				document.getElementById('myInput').click();
			});
			
			var infoBox = document.getElementById(listMarker.title);
			infoBox.addEventListener('click', function() {
				infowindow.open(map, listMarker);			
			});	
		})
	}
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDE3HcSM1KcA7Bw3oLvDnmT4bOL6AIw5_s&callback=myMap"></script>

<%@ include file="common/footer.jspf"%>