package com.techelevator;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.jdbc.core.JdbcTemplate;

import com.techelevator.model.JDBCPotholeDAO;
import com.techelevator.model.Pothole;

public class JDBCPotholeDAOTest extends DAOIntegrationTest {

	private JDBCPotholeDAO dao;
	
	@Before
	public void setup() {
		String sqlInsertPothole = "Insert into pothole (address, landmark, severity,additional_information) Values ('test123', 'test', 4, 'test')";
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());
		jdbcTemplate.update(sqlInsertPothole);
		dao = new JDBCPotholeDAO(getDataSource());
	}

	@Test
	public void get_all_potholes() {
	List<Pothole> results = dao.getAllPotholes();
	int numOfPotholesBefore = results.size();
	String insertSql = "Insert into pothole (address, landmark, severity,additional_information) Values ('test123', 'test', 4, 'test')";
	JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());
	jdbcTemplate.update(insertSql);
	results = dao.getAllPotholes();
	assertNotNull(results);
	assertEquals(numOfPotholesBefore + 1, results.size()); 
	}
	
	@Test
	public void create_a_pothole() {
	List<Pothole> results = dao.getAllPotholes();
	int numOfPotholesBefore = results.size();
	dao.createPothole("test1", "test1", "test1", 4);
	results = dao.getAllPotholes();
	assertNotNull(results);
	assertEquals(numOfPotholesBefore + 1, results.size()); 
	
	}
	
	@Test
	public void saves_a_pothole() {
		Pothole pothole = new Pothole();
		pothole.setAddress("test street");
		List<Pothole> results = dao.getAllPotholes();
		int numOfPotholesBefore = results.size();
		dao.save(pothole);
		results = dao.getAllPotholes();
		assertNotNull(results);
		assertEquals(numOfPotholesBefore + 1, results.size()); 
	}
	
	//Puts a pothole into the database, creates a object with the same id, gets amount of potholes before, deletes the pothole we created, gets the number of potholes after, checks the values.
	@Test
	public void deletes_a_pothole() {
		String insertSql = "Insert into pothole (id, address, landmark, severity,additional_information) Values (7777, 'testerrr', 'test', 4, 'test')";
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());
		jdbcTemplate.update(insertSql);
		Pothole pothole = new Pothole();
		pothole.setId(7777);
		List<Pothole> results = dao.getAllPotholes();
		int numOfPotholesBefore = results.size();
		dao.deletePothole(pothole);
		results = dao.getAllPotholes();
		assertNotNull(results);
		assertEquals(numOfPotholesBefore - 1, results.size()); 
		
	}
	
	
	
	
	
}
