-- *************************************************************************************************
-- This script creates all of the database objects (tables, sequences, etc) for the database
-- *************************************************************************************************

BEGIN;

-- CREATE statements go here
DROP TABLE IF EXISTS app_user;
DROP TABLE IF EXISTS pothole;

CREATE TABLE app_user (
  id SERIAL PRIMARY KEY,
  user_name varchar(32) NOT NULL UNIQUE,
  password varchar(32) NOT NULL,
  role varchar(32) DEFAULT 'normaluser',
  salt varchar(255) NOT NULL
);

CREATE TABLE pothole (
id SERIAL PRIMARY KEY,
address varchar(1500),
landmark varchar(1500),
severity INT,
additional_information varchar(1500),
reported_date DATE DEFAULT NOW(),
inspected_date DATE,
repaired_date DATE,
lat FLOAT8,
lng FLOAT8,
image varchar(1500) DEFAULT ''

);

COMMIT;
