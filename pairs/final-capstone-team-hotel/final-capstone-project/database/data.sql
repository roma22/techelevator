-- *****************************************************************************
-- This script contains INSERT statements for populating tables with seed data
-- *****************************************************************************

BEGIN;

INSERT INTO app_user (user_name, password, role, salt) VALUES ('admin@test.com', 'f0SoseEVQFwA49PZLLLFjg==', 'employee', 'W1598UZZfco/gOPGoUPpxpwGvyK6t0hfAu9FwRoXi5x2vMpdYCSJzd+p/MEBSmXGM9ACRZOZvnY9RYPmIY7YTT1DF4m/UXqn1HQ13RXYq/QfG1lvJf5gDPQUPRpmjk+fAUcj06f6180jIWiS/eYjmei+W2bwm3xSfz6mE+k9Xjw=');
INSERT INTO app_user (user_name, password, role, salt) VALUES ('cgagnon@test.com', 'f0SoseEVQFwA49PZLLLFjg==', 'employee', 'W1598UZZfco/gOPGoUPpxpwGvyK6t0hfAu9FwRoXi5x2vMpdYCSJzd+p/MEBSmXGM9ACRZOZvnY9RYPmIY7YTT1DF4m/UXqn1HQ13RXYq/QfG1lvJf5gDPQUPRpmjk+fAUcj06f6180jIWiS/eYjmei+W2bwm3xSfz6mE+k9Xjw=');
INSERT INTO app_user (user_name, password, role, salt) VALUES ('roma@test.com', 'f0SoseEVQFwA49PZLLLFjg==', 'normaluser', 'W1598UZZfco/gOPGoUPpxpwGvyK6t0hfAu9FwRoXi5x2vMpdYCSJzd+p/MEBSmXGM9ACRZOZvnY9RYPmIY7YTT1DF4m/UXqn1HQ13RXYq/QfG1lvJf5gDPQUPRpmjk+fAUcj06f6180jIWiS/eYjmei+W2bwm3xSfz6mE+k9Xjw=');

INSERT INTO pothole (address, landmark, severity, additional_information, lat, lng, image) VALUES ('569 N Wilson Rd','', 0, '',39.96453094482422,	-83.09390258789062, '');
INSERT INTO pothole (address, landmark, severity, additional_information, lat, lng, image) VALUES ('3347 Colchester Rd', '', 0, '' ,40.02431869506836,	-83.06781005859375, '');
INSERT INTO pothole (address, landmark, severity, additional_information, lat, lng, image) VALUES ('3023 Hamilton Ave','', 0, '',	40.02799987792969, -82.97785949707031, '');
INSERT INTO pothole (address, landmark, severity, additional_information, lat, lng, image) VALUES ('3978 Ritamarie Dr','', 0, '',	40.0374641418457,	-83.0650634765625, '');
INSERT INTO pothole (address, landmark, severity, additional_information, lat, lng, image) VALUES ('Lockbourne Rd & E Markison Ave','', 0, '',39.930660247802734,	-82.96481323242188, '');
INSERT INTO pothole (address, landmark, severity, additional_information, lat, lng, image) VALUES ('3357 Elbern Ave','', 0, '',39.96803283691406,	-82.91056823730469, '');
INSERT INTO pothole (address, landmark, severity, additional_information, lat, lng, image) VALUES ('2385 Argyle Dr','', 0, '',40.000125885009766,	-82.94352722167969, '');
INSERT INTO pothole (address, landmark, severity, additional_information, lat, lng, image) VALUES ('1473 1/2 Thomas Ave','', 0, '',39.94697952270508,	-83.03759765625, '');
INSERT INTO pothole (address, landmark, severity, additional_information, lat, lng, image) VALUES ('5640 Frantz Rd','', 0, '',40.08108139038086,	-83.12136840820312, '');
INSERT INTO pothole (address, landmark, severity, additional_information, lat, lng, image) VALUES ('3617 Kays Ave','', 0, '',40.0789794921875,	-83.08909606933594, '');
INSERT INTO pothole (address, landmark, severity, additional_information, lat, lng, image) VALUES ('5880 Sinclair Rd','', 0, '',40.083709716796875,	-82.99639892578125, '');
INSERT INTO pothole (address, landmark, severity, additional_information, lat, lng, image) VALUES ('2419 Belcher Dr','', 0, '',40.0548095703125,	-82.95314025878906, '');
INSERT INTO pothole (address, landmark, severity, additional_information, lat, lng, image) VALUES ('3543 Poth Rd','', 0, '',39.982765197753906,	-82.88310241699219, '');
INSERT INTO pothole (address, landmark, severity, additional_information, lat, lng, image) VALUES ('3861 Patricia Dr','', 0, '',40.03535842895508,	-83.06437683105469, '');
INSERT INTO pothole (address, landmark, severity, additional_information, lat, lng, image) VALUES ('4829 Teter Ct','', 0, '',40.05585861206055,	-83.06437683105469, '');
INSERT INTO pothole (address, landmark, severity, additional_information, lat, lng, image) VALUES ('1275 Primrose Pl','', 0, '',	39.987632751464844,	-83.04016876220703, '');
INSERT INTO pothole (address, landmark, severity, additional_information, lat, lng, image) VALUES ('1020 W 6th Ave','', 0, '',39.989341735839844,	-83.03382110595703, '');
INSERT INTO pothole (address, landmark, severity, additional_information, lat, lng, image) VALUES ('1296 Roberts Pl','', 0, '',39.930660247802734,	-82.95932006835938, '');
INSERT INTO pothole (address, landmark, severity, additional_information, lat, lng, image) VALUES ('1204 Mooberry St','', 0, '',39.9527702331543,	-82.9661865234375, '');
INSERT INTO pothole (address, landmark, severity, additional_information, lat, lng, image) VALUES ('1797 Bide A Wee Park Ave','', 0, '',39.94961166381836,	-82.94970703125, '');
INSERT INTO pothole (address, landmark, severity, additional_information, lat, lng, image) VALUES ('2185 Brown Rd','', 0, '',	39.91170120239258,	-83.03416442871094, '');
INSERT INTO pothole (address, landmark, severity, additional_information, lat, lng, image) VALUES ('4223 Demorest Cove Ct'	,'', 0, '',	39.91012191772461,	-83.10626220703125, '');
INSERT INTO pothole (address, landmark, severity, additional_information, lat, lng, image) VALUES ('Olentangy Fwy'	,'', 0, '',	40.0032844543457,	-83.03004455566406, '');
INSERT INTO pothole (address, landmark, severity, additional_information, lat, lng, image) VALUES ('Refuse Vehicle Storage Building','', 0, '',	40.01169967651367,	-83.03279113769531, '');
INSERT INTO pothole (address, landmark, severity, additional_information, lat, lng, image) VALUES ('724 Bevis Rd','', 0, '',	40.02273941040039,	-83.03347778320312, '');
INSERT INTO pothole (address, landmark, severity, additional_information, lat, lng, image) VALUES ('W Innerbelt'	,'', 0, '',	39.96434783935547,	-83.02043151855469, '');
INSERT INTO pothole (address, landmark, severity, additional_information, lat, lng, image) VALUES ('3660 Interchange Rd','', 0, '',	39.97487258911133,	-83.10282897949219, '');
INSERT INTO pothole (address, landmark, severity, additional_information, lat, lng, image) VALUES ('645 Joyful St','', 0, '',	39.9406623840332,	-83.09184265136719, '');
INSERT INTO pothole (address, landmark, severity, additional_information, lat, lng, image) VALUES ('3000 Agler Rd','', 0, '',	40.02273941040039,	-82.93185424804688, '');
INSERT INTO pothole (address, landmark, severity, additional_information, lat, lng, image) VALUES ('2323 Citygate Dr','', 0, '',	40.01642990112305,	-82.92498779296875, '');
INSERT INTO pothole (address, landmark, severity, additional_information, lat, lng, image) VALUES ('440 Trevitt St','', 0, '',	39.975399017333984,	-82.97099304199219, '');
INSERT INTO pothole (address, landmark, severity, additional_information, lat, lng, image) VALUES ('908 E 12th Ave','', 0, '',	39.99644470214844,	-82.982666015625, '');
INSERT INTO pothole (address, landmark, severity, additional_information, lat, lng, image) VALUES ('2031 Old Colony Ln','', 0, '',	39.92855453491211,	-82.92430114746094, '');
INSERT INTO pothole (address, landmark, severity, additional_information, lat, lng, image) VALUES ('725 E N Broadway','', 0, '',40.03115463256836	,-82.99433898925781, '');
INSERT INTO pothole (address, landmark, severity, additional_information, lat, lng, image) VALUES ('3474 Johnny Appleseed Ct','', 0, '',	40.0774040222168,	-82.92704772949219, '');
INSERT INTO pothole (address, landmark, severity, additional_information, lat, lng, image) VALUES ('2100 Mc Gaw Rd','', 0, '',	39.86797332763672,	-82.94833374023438, '');
INSERT INTO pothole (address, landmark, severity, additional_information, lat, lng, image) VALUES ('640 Scriven Ave','', 0, '',	39.93992233276367,	-83.11058807373047, '');
INSERT INTO pothole (address, landmark, severity, additional_information, lat, lng, image) VALUES ('505 Spruce St','', 0, '',	39.97119140625,	-83.01768493652344, '');
INSERT INTO pothole (address, landmark, severity, additional_information, lat, lng, image) VALUES ('655 Wager St','', 0, '',	39.951717376708984,	-82.98129272460938, '');
INSERT INTO pothole (address, landmark, severity, additional_information, lat, lng, image) VALUES ('474 Taylor Ave','', 0, '',	39.97697830200195,	-82.960693359375, '');
INSERT INTO pothole (address, landmark, severity, additional_information, lat, lng, image) VALUES ('Thoburn Rd','', 0, '',	40.026947021484375,	-83.09390258789062, '');
INSERT INTO pothole (address, landmark, severity, additional_information, lat, lng, image) VALUES ('157 Benfield Ave' ,'', 0, '',	39.91117477416992,	-82.99090576171875, '');
INSERT INTO pothole (address, landmark, severity, additional_information, lat, lng, image) VALUES ('998-982 E Markison Ave','', 0, '',	39.930660247802734,	-82.96961975097656, '');
INSERT INTO pothole (address, landmark, severity, additional_information, lat, lng, image) VALUES ('1008 E Deshler Ave','', 0, '',	39.93960952758789,	-82.96893310546875, '');
INSERT INTO pothole (address, landmark, severity, additional_information, lat, lng, image) VALUES ('1036 Newton St','', 0, '',	39.951717376708984,	-82.97030639648438, '');
INSERT INTO pothole (address, landmark, severity, additional_information, lat, lng, image) VALUES ('1637 E Livingston Ave','', 0, '',	39.94855880737305,	-82.95382690429688, '');
INSERT INTO pothole (address, landmark, severity, additional_information, lat, lng, image) VALUES ('816 Elliot Alley'	,'', 0, '',	39.962772369384766,	-82.97854614257812, '');
INSERT INTO pothole (address, landmark, severity, additional_information, lat, lng, image) VALUES ('973 Mt Vernon Ave'	,'', 0, '',	39.97119140625,	-82.97579956054688, '');
INSERT INTO pothole (address, landmark, severity, additional_information, lat, lng, image) VALUES ('420 S Monroe Ave'	,'', 0, '',	39.95540237426758,	-82.97854614257812, '');
INSERT INTO pothole (address, landmark, severity, additional_information, lat, lng, image) VALUES ('Marion Franklin'	,'', 0, '',	39.91170120239258,	-82.95108032226562, '');
INSERT INTO pothole (address, landmark, severity, additional_information, lat, lng, image) VALUES ('744 Glendower Ave'	,'', 0, '',	39.91328048706055,	-82.97511291503906, '');
INSERT INTO pothole (address, landmark, severity, additional_information, lat, lng, image) VALUES ('1965 Prince George Dr','', 0, '',	39.92960739135742,	-82.92361450195312, '');
INSERT INTO pothole (address, landmark, severity, additional_information, lat, lng, image) VALUES ('1565 Vilardo Ln','', 0, '',	39.94118881225586,	-82.89134216308594, '');
INSERT INTO pothole (address, landmark, severity, additional_information, lat, lng, image) VALUES ('2350 Teakwood Dr','', 0, '',	40.079620361328125,	-82.95556640625, '');
INSERT INTO pothole (address, landmark, severity, additional_information, lat, lng, image) VALUES ('6344 Nicholas Dr','', 0, '',	40.08686065673828,	-83.06163024902344, '');


COMMIT;
