<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@include file="common/header.jspf"%>
<div class="background">
    <h1>Solar System Geek Gift Shop</h1>

    <div class="productcart">
	<table class="table">
	    <tr>
		<th></th>
		<th style="color: black">Name</th>
		<th style="width: 300px; color: black">Price</th>
		<th style="color: black">Qty.</th>
		<th style="color: black">Total</th>
	    </tr>

	    <c:set var="pricetotal" value="0"/>
	    <c:forEach items="${ productList }" var="product">
		<c:set var="qty" value="${ cart.get(product.id) }"/>
		<tr>
		    <td><c:url var="imgUrl" value="/img/${ product.imageName }" />
			<img style="width: 200px; height: 150px;" src="${ imgUrl }" /></td>
		    <td style="width: 300px; color: black"><c:out value="${ product.name }"/></td>
		    <td style="color: black"><fmt:parseNumber var="price" value="${ product.price }" type="currency" parseLocale="en_US"/>
			<fmt:formatNumber var="money" value="${ price }" type="currency"/><c:out value="${ money }"/></td>
		    <td style="color: black"><c:out value="${ qty }"/></td>
		    <td style="color: black">
			<fmt:formatNumber var="total" value="${ price * qty }" type="currency"/><c:out value="${ total }"/></td>
		</tr>
		<c:set var="pricetotal" value="${ pricetotal + price * qty }"/>
	    </c:forEach>
	    <c:url value="/storeCheckout" var="formAction" />
	    <form method="POST" action="${ formAction }">
		<tr><td></td><td></td><td></td><td></td><td style="color: black">
			<fmt:formatNumber var="grandtotal" value="${ pricetotal }" type="currency"/><c:out value="Grand total: ${ grandtotal }"/></td></tr>
		<tr><td></td><td></td><td></td><td></td><td><input type="submit" value="Check out"/></td></tr></form>
	</table>
    </div>
</div>
<%@include file="common/footer.jspf"%>
