<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<%@include file="common/header.jspf"%>

<div class="background">
<div class="return-format">
	<img class="return-image" src="img/${fn:toLowerCase(param.planet)}.jpg">

	<p>
		<c:out value="${param.age}" />
		years old on planet Earth, equals
		<fmt:formatNumber type="number" maxFractionDigits="2" value="${calculator.alienAge}" />
		<c:out value="${param.planet}" />
		years old.
	</p>
</div>
</div>

<%@include file="common/footer.jspf"%>