<%@ page language="java" contentType="text/html; charset=UTF-8"
	 pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@include file="common/header.jspf"%>

<div class="background">
    <h1>Solar System Geek Forum</h1>

    <c:url value="/forumSubmit" var="forumSubmit" />
    <h3><a href="${forumSubmit}"> Post a Message</a></h3>
    <div class="posts">
	<c:forEach items="${posts}" var="post">
	    <div class="post">
		<ul> 
		    <li> ${post.subject} </li>
		    <li> by ${post.username} on ${post.datePosted} </li>
		    <li> ${post.message} </li>
		</ul>
	    </div>
	</c:forEach>
    </div>
</div>
<%@include file="common/footer.jspf"%>
