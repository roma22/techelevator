<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<%@include file="common/header.jspf"%>

<div class="background">
<div class="return-format">
	<img class="return-image" src="img/${fn:toLowerCase(param.planet)}.jpg">

	<p>
		If you are
		<c:out value="${param.weight}" />
		lbs on planet Earth, you would weigh
		<fmt:formatNumber type="number" maxFractionDigits="2" value="${calculator.alienWeight}" />
		lbs on
		<c:out value="${param.planet}" />
		.
	</p>
</div>
</div>

<%@include file="common/footer.jspf"%>