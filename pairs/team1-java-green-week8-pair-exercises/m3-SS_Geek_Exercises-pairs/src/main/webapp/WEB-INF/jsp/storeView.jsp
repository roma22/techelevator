<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="common/header.jspf" %>
<section id="main-content">

    <h1 style="color: black">Solar System Geek Gift Shop</h1>

    <div class="productlist">
	<c:forEach var="product" items="${ products }">
	    <div id="product">
		<c:url var="imgUrl" value="/img/${ product.imageName }" />
		<a href="storeDetail?id=${ product.id }">
		    <img src="${ imgUrl }" />
		</a>

		<div id="info">
		    <div id="name">
			<c:out value="${ product.name }" />
		    </div>

		    <div id="price">
			<c:out value="${ product.price }" />
		    </div>
		</div>
	    </div>
	</c:forEach>
    </div>
</section>
<%@ include file="common/footer.jspf" %>
