<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@include file="common/header.jspf"%>

<c:url var="formAction" value="/forumSubmitResult"/>
<form method="POST" action="${formAction}">


	<h1> New Geek Post </h1>

	<div>
		<label for="username"> Username </label> 
		<input type="text" name="username" id="username" required />
	</div>
	
		<div>
		<label for="subject">Subject</label> 
		<input type="text" name="subject" id="subject" required />
	</div>
	
		<div>
		<label for="message">Message</label> 
		<input type="text" name="message" id="message" required />
	</div>

	<button class="formSubmitButton" type="submit">Submit</button>
</form>




<%@include file="common/footer.jspf"%>