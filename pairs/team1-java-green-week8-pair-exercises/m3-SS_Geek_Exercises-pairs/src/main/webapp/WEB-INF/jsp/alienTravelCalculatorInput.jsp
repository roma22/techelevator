<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@include file="common/header.jspf"%>

<h2 class="headline">Alien Travel Calculator</h2>
<c:url var="formAction" value="/alienTravelCalculatorResult" />
<form method="GET" action="${formAction}">
	<div class="formInputGroup">
		<label for="planet">Choose a Planet:</label> 
		<select name="planet"	id="planet" required>
			<option value="Mercury"> Mercury </option>
			<option value="Venus"> Venus </option>
			<option value="Mars"> Mars </option>
			<option value="Jupiter"> Jupiter </option>
			<option value="Saturn"> Saturn </option>
			<option value="Uranus"> Uranus </option>
			<option value="Neptune"> Neptune </option>
		</select>
		
	<label for="travel">Choose a Travel Method:</label> 
		<select name="travel"	id="travel" required>
		
			<option value="3.0"> Walking </option>
			<option value="100.0"> Car </option>
			<option value="200.0"> Bullet Train </option>
			<option value="570.0"> Boeing 747 </option>
			<option value="1350.0"> Concorde </option>
		</select>
	</div>
	<div>
		<label for="age">Enter your Earth Age:</label> 
		<input type="text" name="age" id="age" required />
	</div>

	<button class="formSubmitButton" type="submit">Calculate Travel Time</button> 
</form>


<%@include file="common/footer.jspf"%>