<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@include file="common/header.jspf"%>

<h2 class="headline">Alien Age Calculator</h2>
<c:url var="formAction" value="/ageCalculatorResult" />
<form method="GET" action="${formAction}">
	<div class="formInputGroup">
		<label for="planet">Choose a Planet:</label> 
		<select name="planet"	id="planet" required>
			<option value="Mercury"> Mercury </option>
			<option value="Venus"> Venus </option>
			<option value="Mars"> Mars </option>
			<option value="Jupiter"> Jupiter </option>
			<option value="Saturn"> Saturn </option>
			<option value="Uranus"> Uranus </option>
			<option value="Neptune"> Neptune </option>
			<option value="Pluto"> Pluto </option>
		</select>
	</div>

	<div>
		<label for="age">Enter your Earth age:</label> 
		<input type="text" name="age" id="age" required />
	</div>

	<button class="formSubmitButton" type="submit">Calculate Age</button>
</form>


<%@include file="common/footer.jspf"%>