<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<%@include file="common/header.jspf"%>

<div class="background">
	<div class="return-format">
		<img class="return-image"
			src="img/${fn:toLowerCase(param.planet)}.jpg">

		<p>
			Traveling by
			<c:choose>
			<c:when test="${param.travel == '3.0'}">
			<c:set var="travelMethod" value="walking"/>
			</c:when>
			<c:when test="${param.travel == '100.0'}">
			<c:set var="travelMethod" value="car"/>
			</c:when>
			<c:when test="${param.travel == '200.0'}">
			<c:set var="travelMethod" value="bullet train"/>
			</c:when>
			<c:when test="${param.travel == '570.0'}">
			<c:set var="travelMethod" value="Boeing 747"/>
			</c:when>
			<c:otherwise>
			<c:set var="travelMethod" value="Concorde"/>
			</c:otherwise>
			</c:choose>
			<c:out value="${travelMethod}" />
			you will reach
			<c:out value="${param.planet}" />
			in
			<fmt:formatNumber type="number" maxFractionDigits="2" value="${calculator.years}" />
			years. You will be
			<fmt:formatNumber type="number" maxFractionDigits="2" value="${calculator.newAge}" />
			years old.
		</p>
	</div>
</div>


<%@include file="common/footer.jspf"%>