<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="common/header.jspf" %>
<section id="main-content">

    <h1 style="color: black">Solar System Geek Gift Shop</h1>

    <c:set var="productid" value="${ param.id }" />
    <div class="productdetail">
	<c:set var="product" value="${ product }" />
	<div id="product">
	    <c:url var="imgUrl" value="/img/${ product.imageName }" />
	    <img src="${ imgUrl }" />

	    <div id="info">
		<div id="name">
		    <c:out value="${ product.name }" />
		</div>

		<div id="price">
		    <c:out value="${ product.price }" />
		</div>

		<div id="description">
		    <b>Description: </b>
		    <c:out value="${ product.description }" />
		</div>

		<c:url value="/storeCart" var="formAction" />
		<form method="POST" action="${ formAction }">
		    <span id="group">
			<input style="display: none" type="radio" name="id" value="${ productid }" checked/>
			<label for="qty">Quantity to buy:</label>
			<input type="text" name="qty" />
		    </span>
		    <input type="submit" value="Add to Cart" id="button"/>
		</form>
	    </div>
	</div>
    </div>
</section>
<%@ include file="common/footer.jspf" %>
