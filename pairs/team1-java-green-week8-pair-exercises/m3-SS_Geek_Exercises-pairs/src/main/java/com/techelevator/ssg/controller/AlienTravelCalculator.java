package com.techelevator.ssg.controller;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class AlienTravelCalculator {

	private double years;
	private String planetName;
	private double travelMethod;
	private double earthAge;
	private double newAge;
	private final double HOURS_IN_YEAR = 8760.0;

	public AlienTravelCalculator(String planetName, double earthAge, double travelMethod) {
		this.planetName = planetName;
		this.earthAge = earthAge;
		this.travelMethod = travelMethod;
	}

	Map<String, Double> planetDistanceMap = new HashMap<String, Double>();

	public Map<String, Double> fillMap() {

		planetDistanceMap.put("Mercury", 56974146.0);
		planetDistanceMap.put("Venus", 25724767.0);
		planetDistanceMap.put("Mars", 48678219.0);
		planetDistanceMap.put("Jupiter", 390674710.0);
		planetDistanceMap.put("Saturn", 792248270.0);
		planetDistanceMap.put("Uranus", 1692662530.0);
		planetDistanceMap.put("Neptune", 2703959960.0);

		return planetDistanceMap;
	}

	public double getYears() {
		years = fillMap().get(planetName) / travelMethod / HOURS_IN_YEAR;
		return years;
	}

	public double getNewAge() {
		newAge = years + earthAge;
		return newAge;
	}

}
