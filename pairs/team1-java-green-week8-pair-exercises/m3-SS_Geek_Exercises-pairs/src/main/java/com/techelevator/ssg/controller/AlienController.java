package com.techelevator.ssg.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import com.techelevator.ssg.controller.AlienAgeCalculator;
import com.techelevator.ssg.controller.AlienTravelCalculator;
import com.techelevator.ssg.controller.AlienWeightCalculator;

@Controller
public class AlienController {

	@RequestMapping("/alienAgeCalculator")
	public String displayAlienAgeCalculatorInputPage() {
		return "alienAgeCalculator";
	}
	
	@RequestMapping("/ageCalculatorResult")
	public String displayAlienAgeCalculatorResultPage(HttpServletRequest request) {
		
		Double age = Double.parseDouble(request.getParameter("age"));
		String planet = request.getParameter("planet");
		
		AlienAgeCalculator calculator = new AlienAgeCalculator(planet, age);
		request.setAttribute("calculator", calculator);
		
		return "ageCalculatorResult";
	}
	
	@RequestMapping("/alienWeightCalculatorInput")
	public String displayAlienWeightCalculatorInputPage() {
		return "alienWeightCalculatorInput";
	}
	
	@RequestMapping("/alienWeightCalculatorResult")
	public String displayAlienWeightCalculatorResultPage(HttpServletRequest request) {
		
		Double weight = Double.parseDouble(request.getParameter("weight"));
		String planet = request.getParameter("planet");
		
		AlienWeightCalculator calculator = new AlienWeightCalculator(planet, weight);
		request.setAttribute("calculator", calculator);
		
		return "alienWeightCalculatorResult";
	}
	
	@RequestMapping("/alienTravelCalculatorInput")
	public String displayAlienTravelCalculatorInputPage() {
		return "alienTravelCalculatorInput";
	}
	
	@RequestMapping("/alienTravelCalculatorResult")
	public String displayAlienTravelCalculatorResultPage(HttpServletRequest request) {
		
		Double age = Double.parseDouble(request.getParameter("age"));
		String planet = request.getParameter("planet");
		Double travel = Double.parseDouble(request.getParameter("travel"));
		
		AlienTravelCalculator calculator = new AlienTravelCalculator(planet, age, travel);
		request.setAttribute("calculator", calculator);
		
		return "alienTravelCalculatorResult";
	}
	
}





