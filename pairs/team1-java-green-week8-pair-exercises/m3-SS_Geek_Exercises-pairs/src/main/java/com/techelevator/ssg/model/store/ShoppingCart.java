
package com.techelevator.ssg.model.store;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ShoppingCart
{
    private List<CartItem> shoppingCart = new ArrayList<>();

    public List<CartItem> getShoppingCart()
    {
	return shoppingCart;
    }

    public void resetShoppingCart()
    {
	this.shoppingCart = new ArrayList<>();
    }
    
    public void addCartItem(CartItem cartItem)
    {
	shoppingCart.add(cartItem);
    }
    
    public void removeCartItem(int index)
    {
	shoppingCart.remove(index);
    }
}
