
package com.techelevator.ssg.model.store;

public class CartItem
{
    private Long id;
    private Long qty;

    public Long getId()
    {
	return id;
    }

    public void setId(Long id)
    {
	this.id = id;
    }

    public Long getQty()
    {
	return qty;
    }

    public void setQty(Long qty)
    {
	this.qty = qty;
    }
}
