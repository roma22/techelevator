package com.techelevator.ssg.controller;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class AlienAgeCalculator {

	private double alienAge;
	private String planetName;
	private double earthAge;

	public AlienAgeCalculator(String planetName, double earthAge) {
		this.planetName = planetName;
		this.earthAge = earthAge;
	}

	Map<String, Double> ageMap = new HashMap<String, Double>();

	public Map<String, Double> fillMap() {

		ageMap.put("Mercury", (365.26 / 87.96));
		ageMap.put("Venus", (365.26 / 224.68));
		ageMap.put("Earth", 365.26);
		ageMap.put("Mars", (365.26 / 686.98));
		ageMap.put("Jupiter", 11.862);
		ageMap.put("Saturn", 29.456);
		ageMap.put("Uranus", 84.07);
		ageMap.put("Neptune", 164.81);
		ageMap.put("Pluto", 247.7);
		
		return ageMap;
	}

	public double getAlienAge() {
		alienAge = earthAge * fillMap().get(planetName);
		return alienAge;
	}

}
