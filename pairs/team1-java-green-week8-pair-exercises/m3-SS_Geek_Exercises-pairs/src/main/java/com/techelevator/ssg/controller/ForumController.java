package com.techelevator.ssg.controller;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;


import com.techelevator.ssg.model.forum.ForumDao;
import com.techelevator.ssg.model.forum.ForumPost;

@Controller
public class ForumController {
 
	@Autowired
	private ForumDao forumDao;
	
	@RequestMapping(path="/forumView", method=RequestMethod.GET)
	public String showForumViewForm(HttpServletRequest request) {
		
		request.setAttribute("posts", forumDao.getAllPosts());
		
		return "forumView";
	}
	
	@RequestMapping(path="/forumSubmit", method=RequestMethod.GET)
	public String showForumSubmitForm(HttpServletRequest request) {
		return "forumSubmit";
	}
	
	@RequestMapping(path="/forumSubmitResult", method=RequestMethod.POST)
	public String addNewCityByPostAndRedirect(HttpServletRequest request, @RequestParam String username,
			 @RequestParam String subject,
			 @RequestParam String message) {
			
		
		ForumPost newPost = new ForumPost();
		newPost.setUsername(username);
		newPost.setSubject(subject);
		newPost.setMessage(message);
		newPost.setDatePosted(LocalDateTime.now());
		
		forumDao.save(newPost);
		return "redirect:/forumView";  
	}
}

