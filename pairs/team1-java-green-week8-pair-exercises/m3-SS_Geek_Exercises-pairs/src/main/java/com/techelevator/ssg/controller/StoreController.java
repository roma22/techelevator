package com.techelevator.ssg.controller;

import com.techelevator.ssg.model.store.Product;
import com.techelevator.ssg.model.store.ProductDao;
import com.techelevator.ssg.model.store.CartItem;
import com.techelevator.ssg.model.store.ShoppingCart;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

@Controller
@SessionAttributes("cart")
public class StoreController
{
    @Autowired
    private ProductDao productDao;

    @RequestMapping("/storeView")
    public String showStore(HttpServletRequest request)
    {
	request.setAttribute("products", productDao.getAllProducts());
	return "storeView";
    }

    @RequestMapping("/storeDetail")
    public String showDetail(HttpServletRequest request)
    {
	Long id = Long.parseLong(request.getParameter("id"));
	request.setAttribute("product", productDao.getProductById(id));
	return "storeDetail";
    }

    @RequestMapping(path = "/storeCart", method = RequestMethod.GET)
    public String showCart(ModelMap modelMap)
    {
	Map<Long, Long> shopCart = (HashMap)modelMap.get("cart");
	List<Product> productList = new ArrayList<>();

	for (Long var : shopCart.keySet())
	{
	    Product product = productDao.getProductById(var);
	    productList.add(product);
	}
	modelMap.addAttribute("productList", productList);
	return "storeCart";
    }

    @RequestMapping(path = "/storeCart", method = RequestMethod.POST)
    public String addToCart(CartItem cartItem, Map<Long, Long> shopCart, ModelMap modelMap)
    {
	if (modelMap.get("cart") == null)
	{
	    shopCart = new HashMap<>();
	    modelMap.addAttribute("cart", shopCart);
	}
	shopCart = (Map)modelMap.get("cart");
	shopCart.put(cartItem.getId(), cartItem.getQty());
	return "redirect:/storeCart";
    }

    @RequestMapping(path = "/storeCheckout", method = RequestMethod.GET)
    public String showCheckout()
    {
	return "storeCheckout";
    }

    @RequestMapping(path = "/storeCheckout", method = RequestMethod.POST)
    public String finishCheckout(ModelMap modelMap, HttpSession session)
    {
	session.invalidate();
	modelMap.clear();
	return "redirect:/storeCheckout";
    }
}
