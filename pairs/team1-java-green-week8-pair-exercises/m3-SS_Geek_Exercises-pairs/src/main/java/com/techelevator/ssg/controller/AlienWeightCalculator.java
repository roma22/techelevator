package com.techelevator.ssg.controller;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class AlienWeightCalculator {

	private double alienWeight;
	private String planetName;
	private double earthWeight;

	public AlienWeightCalculator(String planetName, double earthWeight) {
		this.planetName = planetName;
		this.earthWeight = earthWeight;
	}

	Map<String, Double> weightMap = new HashMap<String, Double>();

	public Map<String, Double> fillMap() {

		weightMap.put("Sun", 27.95);
		weightMap.put("Mercury", 0.37);
		weightMap.put("Venus", 0.9);
		weightMap.put("Earth", 1.0);
		weightMap.put("Moon", 0.17);
		weightMap.put("Mars", 0.38);
		weightMap.put("Jupiter", 2.65);
		weightMap.put("Saturn", 1.13);
		weightMap.put("Uranus", 1.09);
		weightMap.put("Neptune", 1.43);
		weightMap.put("Pluto", 0.04);
		
		return weightMap;
	}

	public double getAlienWeight() {
		System.out.println("Here");
				alienWeight = earthWeight * fillMap().get(planetName);
		
		return alienWeight;
	}

}