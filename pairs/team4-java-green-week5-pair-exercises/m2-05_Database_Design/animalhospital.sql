BEGIN TRANSACTION;

DROP TABLE IF EXISTS procedure_info;
DROP TABLE IF EXISTS procedure_price;
DROP TABLE IF EXISTS owner_address;
DROP TABLE IF EXISTS pet_owner;
DROP TABLE IF EXISTS procedure;
DROP TABLE IF EXISTS address;
DROP TABLE IF EXISTS pet;
DROP TABLE IF EXISTS pet_type;
DROP TABLE IF EXISTS owner;

CREATE TABLE owner
(
       owner_id serial primary key,
       first_name varchar(100) not null,
       last_name varchar(100) not null
);

CREATE TABLE pet_type
(
        pet_type_id serial primary key,
        pet_type_name varchar(100) not null
);

CREATE TABLE pet
(
       pet_id serial primary key,
       pet_name varchar(100) not null,
       pet_type_id int not null,
       age int not null,
       
       constraint fk_pet_type_id foreign key (pet_type_id) references pet_type (pet_type_id)
);

CREATE TABLE address
(
        address_id serial primary key,
        address_1 varchar(100) not null,
        city varchar(100) not null,
        district varchar(100) not null,
        code varchar(100) not null
);

CREATE TABLE procedure
(
       procedure_id serial primary key,
       procedure_number varchar(100) not null,
       procedure_name varchar(100) not null
);

CREATE TABLE pet_owner
(
        pet_owner_id serial primary key,
        owner_id int not null,
        pet_id int not null,
        
        constraint fk_owner_id foreign key (owner_id) references owner (owner_id),
        constraint fk_pet_id foreign key (pet_id) references pet (pet_id)
);

CREATE TABLE owner_address
(
        owner_address_id serial primary key,
        owner_id int not null,
        address_id int not null,
        
        constraint fk_owner_id foreign key (owner_id) references owner (owner_id),
        constraint fk_address_id foreign key (address_id) references address (address_id)
);

CREATE TABLE procedure_price
(
        procedure_price_id serial primary key,
        procedure_cost decimal (4,2) not null,
        procedure_id int not null,
        pet_type_id int not null,
        
        constraint fk_procedure_id foreign key (procedure_id) references procedure (procedure_id),
        constraint fk_pet_type_id foreign key (pet_type_id) references pet_type (pet_type_id)
);

CREATE TABLE procedure_info
(
        procedure_info_id serial primary key,
        pet_id int not null,
        procedure_price_id int not null,
        visit_date varchar(100) not null,
        
        constraint fk_pet_id foreign key (pet_id) references pet (pet_id),
        constraint fk_procedure_price_id foreign key (procedure_price_id) references procedure_price (procedure_price_id)
);

COMMIT TRANSACTION;

--

INSERT INTO owner (first_name, last_name)
VALUES ('SAM', 'COOK');

INSERT INTO owner (first_name, last_name)
VALUES ('TERRY', 'KIM');

INSERT INTO owner (first_name, last_name)
VALUES ('RICHARD', 'COOK');

--

INSERT INTO pet_type (pet_type_name)
VALUES ('DOG');

INSERT INTO pet_type (pet_type_name)
VALUES ('CAT');

INSERT INTO pet_type (pet_type_name)
VALUES ('BIRD');

--

INSERT INTO pet (pet_name, pet_type_id, age)
VALUES ('ROVER', 1, 12);

INSERT INTO pet (pet_name, pet_type_id, age)
VALUES ('SPOT', 1, 2);

INSERT INTO pet (pet_name, pet_type_id, age)
VALUES ('MORRIS', 2, 4);

INSERT INTO pet (pet_name, pet_type_id, age)
VALUES ('TWEEDY', 3, 2);

--

INSERT INTO address (address_1, city, district, code)
VALUES ('123 THIS STREET', 'MY CITY', 'ONTARIO', 'Z5Z 6G6');

INSERT INTO address (address_1, city, district, code)
VALUES ('123 THAT STREET', 'YOUR CITY', 'ONTARIO', 'Z5Z 6G6');

--

INSERT INTO procedure (procedure_number, procedure_name)
VALUES ('01', 'RABIES VACCINATION');

INSERT INTO procedure (procedure_number, procedure_name)
VALUES ('05', 'HEART WORM TEST');

INSERT INTO procedure (procedure_number, procedure_name)
VALUES ('08', 'TETANUS VACCINATION');

INSERT INTO procedure (procedure_number, procedure_name)
VALUES ('10', 'EXAMINE and TREAT WOUND');

INSERT INTO procedure (procedure_number, procedure_name)
VALUES ('12', 'EYE WASH');

INSERT INTO procedure (procedure_number, procedure_name)
VALUES ('20', 'ANNUAL CHECK UP');

--

INSERT INTO pet_owner (owner_id, pet_id)
VALUES (1, 1);

INSERT INTO pet_owner (owner_id, pet_id)
VALUES (1, 3);

INSERT INTO pet_owner (owner_id, pet_id)
VALUES (2, 2);

INSERT INTO pet_owner (owner_id, pet_id)
VALUES (2, 4);

INSERT INTO pet_owner (owner_id, pet_id)
VALUES (3, 1);

INSERT INTO pet_owner (owner_id, pet_id)
VALUES (3, 3);

--

INSERT INTO owner_address (owner_id, address_id)
VALUES (1, 1);

INSERT INTO owner_address (owner_id, address_id)
VALUES (2, 2);

INSERT INTO owner_address (owner_id, address_id)
VALUES (3, 1);

--

INSERT INTO procedure_price (procedure_cost, procedure_id, pet_type_id)
VALUES ('30.00', 1, 1);

INSERT INTO procedure_price (procedure_cost, procedure_id, pet_type_id)
VALUES ('24.00', 1, 2);

--

INSERT INTO procedure_info (pet_id, procedure_price_id, visit_date)
VALUES (3, 2, 'JAN 23/2001');

INSERT INTO procedure_info (pet_id, procedure_price_id, visit_date)
VALUES (1, 1, 'JAN 13/2002');

INSERT INTO procedure_info (pet_id, procedure_price_id, visit_date)
VALUES (3, 2, 'JAN 13/2002');

--

SELECT pet.pet_id, pet.pet_name, pet_type.pet_type_name, pet.age, owner.first_name, owner.last_name, procedure_info.visit_date, procedure.procedure_number, procedure.procedure_name
FROM procedure
JOIN procedure_price ON procedure_price.procedure_id = procedure.procedure_id
JOIN procedure_info ON procedure_info.procedure_price_id = procedure_price.procedure_price_id
JOIN pet ON pet.pet_id = procedure_info.pet_id
JOIN pet_type ON pet_type.pet_type_id = pet.pet_type_id
JOIN pet_owner ON pet_owner.pet_id = pet.pet_id
JOIN owner ON owner.owner_id = pet_owner.owner_id
WHERE owner.first_name = 'SAM'
ORDER BY pet.pet_id ASC;

SELECT procedure_info.visit_date, owner.first_name, owner.last_name, address.address_1, address.city, address.district, address.code, pet.pet_name, procedure.procedure_name, procedure_price.procedure_cost
FROM procedure
JOIN procedure_price ON procedure_price.procedure_id = procedure.procedure_id
JOIN procedure_info ON procedure_info.procedure_price_id = procedure_price.procedure_price_id
JOIN pet ON pet.pet_id = procedure_info.pet_id
JOIN pet_owner ON pet_owner.pet_id = pet.pet_id
JOIN owner ON owner.owner_id = pet_owner.owner_id
JOIN owner_address ON owner_address.owner_id = owner.owner_id
JOIN address ON address.address_id = owner_address.address_id
WHERE owner.first_name = 'RICHARD' AND procedure_info.visit_date = 'JAN 13/2002'
ORDER BY procedure_price.procedure_cost DESC;
