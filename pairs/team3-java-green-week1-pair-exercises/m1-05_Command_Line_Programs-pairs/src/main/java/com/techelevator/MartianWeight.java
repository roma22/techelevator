package com.techelevator;

/*
 In case you've ever pondered how much you weight on Mars, here's the calculation:
 	Wm = We * 0.378
 	where 'Wm' is the weight on Mars, and 'We' is the weight on Earth
 
Write a command line program which accepts a series of Earth weights from the user  
and displays each Earth weight as itself, and its Martian equivalent.

 $ MartianWeight 
 
Enter a series of Earth weights (space-separated): 98 235 185
 
 98 lbs. on Earth, is 37 lbs. on Mars.
 235 lbs. on Earth, is 88 lbs. on Mars.
 185 lbs. on Earth, is 69 lbs. on Mars. 
 */
import java.util.Scanner;

public class MartianWeight {

	public static void main(String[] args) {
		
		try (Scanner scanner = new Scanner(System.in)){

            boolean shouldWeLoop = true;

            while (shouldWeLoop) {

            	System.out.print("Enter a series of Earth weights (space-separated) : ");
            	String inputWeight = scanner.nextLine();
            
            	String[] storedWeight = inputWeight.split(" ");
            
            		for (int i=0; i < storedWeight.length; i++) {
            	 
            			int valueWeightEarth = Integer.parseInt(storedWeight[i]);
            	 
            			double valueWeightMars = valueWeightEarth * 0.378;
            			int valueWeightMarsInt = (int)valueWeightMars;
            	 
            			System.out.println(storedWeight[i] + " lbs. on Earth, is " + valueWeightMarsInt + " lbs. on Mars.");
            			 
            		}
            	break;
            }
		}
	}
}
	

