package com.techelevator;

/*
 Write a command line program which prompts the user for the total bill, and the amount tendered. It should then display the change required.
 
 $ java MakeChange
 Please enter the amount of the bill: 23.65
 Please enter the amount tendered: 100.00
 The change required is 76.35
 */
import java.util.Scanner;

public class MakeChange {

	public static void main(String[] args) {
		
		try (Scanner scanner = new Scanner(System.in)){
			
			boolean shouldWeLoop = true;
			
			while (shouldWeLoop) {
				System.out.print("Please enter the amount of the bill: ");
				String inputBill = scanner.nextLine();
				
				System.out.print("Please enter the amount tendered: ");
				String inputTender = scanner.nextLine();
				
				double valueOfBill = Double.parseDouble(inputBill);
				double valueOfTender = Double.parseDouble(inputTender);
				
				valueOfBill = valueOfBill * 100;
				valueOfTender = valueOfTender * 100;
				
				int changeInt = (int)valueOfTender - (int)valueOfBill;
				double change = (double)changeInt / 100;
				
				System.out.println("The change required is " + change);
				break;
			}
		}		
	}
}
