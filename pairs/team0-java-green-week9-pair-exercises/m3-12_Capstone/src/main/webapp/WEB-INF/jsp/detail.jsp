<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<%@include file="common/header.jspf"%>
<!-- include (attach) the header file here. -->

<h1>Park Details</h1>

<div class="parkdetail">
	<c:set var="park" value="${ park }" />

	<div class="top">
		<div class="image">
			<c:url var="imgUrl"
				value="/img/parks/${ park.parkCode.toLowerCase() }.jpg" />
			<img src="${ imgUrl }"
				alt="Lovely ${ park.parkName } National Park Image" />
		</div>
	</div>

	<div class="info">
		<div id="parkname">
			<h4>
				<c:out value="${ park.parkName }" />
			</h4>
		</div>

		<div id="parkdescription">
			<c:out value="${ park.parkDescription }" />
		</div>
		<h3>Today:</h3>
	</div>

	<div class="top">
		<div class="data">
			<div id="state">
				<b>State:</b>&nbsp;
				<c:out value="${ park.state }" />
			</div>

			<div id="acreage">
				<b>Acreage:</b>&nbsp;
				<c:out value="${ park.acreage }" />
			</div>

			<div id="elevationinfeet">
				<b>Elevation in feet:</b>&nbsp;
				<c:out value="${ park.elevationInFeet }" />
			</div>

			<div id="milesoftrail">
				<b>Miles of trail:</b>&nbsp;
				<fmt:formatNumber var="milesoftrail" value="${ park.milesOfTrail }"
					maxFractionDigits="0" />
				<c:out value="${ milesoftrail }" />
			</div>

			<div id="numberofcampsites">
				<b>Number of campsites:</b>&nbsp;
				<c:out value="${ park.numberOfCampsites }" />
			</div>

			<div id="climate">
				<b>Climate:</b>&nbsp;
				<c:out value="${ park.climate }" />
			</div>

			<div id="yearfounded">
				<b>Year founded:</b>&nbsp;
				<c:out value="${ park.yearFounded }" />
			</div>

			<div id="annualvisitorcount">
				<b>Annual visitor count:</b>&nbsp;
				<c:out value="${ park.annualVisitorCount }" />
			</div>

			<div id="inspirationalquote">
				<b>Quote:</b>&nbsp;
				<c:out value="${ park.inspirationalQuoteSource } - " />
				<i>"<c:out value="${ park.inspirationalQuote }" />"
				</i>
			</div>

			<div id="entryfee">
				<b>Entry fee:</b>&nbsp;
				<fmt:formatNumber var="entryfee" value="${ park.entryFee }"
					type="currency" />
				<c:out value="${ entryfee }" />
			</div>

			<div id="numberofanimalspecies">
				<b>Number of animal species:</b>&nbsp;
				<c:out value="${ park.numberOfAnimalSpecies }" />
			</div>
		</div>
	</div>

	<div class="weather" id="tempChangeReturn">
		<c:forEach var="dailyWeather" items="${ weather }">
			<div class="dailyWeather">
				<c:set var="forecast"
					value="${ fn:replace(dailyWeather.forecast, ' ', '') }" />
				<!-- use "fn" to remove space from image filename. -->

				<c:url var="imgUrl" value="/img/weather/${ forecast }.png" />
				<img src="${ imgUrl }" alt="Lovely ${ forecast } Image" />
				<!-- Display the image specified above. And include the "alt" text. -->

				<div class="tempInfo">
					<c:choose>
						<c:when test="${ tempUnit.equals('celsius') }">
							<!-- When selected temperature is "celsius", perform the following. -->
							<c:set var="celsiusChoice" value="selected" />
							<c:set var="fahrenheitChoice" value="" />
							<div id="high">
								<!-- Convert F to C. -->
								<fmt:formatNumber var="high"
									value="${ (dailyWeather.high - 32) / 1.8 }"
									maxFractionDigits="0" />
								<b>High: </b>
								<c:out value="${ high }" />
								&deg;<i>C</i>
							</div>

							<div id="low">
								<!-- Convert F to C. -->
								<fmt:formatNumber var="low"
									value="${ (dailyWeather.low - 32) / 1.8 }"
									maxFractionDigits="0" />
								<b>Low: </b>
								<c:out value="${ low }" />
								&deg;<i>C</i>
							</div>
						</c:when>
						<c:otherwise>
							<!-- Otherwise--when selected temperature is "fahrenheit"--perform the following. -->
							<c:set var="fahrenheitChoice" value="selected" />
							<c:set var="celsiusChoice" value="" />
							<div id="high">
								<b>High: </b>
								<c:out value="${ dailyWeather.high }" />
								&deg;<i>F</i>
							</div>

							<div id="low">
								<b>Low: </b>
								<c:out value="${ dailyWeather.low }" />
								&deg;<i>F</i>
							</div>
						</c:otherwise>
					</c:choose>
				</div>
			</div>
		</c:forEach>
	</div>
	<div class="advisory">
		<b>Weather advisory: </b>&nbsp;
		<!-- Display weather advisory. -->
		<!-- The bold heading is shown whether there is associated text or not. Not sure why? -->
		<c:out value="${ advisoryMap.get(weather.get(0).getForecast()) }" />
		<c:forEach var="tempAdvisory" items="${tempAdvisories}">
			<br>
			<!-- Display temperature advisory -->
			<b>Temperature advisory: </b>&nbsp;
			<c:out value="${tempAdvisory}" />
		</c:forEach>

	</div>
	<c:url var="formAction" value="/detail" />
	<form action="${ formAction }" method="POST">
		<b>Temperature unit: </b>&nbsp; <select name="selectedTempUnit">
			<option value="fahrenheit" ${ fahrenheitChoice }>Fahrenheit</option>
			<!-- F option for temperature scale -->
			<option value="celsius" ${ celsiusChoice }>Celsius</option>
			<!-- C option for temperature scale -->
		</select>&nbsp;
		<input type="submit" value="Change" id="changeButton" />
		<!-- "Change" button used to process the temperature change request. -->
		<input type="hidden" name="parkcode" value="${ park.parkCode }" />
	</form>
</div>

<%@include file="common/footer.jspf"%>
<!-- include (attach) the footer file here. -->
