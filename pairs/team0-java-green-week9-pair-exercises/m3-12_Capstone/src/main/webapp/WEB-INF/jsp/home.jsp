<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@include file="common/header.jspf"%>	<!-- include (attach) the header file here. -->

<h1>List of Parks</h1>

<div class="parklist">
	<c:forEach var="park" items="${ parks }">
		<div class="park">
			<c:url var="imgUrl" value="/img/parks/${ park.parkCode.toLowerCase() }.jpg" />	<!-- Change each parkCode to lower case so it can be found in correct filename for display. -->
			<a href="detail?parkCode=${ park.parkCode }">	<!-- Send park code--when clicked--through query string, as "detail". -->
				<img src="${ imgUrl }" alt="Lovely ${ park.parkName } National Park Image" />	<!-- Display park image and include "alt" text. -->
			</a>

			<div class="info">
				<div id="parkname">
					<h4>
						<a href="detail?parkCode=${ park.parkCode }">	<!-- Send park code--when clicked--through query string, as "detail". -->
							<c:out value="${ park.parkName }" />		<!-- Display the park name. -->
						</a>
					</h4>
				</div>

				<div id="parkdescription">
					<c:out value="${ park.parkDescription }" />			<!-- Display the park description. -->
				</div>
			</div>
		</div>
	</c:forEach>
</div>

<%@include file="common/footer.jspf"%>	<!-- include (attach) the footer file here. -->
