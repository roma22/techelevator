<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@include file="common/header.jspf"%>	<!-- include (attach) the header file here. -->

<div class="surveyresults">
	<h1>You've reached the Survey Result Page</h1>

	<c:set var="i" value="0" />
	<c:forEach var="park" items="${ parks }">
		<div class="park">
			<div class="info">
				<div id="image">
					<c:url var="imgUrl" value="/img/parks/${ park.parkCode.toLowerCase() }.jpg" />	<!-- Change each parkCode to lower case so it can be found in correct filename for display. -->
					<a href="detail?parkCode=${ park.parkCode }">	<!-- Send park code--when clicked--through query string, as "detail". -->
						<img src="${ imgUrl }" alt="Lovely ${ park.parkName } National Park Image" />	<!-- Display park image and include "alt" text. -->
					</a>
				</div>

				<div id="parkname">
					<h4>
						<a href="detail?parkCode=${ park.parkCode }">	<!-- Send park code--when clicked--through query string, as "detail". -->
							<c:out value="${ park.parkName }" />		<!-- Display the park name. -->
						</a>
					</h4>
				</div>
			</div>

			<div id="rank">
				<div id="rankText">
					Number of Votes 
				</div>
				<c:out value="${ ranks.get(i) }" />	<!-- Get the rank value--number of surveys--of each park. -->
			</div>
		</div>
		<c:set var="i" value="${ i + 1 }" />
	</c:forEach>
</div>
<%@include file="common/footer.jspf"%>	<!-- include (attach) the footer file here. -->
