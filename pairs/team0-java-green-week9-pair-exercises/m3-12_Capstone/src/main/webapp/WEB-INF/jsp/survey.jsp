<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%@include file="common/header.jspf"%>	<!-- include (attach) the header file here. -->

<h1>You've reached the Survey Page</h1>
<hr />

<c:url var="formAction" value="/survey" />
<form:form action="${ formAction }" method="POST" modelAttribute="survey">
<form:errors path="*" style="color: red"/>
	<div class="group">
		<label for="parkCode">Favorite park:</label>
		<form:select path="parkCode">
			<c:set var="i" value="0" />
			<c:forEach var="park" items="${ parkAbbreviation }">
				<option value="${ park }">
					<c:out value="${ parkFullName.get(i) }" />
				</option>
				<c:set var="i" value="${ i + 1 }" />
			</c:forEach>
		</form:select>
	</div>

	<div class="group">
		<label for="emailAddress">Email:</label>
		<form:input type="email" path="emailAddress" placeholder=" enter email" />
	</div>

	<div class="group">
		<label for="state">State of residence:</label>
		<form:select path="state">
			<c:forEach var="state" items="${ stateList }">
				<option value="${ state }">
					<c:out value="${ state }" />
				</option>
			</c:forEach>
		</form:select>
	</div>

	<div class="group">
		<label for="activityLevel">Activity level:</label>
		<div id="activity">
			<form:radiobutton path="activityLevel" value="inactive"/>
			&nbsp;Inactive
			<form:radiobutton path="activityLevel" value="sedentary"/>
			&nbsp;Sedentary
			<form:radiobutton path="activityLevel" value="active"/>
			&nbsp;Active
			<form:radiobutton path="activityLevel" value="extremely active"/>
			&nbsp;Extremely Active
		</div>
	</div>
	<div class="group">
		<input type="submit" value="Submit" />
	</div>
	<div>
		<a href="surveyResult">Go to survey results</a>
	</div>
</form:form>
<%@include file="common/footer.jspf"%>	<!-- include (attach) the footer file here. -->
