package com.techelevator.npgeek.model.jdbc;

import java.util.List;

import com.techelevator.npgeek.model.Weather;
import com.techelevator.npgeek.model.WeatherDAO;
import java.util.ArrayList;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;

@Component
public class JDBCWeatherDAO implements WeatherDAO	// Create a class "JDBCWeatherDAO" which implements the WeatherDAO interface.
{
	private JdbcTemplate jdbcTemplate;	// Create a "JdbcTemplate" type object. Spring framework does all the behind-the-scenes opening and closing of db connections.

	@Autowired
	public JDBCWeatherDAO(DataSource dataSource)
	{
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public List<Weather> getWeatherForecast(String parkCode)
	{
		List<Weather> parkWeather = new ArrayList<>();	// Create a "List" of "Weather" and declare it's name "parkWeather".
		String sqlSelectWeatherByParkCode = "SELECT * FROM weather WHERE parkcode = ?";	// Give the sql query a name so we can use it in the future.
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlSelectWeatherByParkCode, parkCode);	// Substitute the "parkCode" provided to the method into the "?" in the sql statement/query.
		while (results.next())	// Continue looking at the rows while there are additional rows to view.
		{
			parkWeather.add(mapRowToWeather(results));	// Use helper method "mapRowToWeather" to put each database field into the "parkWeather" object. 
		}
		return parkWeather;	// Return object to method-caller.
	}

	private Weather mapRowToWeather(SqlRowSet row)	// Helper method to get each of the strings from the database and add their appropriately-typed values to the "weather" object.
	{
		Weather weather = new Weather();			// Create "Weather" object and call it "weather". We will set the object's variables to the associated database value.
		weather.setParkCode(row.getString("parkcode"));							// "parkcode" is the database column label. What is the other term used for "column"?
		weather.setFiveDayForecastValue(row.getInt("fivedayforecastvalue"));
		weather.setLow(row.getInt("low"));
		weather.setHigh(row.getInt("high"));
		weather.setForecast(row.getString("forecast"));
		return weather;
	}
}
