package com.techelevator.npgeek.model;

import java.util.Map;

public interface SurveyDAO {	// Create a SurveyDAO interface class.
	
	public void addSurveyToDb(Survey survey);		// Declare method to add the survey results to the database.
	public Map<String, Object> getParksByRank();	// Declare a method--which has no input arguments--to get a list of the parks and order them by number of survey responses.
													// Outputs a "Map" with "key"s of "String" type and "value"s of "Object" type.
}
