package com.techelevator.npgeek.model.jdbc;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;

import com.techelevator.npgeek.model.Park;
import com.techelevator.npgeek.model.ParkDAO;

@Component
public class JDBCParkDAO implements ParkDAO	// Create a class "JDBCParkDAO" which implements "ParkDAO".
{
	private JdbcTemplate jdbcTemplate;	// Create a "JdbcTemplate" object and name it "jdbcTemplate".

	@Autowired
	public JDBCParkDAO(DataSource dataSource)	// Not sure what this is doing?
	{
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public List<Park> getListOfParks()	// A method to get the list of all parks.
	{
		List<Park> allParks = new ArrayList<>();	// Create a "List" of "Park"s object and name it "allParks".
		String sqlSelectAllParks = "SELECT parkcode, parkname, parkdescription FROM park ORDER BY parkname";	// Create a sql statement and call it a "String" type variable "sqlSelectAllParks".
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlSelectAllParks);	// Not sure of the proper jargon for this part?
		while (results.next())	// While there are still rows of the results object, do the following.
		{
			allParks.add(mapShortRowToPark(results));	// Convert--as needed--and add to the "object" "allParks", the values from "results".
		}
		return allParks;
	}

	@Override
	public Park getParkByCode(String parkCode)	// Method to get all the information of an individual park in order to display it on the detail page.
	{
		Park park = null;	// Create a "Park"-type object and name it "park". Also, initialize it to "null".
		String sqlSelectParkByCode = "SELECT * FROM park WHERE parkcode = ?";	// Build a sql statement "String" and call it "sqlSelectParkByCode". 
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlSelectParkByCode, parkCode);
		if (results.next())	// While there are remaining rows, continue through the "SqlRowSet" called "results" and add the row results to the "park" object.
		{
			park = mapRowToPark(results);	// Helper method to add each row of "results" to the "park" object. Convert primitive type as needed in helper method.
		}
		return park;
	}

	private Park mapShortRowToPark(SqlRowSet row)	// This helper method with accept the "results" object and specify it as "row" to work with it inside the method.
	{
		Park park = new Park();
		park.setParkCode(row.getString("parkcode"));				// "parkcode" is column name.
		park.setParkName(row.getString("parkname"));				// "parkname" is column name.
		park.setParkDescription(row.getString("parkdescription"));	// "parkdescription" is column name.
		return park;
	}

	private Park mapRowToPark(SqlRowSet row)
	{
		Park park = new Park();
		park.setParkCode(row.getString("parkcode"));					// Database column name is in quotes, inside of the "row" method.
		park.setParkName(row.getString("parkname"));					// "set" the "parkName" variable of "park" to the value from the "parkname" column of the database.
		park.setState(row.getString("state"));
		park.setAcreage(row.getInt("acreage"));
		park.setElevationInFeet(row.getInt("elevationinfeet"));
		park.setMilesOfTrail((double)row.getFloat("milesoftrail"));
		park.setNumberOfCampsites(row.getInt("numberofcampsites"));
		park.setClimate(row.getString("climate"));
		park.setYearFounded(row.getInt("yearfounded"));
		park.setAnnualVisitorCount(row.getInt("annualvisitorcount"));
		park.setInspirationalQuote(row.getString("inspirationalquote"));
		park.setInspirationalQuoteSource(row.getString("inspirationalquotesource"));
		park.setParkDescription(row.getString("parkdescription"));
		park.setEntryFee(row.getInt("entryfee"));
		park.setNumberOfAnimalSpecies(row.getInt("numberofanimalspecies"));
		return park;
	}
}
