package com.techelevator.npgeek.model;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

public class Survey {	// Create a "Survey" class.

	private int surveyId;
	
	@NotBlank(message = "Park selection required, please and thank you.")
	private String parkCode;
	
	@Email(message = "Please enter a valid email")
	@NotBlank(message = "Email required, please and thank you.")
	private String emailAddress;
	
	@NotBlank(message = "State required, please and thank you.")
	private String state;
	
	@NotBlank(message = "Activity level required, please and thank you.")
	private String activityLevel;

	public int getSurveyId() {
		return surveyId;
	}

	public void setSurveyId(int surveyId) {
		this.surveyId = surveyId;
	}

	public String getParkCode() {
		return parkCode;
	}

	public void setParkCode(String parkCode) {
		this.parkCode = parkCode;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getActivityLevel() {
		return activityLevel;
	}

	public void setActivityLevel(String activityLevel) {
		this.activityLevel = activityLevel;
	}

}
