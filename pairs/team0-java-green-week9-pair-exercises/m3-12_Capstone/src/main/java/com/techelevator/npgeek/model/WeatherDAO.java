package com.techelevator.npgeek.model;

import java.util.List;

public interface WeatherDAO {	// Create a WeatherDAO interface class.
	
	public List<Weather> getWeatherForecast(String parkCode);	// Declare a method which receives a "String", and outputs a "List" of "Weather" objects.
	
}
