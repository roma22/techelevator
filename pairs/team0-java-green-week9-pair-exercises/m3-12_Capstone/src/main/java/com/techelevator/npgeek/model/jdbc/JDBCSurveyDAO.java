package com.techelevator.npgeek.model.jdbc;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;

import com.techelevator.npgeek.model.Park;
import com.techelevator.npgeek.model.Survey;
import com.techelevator.npgeek.model.SurveyDAO;
import java.util.HashMap;
import java.util.Map;

@Component
public class JDBCSurveyDAO implements SurveyDAO	// Create a class "JDBCSurveyDAO" which implements the "SurveyDAO" interface.
{
	private JdbcTemplate jdbcTemplate;	// Create a type "JdbcTemplate" object and call it "jdbcTemplate".

	@Autowired
	public JDBCSurveyDAO(DataSource dataSource)
	{
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public void addSurveyToDb(Survey survey)	// Implement a method specified by the interface.
	{
		//	Create an sql "String" statement and call it "sqlInsertSurvey".
		String sqlInsertSurvey = "INSERT INTO survey_result(parkcode, emailaddress, state, activitylevel) VALUES(?, ?, ?, ?)";
		//	Get the associated "Survey" class variable values of "survey" and substitute them for the "?"s in the sql statement.
		jdbcTemplate.update(sqlInsertSurvey, survey.getParkCode(), survey.getEmailAddress(), survey.getState(),
			survey.getActivityLevel());
	}

	@Override
	public Map<String, Object> getParksByRank()	// Implement a method specified by the interface. Takes no input arguments and outputs a "Map" of "String" "key"s and "Object" "value"s.
	{
		Map<String, Object> parksByRank = new HashMap<>();	// Declare a "Map" <String, Object> object of type "HashMap" and name it "parksByRank".
		List<Park> rankedParks = new ArrayList<>();			// Declare a "List" <Park> object of type "ArrayList" and name it "rankedParks".
		List<Integer> surveyCount = new ArrayList<>();		// Declare a "List" <Integer> object of type "ArrayList" and name it "surveyCount".
		// Create a sql statement "String" and name it "sqlSelectAllParks".
		String sqlSelectAllParks = "SELECT park.parkcode, park.parkname, COUNT(*) as totalSurveys " +
			"FROM survey_result " +
			"JOIN park ON survey_result.parkcode = park.parkcode " +
			"GROUP BY park.parkcode " +
			"ORDER BY totalSurveys DESC, park.parkname ASC";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlSelectAllParks);	// Get output of sql query and make it an object with name "results".
		while (results.next())	// While there are remaining rows of the sql query, do the following.
		{
			rankedParks.add(mapShortRowToPark(results));		// Use helper method "mapShortRowToPark" and add the "Park" objects to the "List" of "Park"s called "rankedParks".
			surveyCount.add(results.getInt("totalSurveys"));	// Add the count of surveys of each "Park" to the elements of "surveyCount" "List".
		}
		parksByRank.put("parks", rankedParks);	// "put" the key/value pairs of each item in "rankedParks" and call name that object "parks" so it can be retrieved by the JSP.
		parksByRank.put("ranks", surveyCount);
		return parksByRank;
	}

	private Park mapShortRowToPark(SqlRowSet row)	// Help method to take each "row" from the sql statement output and add it to the "Park" object named "park".
	{
		Park park = new Park();
		park.setParkCode(row.getString("parkcode"));	// "parkcode" is the column name in the database.
		park.setParkName(row.getString("parkname"));	// "parkname" is the column name in the database.
		return park;
	}
}
