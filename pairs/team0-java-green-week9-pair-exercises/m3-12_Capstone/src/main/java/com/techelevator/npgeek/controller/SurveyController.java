package com.techelevator.npgeek.controller;

import com.techelevator.npgeek.model.Park;
import com.techelevator.npgeek.model.Survey;
import com.techelevator.npgeek.model.SurveyDAO;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class SurveyController
{
	@Autowired
	private SurveyDAO surveyDAO;

	@RequestMapping(path = "/survey", method = RequestMethod.GET)
	public String goToSurveyPage(HttpServletRequest request, Survey surveyy)
	{
		request.setAttribute("stateList", createStateList());
		request.setAttribute("parkAbbreviation", createParkCodeList());
		request.setAttribute("parkFullName", createParkNameList());
		return "survey";
	}

	@RequestMapping(path = "/survey", method = RequestMethod.POST)
	public String redirectToSurveyResultPage(@Valid @ModelAttribute("survey") Survey surveyy, 
			BindingResult result, HttpServletRequest request)
	{
		if (result.hasErrors())
		{
			request.setAttribute("stateList", createStateList());
			request.setAttribute("parkAbbreviation", createParkCodeList());
			request.setAttribute("parkFullName", createParkNameList());
			return "survey";
		}
		surveyDAO.addSurveyToDb(surveyy);
		return "redirect:surveyResult";
	}

	@RequestMapping(path = "/surveyResult", method = RequestMethod.GET)
	public String goToSurveyResultPage(ModelMap modelMap)
	{
		Map<String, Object> parksByRank = surveyDAO.getParksByRank();
		List<Park> rankedParks = (List<Park>)parksByRank.get("parks");
		List<Integer> surveyCount = (List<Integer>)parksByRank.get("ranks");
		modelMap.addAttribute("parks", rankedParks);
		modelMap.addAttribute("ranks", surveyCount);
		return "surveyResult";
	}

	public List<String> createStateList()
	{
		List<String> stateList = new ArrayList<>();
		stateList.add("Alabama");
		stateList.add("Alaska");
		stateList.add("Arkansas");
		stateList.add("Arizona");
		stateList.add("California");
		stateList.add("Colorado");
		stateList.add("Connecticut");
		stateList.add("District of Columbia");
		stateList.add("Delaware");
		stateList.add("Florida");
		stateList.add("Georgia");
		stateList.add("Hawaii");
		stateList.add("Iowa");
		stateList.add("Idaho");
		stateList.add("Illinois");
		stateList.add("Indiana");
		stateList.add("Kansas");
		stateList.add("Kentucky");
		stateList.add("Louisiana");
		stateList.add("Massachusetts");
		stateList.add("Maryland");
		stateList.add("Maine");
		stateList.add("Michigan");
		stateList.add("Minnesota");
		stateList.add("Missouri");
		stateList.add("Mississippi");
		stateList.add("Montana");
		stateList.add("North Carolina");
		stateList.add("North Dakota");
		stateList.add("Nebraska");
		stateList.add("New Hampshire");
		stateList.add("New Jersey");
		stateList.add("New Mexico");
		stateList.add("Nevada");
		stateList.add("New York");
		stateList.add("Ohio");
		stateList.add("Oklahoma");
		stateList.add("Oregon");
		stateList.add("Pennsylvania");
		stateList.add("Rhode Island");
		stateList.add("South Carolina");
		stateList.add("South Dakota");
		stateList.add("Tennessee");
		stateList.add("Texas");
		stateList.add("Utah");
		stateList.add("Virginia");
		stateList.add("Vermont");
		stateList.add("Washington");
		stateList.add("Wisconsin");
		stateList.add("West Virginia");
		stateList.add("Wyoming");
		stateList.add("Guam");
		stateList.add("Puerto Rico");
		stateList.add("Virgin Islands");
		return stateList;
	}

	public List<String> createParkCodeList()
	{
		List<String> parkCodeList = new ArrayList<>();
		parkCodeList.add("CVNP");
		parkCodeList.add("ENP");
		parkCodeList.add("GNP");
		parkCodeList.add("GCNP");
		parkCodeList.add("GTNP");
		parkCodeList.add("GSMNP");
		parkCodeList.add("MRNP");
		parkCodeList.add("RMNP");
		parkCodeList.add("YNP");
		parkCodeList.add("YNP2");
		return parkCodeList;
	}

	public List<String> createParkNameList()
	{
		List<String> parkNameList = new ArrayList<>();
		parkNameList.add("Cuyahoga Valley");
		parkNameList.add("Everglades");
		parkNameList.add("Glacier");
		parkNameList.add("Grand Canyon");
		parkNameList.add("Grand Teton");
		parkNameList.add("Great Smoky Mountains");
		parkNameList.add("Mount Rainier");
		parkNameList.add("Rocky Mountains");
		parkNameList.add("Yellowstone");
		parkNameList.add("Yosemite");
		return parkNameList;
	}
}
