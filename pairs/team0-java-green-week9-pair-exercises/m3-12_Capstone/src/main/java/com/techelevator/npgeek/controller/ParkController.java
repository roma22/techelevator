package com.techelevator.npgeek.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.techelevator.npgeek.model.ParkDAO;
import com.techelevator.npgeek.model.WeatherDAO;
import org.springframework.ui.ModelMap;

@Controller
@SessionAttributes("tempUnit")
public class ParkController
{
	@Autowired
	private ParkDAO parkDAO;

	@Autowired
	private WeatherDAO weatherDAO;

	@RequestMapping(path = { "/", "/home" }, method = RequestMethod.GET)
	public String goToHomePage(HttpServletRequest request)
	{
		request.setAttribute("parks", parkDAO.getListOfParks());
		return "home";
	}

	@RequestMapping(path = "/detail", method = RequestMethod.GET)
	public String goToDetailPage(HttpServletRequest request)
	{
		String parkCode = request.getParameter("parkCode");
		request.setAttribute("park", parkDAO.getParkByCode(parkCode));
		request.setAttribute("weather", weatherDAO.getWeatherForecast(parkCode));
		request.setAttribute("advisoryMap", createAdvisoryMap());
		request.setAttribute("tempAdvisories", temperatureAdvisory(parkCode));
//		System.out.println(weatherDAO.getWeatherForecast(parkCode).get(0).getHigh());
		return "detail";
	}

	@RequestMapping(path = "/detail", method = RequestMethod.POST)
	public String goToDetailPage(@RequestParam("selectedTempUnit") String selectedTempUnit,
		@RequestParam("parkcode") String parkCode, ModelMap modelMap)
	{
		modelMap.addAttribute("tempUnit", selectedTempUnit);
		modelMap.addAttribute("parkCode", parkCode);
		return "redirect:detail#tempChangeReturn";
	}

	private Map<String, String> createAdvisoryMap()		// Create a "Map" of <String, String> "key"/"value" pairs of weather type to weather advisory.
	{
		Map<String, String> advisoryMap = new HashMap<>();
		advisoryMap.put("snow", "Remember to pack snowshoes.");
		advisoryMap.put("rain", "Remember to pack rain gear and wear waterproof shoes.");
		advisoryMap.put("thunderstorms", "Please seek shelter and avoid hiking on exposed ridges.");
		advisoryMap.put("sunny", "Please remember to pack sunblock.");
		return advisoryMap;
	}

	private List<String> temperatureAdvisory(String parkCode)	// Method to take the input parkCode and determine the required temperature advisory. Output is "List" of "String".
	{
		int dailyHigh = weatherDAO.getWeatherForecast(parkCode).get(0).getHigh();	// Use weatherDAO to get forecast high temperature.
		int dailyLow = weatherDAO.getWeatherForecast(parkCode).get(0).getLow();		// Use weatherDAO to get forecast low temperature.

		List<String> tempAdvisory = new ArrayList<>();	// Create a "List" of "String" to hold the temperature advisory, which will display.
		if (dailyHigh > 75)
		{
			tempAdvisory.add("Please remember to bring extra drinking water.");
		}
		if (dailyHigh - dailyLow > 20)
		{
			tempAdvisory.add("Please remember to layer accordingly with breathable layers.");
		}
		if (dailyLow < 20)
		{
			tempAdvisory.add("Please be advised of frigid temperatures.");
		}
		return tempAdvisory;
	}
}
