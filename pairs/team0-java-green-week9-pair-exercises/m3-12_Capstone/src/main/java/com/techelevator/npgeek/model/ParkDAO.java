package com.techelevator.npgeek.model;

import java.util.List;

public interface ParkDAO {	// Create a ParkDAO interface class.

	public List<Park> getListOfParks();			// Get the "List" of "Park"s from database.
	public Park getParkByCode(String parkCode);	// Search for and get a "Park" by it's "String" "parkCode".

}
