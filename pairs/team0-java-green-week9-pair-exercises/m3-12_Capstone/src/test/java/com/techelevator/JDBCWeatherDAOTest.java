package com.techelevator;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.jdbc.core.JdbcTemplate;

import com.techelevator.npgeek.model.Weather;
import com.techelevator.npgeek.model.jdbc.JDBCWeatherDAO;

public class JDBCWeatherDAOTest extends DAOIntegrationTest
{
	private JDBCWeatherDAO testing;
	private JdbcTemplate jdbcTemplate;

	@Before
	public void setUp()
	{
		testing = new JDBCWeatherDAO(getDataSource());
	}

	@Test
	public void getWeatherForecastTest()
	{
		jdbcTemplate = new JdbcTemplate(getDataSource());

		String addPark = "INSERT INTO park(parkcode, parkname, state, acreage, elevationinfeet, milesoftrail, " +
			"numberofcampsites, climate, yearfounded, annualvisitorcount, inspirationalquote, " +
			"inspirationalquotesource, parkdescription, entryfee, numberofanimalspecies) " +
			"VALUES ('ABC', 'Crazy Park', 'Ohio', 9000, 4321, 9999, 1, 'Hot or Cold', 1999, 100001, 'Haha!', " +
			"'Me', 'Something for description.', 1, 0)";
		jdbcTemplate.execute(addPark);

		String addWeather = "INSERT INTO weather(parkcode, fivedayforecastvalue, low, high, forecast) " +
			"VALUES ('ABC', 1, 19, 90, 'sunny with meatballs')";
		jdbcTemplate.execute(addWeather);

		List<Weather> weatherList = new ArrayList<>();
		weatherList.add(createDummyWeather());

		assertWeatherIsEqual(weatherList.get(0), testing.getWeatherForecast("ABC").get(0));
	}

	private Weather createDummyWeather()
	{
		Weather weather = new Weather();
		weather.setParkCode("ABC");
		weather.setFiveDayForecastValue(1);
		weather.setLow(19);
		weather.setHigh(90);
		weather.setForecast("sunny with meatballs");
		return weather;
	}

	private void assertWeatherIsEqual(Weather expected, Weather actual)
	{
		assertEquals(expected.getParkCode(), actual.getParkCode());
		assertEquals(expected.getFiveDayForecastValue(), actual.getFiveDayForecastValue());
		assertEquals(expected.getLow(), actual.getLow());
		assertEquals(expected.getHigh(), actual.getHigh());
		assertEquals(expected.getForecast(), actual.getForecast());
	}
}
