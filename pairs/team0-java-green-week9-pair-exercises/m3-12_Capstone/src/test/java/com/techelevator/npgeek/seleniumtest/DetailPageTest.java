package com.techelevator.npgeek.seleniumtest;

import static org.junit.Assert.assertEquals;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.techelevator.npgeek.pageobject.DetailPage;

public class DetailPageTest {

	private static WebDriver webDriver;	// Create a WebDriver object.
	private DetailPage detailPage;		// Create a DetailPage object.
	private final static int SLEEP_TIME = 2000;	// Create a sleep time constant.
	
	@BeforeClass
	public static void openWebBrowserForTesting() {	// Open the web browser for testing.
		
		String homeDir = System.getProperty("user.home");	// Create String object to represent the user's home place in directory structure.
		/* The ChromeDriver requires a system property with the name "webdriver.chrome.driver" that
		 * contains the directory path to the ChromeDriver executable. The following line assumes
		 * we have installed the ChromeDriver in a known location under our home directory */
		System.setProperty("webdriver.chrome.driver", homeDir + "/Development/Programs/chromedriver");	// Specify the file path to "chromedriver".
		webDriver = new ChromeDriver();	// Set the webDriver object to the ChromeDriver.
	}
	
	@Before
	public void openHomePage() {	// Do this before each test.
		webDriver.get("http://localhost:8080/m3-java-capstone/detail?parkCode=CVNP");	// Open the detail page for Cuyahoga Valley National Park.
		detailPage = new DetailPage(webDriver);	// Not sure what is going on here.
	}
	
	@AfterClass
	public static void closeWebBrowser() {	// Do this after each test.
		webDriver.close();
	}
	
//	@Test
//	/* Using the Page Object pattern here makes the test much more readable. */
//	public void returnHomeFromDetailTest() {
//		detailPage.goToHome();
//		
//		assertEquals("http://localhost:8080/m3-java-capstone/home", webDriver.getCurrentUrl());
//	}
	
	// Test the method "convertTempScale" by checking initial high temperature displayed--in Fahrenheit--for day one and verify with associated celsius temperature. 
	@Test
	public void convertTemperatureScaleTest() {
				
		assertEquals("High: 62 " + (char)176 + "F", webDriver.findElement(By.cssSelector(".weather .dailyWeather:nth-child(1) #high")).getText());
		
		detailPage.convertTempScale();
		
		assertEquals("High: 17 " + (char)176 + "C", webDriver.findElement(By.cssSelector(".weather .dailyWeather:nth-child(1) #high")).getText());
	}
	
	private void sleepForDemoPurposes(int millis) {	// Sleep so user can follow the testing sequence. This seems to not be used in this class.
		try {
			Thread.sleep(millis);
		} catch (InterruptedException e) {
			// Ignore...
		}
	}	
}
