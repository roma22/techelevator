package com.techelevator.npgeek.pageobject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class DetailPage {

	private WebDriver webDriver;	// Declare a WebDriver object to control the browser for Selenium testing. Is this part needed?
	private final static int SLEEP_TIME = 2000;	// Create a sleep variable to be used between each test so we can see what is going on.

	public DetailPage(WebDriver webDriver) {
		this.webDriver = webDriver;
	}
	
//	public DetailPage goToHome() {
//		sleepForDemoPurposes(SLEEP_TIME);
//		WebElement homeLink = webDriver.findElement(By.linkText("Home"));
//		
//		sleepForDemoPurposes(SLEEP_TIME);
//		homeLink.click();
//		
//		sleepForDemoPurposes(SLEEP_TIME);
//		return this;
//	}
	
	// Create a method to test the temperature scale selector.
	public DetailPage convertTempScale() {
		sleepForDemoPurposes(SLEEP_TIME);
		WebElement dropDownMenu = webDriver.findElement(By.name("selectedTempUnit"));	// Create object to select the dropdown box.
		WebElement changeButton = webDriver.findElement(By.id("changeButton"));			// Create object to select the "Change" button.
		Select clickThis = new Select(dropDownMenu);									// Create object to click the "Change" button.
				
		sleepForDemoPurposes(SLEEP_TIME);	// Sleep so user can view status of test.
		changeButton.click();				// Click the "Change" button.
		
		sleepForDemoPurposes(SLEEP_TIME);	// Sleep so user can view status of test.
		dropDownMenu = webDriver.findElement(By.name("selectedTempUnit"));	// Are we re-declaring here?
		changeButton = webDriver.findElement(By.id("changeButton"));		// Are we re-declaring here?
		clickThis = new Select(dropDownMenu);								// Click the dropdown menu box.
		
		sleepForDemoPurposes(SLEEP_TIME);	// Sleep so user can view status of test.
		clickThis.selectByValue("celsius");	// Select the "celsius" option of the dropdown menu.
		
		sleepForDemoPurposes(SLEEP_TIME);	// Sleep so user can view status of test.
		changeButton.click();				// Click the "Change" button.
		
		sleepForDemoPurposes(SLEEP_TIME);	// Sleep so user can view status of test.
		return this;						// Done with this method so return to same page.
	}
	
	private void sleepForDemoPurposes(int millis) {	// Helper method to create sleep time.
		try {
			Thread.sleep(millis);
		} catch (InterruptedException e) {
			// Ignore...
		}
	}	
}
