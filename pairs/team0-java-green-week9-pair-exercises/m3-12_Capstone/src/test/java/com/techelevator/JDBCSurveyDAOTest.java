package com.techelevator;

import com.techelevator.npgeek.model.Park;
import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;

import com.techelevator.npgeek.model.Survey;
import com.techelevator.npgeek.model.jdbc.JDBCSurveyDAO;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JDBCSurveyDAOTest extends DAOIntegrationTest
{
	private JDBCSurveyDAO testing;
	private JdbcTemplate jdbcTemplate;

	@Before
	public void setUp()
	{
		testing = new JDBCSurveyDAO(getDataSource());
	}

	@Test
	public void addSurveyToDbTest()
	{

		jdbcTemplate = new JdbcTemplate(getDataSource());

		Survey expectedSurvey = createDummySurvey("ABC", "abc@xyx", "Ohio", "inactive");

		testing.addSurveyToDb(createDummySurvey("ABC", "abc@xyx", "Ohio", "inactive"));

		SqlRowSet checkSurveys = jdbcTemplate.queryForRowSet("SELECT * FROM survey_result WHERE parkcode = 'ABC'");
		checkSurveys.next();

		Survey actualSurvey = mapRowToSurvey(checkSurveys);

		assertSurveysAreEqual(expectedSurvey, actualSurvey);
	}

	@Test
	public void getParksByRankTest()
	{
		jdbcTemplate = new JdbcTemplate(getDataSource());

		String eraseSurveyTable = "DELETE FROM survey_result";
		jdbcTemplate.execute(eraseSurveyTable);

		String eraseWeatherTable = "DELETE FROM weather";
		jdbcTemplate.execute(eraseWeatherTable);

		String eraseParkTable = "DELETE FROM park";
		jdbcTemplate.execute(eraseParkTable);

		String addParkOne = "INSERT INTO park(parkcode, parkname, state, acreage, elevationinfeet, milesoftrail, " +
			"numberofcampsites, climate, yearfounded, annualvisitorcount, inspirationalquote, " +
			"inspirationalquotesource, parkdescription, entryfee, numberofanimalspecies) " +
			"VALUES ('ABC', 'Crazy Park', 'Ohio', 9000, 4321, 9999, 1, 'Hot or Cold', 1999, 100001, 'Haha!', " +
			"'Me', 'Something for description.', 1, 0)";
		jdbcTemplate.execute(addParkOne);

		String addParkTwo = "INSERT INTO park(parkcode, parkname, state, acreage, elevationinfeet, milesoftrail, " +
			"numberofcampsites, climate, yearfounded, annualvisitorcount, inspirationalquote, " +
			"inspirationalquotesource, parkdescription, entryfee, numberofanimalspecies) " +
			"VALUES ('LMNOP', 'Elemeno Park', 'Ohio', 9000, 4321, 9999, 1, 'Hot or Cold', 1999, 100001, 'Haha!', " +
			"'Me', 'Something for description.', 1, 0)";
		jdbcTemplate.execute(addParkTwo);

		String addParkThree = "INSERT INTO park(parkcode, parkname, state, acreage, elevationinfeet, milesoftrail, " +
			"numberofcampsites, climate, yearfounded, annualvisitorcount, inspirationalquote, " +
			"inspirationalquotesource, parkdescription, entryfee, numberofanimalspecies) " +
			"VALUES ('XYZ', 'Wild Park', 'Ohio', 9000, 4321, 9999, 1, 'Hot or Cold', 1999, 100001, 'Haha!', " +
			"'Me', 'Something for description.', 1, 0)";
		jdbcTemplate.execute(addParkThree);

		String addSurveyOne = "INSERT INTO survey_result(parkcode, emailaddress, state, activitylevel) " +
			"VALUES('LMNOP', 'abc@xyz', 'Ohio', 'inactive')";
		jdbcTemplate.execute(addSurveyOne);

		String addSurveyTwo = "INSERT INTO survey_result(parkcode, emailaddress, state, activitylevel) " +
			"VALUES('ABC', 'abc@xyz', 'Ohio', 'inactive')";
		jdbcTemplate.execute(addSurveyTwo);

		String addSurveyThree = "INSERT INTO survey_result(parkcode, emailaddress, state, activitylevel) " +
			"VALUES('LMNOP', 'abc@xyz', 'Ohio', 'inactive')";
		jdbcTemplate.execute(addSurveyThree);

		Park expectedParkOne = createDummyPark("ABC", "Crazy Park", "Ohio", 9000, 4321, 9999.0, 1, "Hot or Cold", 1999, 100001, "Haha!", "Me", "Something for description.", 1, 0);
		Park expectedParkTwo = createDummyPark("LMNOP", "Elemeno Park", "Ohio", 9000, 4321, 9999.0, 1, "Hot or Cold", 1999, 100001, "Haha!", "Me", "Something for description.", 1, 0);
//		Park expectedParkThree = createDummyPark("XYZ", "Wild Park", "Ohio", 9000, 4321, 9999.0, 1, "Hot or Cold", 1999, 100001, "Haha!", "Me", "Something for description.", 1, 0);

//		Survey expectedSurveyOne = createDummySurvey("LMNOP", "abc@xyx", "Ohio", "inactive");
//		Survey expectedSurveyTwo = createDummySurvey("ABC", "abc@xyx", "Ohio", "inactive");
//		Survey expectedSurveyThree = createDummySurvey("LMNOP", "abc@xyx", "Ohio", "inactive");

		List<Park> expectedRankedParks = new ArrayList<>();
		expectedRankedParks.add(expectedParkTwo);
		expectedRankedParks.add(expectedParkOne);

		List<Integer> expectedSurveyCount = new ArrayList<>();
		expectedSurveyCount.add(2);
		expectedSurveyCount.add(1);

		Map<String, Object> expectedParksByRank = new HashMap<>();
		expectedParksByRank.put("parks", expectedRankedParks);
		expectedParksByRank.put("ranks", expectedSurveyCount);

		List<Park> actualRankedParks = (List)testing.getParksByRank().get("parks");
		assertParksAreEqual(expectedRankedParks.get(0), actualRankedParks.get(0));
		assertParksAreEqual(expectedRankedParks.get(1), actualRankedParks.get(1));

		assertSurveyListsAreEqual(expectedSurveyCount, (List)testing.getParksByRank().get("ranks"));

//		assertMapsAreEqual(expectedParksByRank, testing.getParksByRank());
	}

	private Survey createDummySurvey(String parkCode, String emailAddress, String state, String activityLevel)
	{
		Survey survey = new Survey();
		survey.setParkCode(parkCode);
		survey.setEmailAddress(emailAddress);
		survey.setState(state);
		survey.setActivityLevel(activityLevel);
		return survey;
	}

	private Survey mapRowToSurvey(SqlRowSet results)
	{
		Survey survey = new Survey();
		survey.setParkCode(results.getString("parkcode"));
		survey.setEmailAddress(results.getString("emailaddress"));
		survey.setState(results.getString("state"));
		survey.setActivityLevel(results.getString("activitylevel"));
		return survey;
	}

	private void assertSurveysAreEqual(Survey expected, Survey actual)
	{
		assertEquals(expected.getParkCode(), actual.getParkCode());
		assertEquals(expected.getEmailAddress(), actual.getEmailAddress());
		assertEquals(expected.getState(), actual.getState());
		assertEquals(expected.getActivityLevel(), actual.getActivityLevel());
	}

	private Park createDummyPark(String parkCode, String parkName, String state, Integer acreage, Integer elevationinfeet, Double milesOfTrail,
		Integer numberOfCampsites, String climate, Integer yearFounded, Integer annualVisitorCount, String inspirationalQuote,
		String inspirationalQuoteSource, String parkDescription, Integer entryFee, Integer numberOfAnimalSpecies)
	{
		Park park = new Park();
		park.setParkCode(parkCode);
		park.setParkName(parkName);
		park.setState(state);
		park.setAcreage(acreage);
		park.setElevationInFeet(elevationinfeet);
		park.setMilesOfTrail(milesOfTrail);
		park.setNumberOfCampsites(numberOfCampsites);
		park.setClimate(climate);
		park.setYearFounded(yearFounded);
		park.setAnnualVisitorCount(annualVisitorCount);
		park.setInspirationalQuote(inspirationalQuote);
		park.setInspirationalQuoteSource(inspirationalQuoteSource);
		park.setParkDescription(parkDescription);
		park.setEntryFee(entryFee);
		park.setNumberOfAnimalSpecies(numberOfAnimalSpecies);
		return park;
	}

	private void assertParksAreEqual(Park expected, Park actual)
	{
		assertEquals(expected.getParkCode(), actual.getParkCode());
		assertEquals(expected.getParkName(), actual.getParkName());
//		assertEquals(expected.getState(), actual.getState());
//		assertEquals(expected.getAcreage(), actual.getAcreage());
//		assertEquals(expected.getElevationInFeet(), actual.getElevationInFeet());
//		assertEquals(expected.getMilesOfTrail(), actual.getMilesOfTrail());
//		assertEquals(expected.getNumberOfCampsites(), actual.getNumberOfCampsites());
//		assertEquals(expected.getClimate(), actual.getClimate());
//		assertEquals(expected.getYearFounded(), actual.getYearFounded());
//		assertEquals(expected.getAnnualVisitorCount(), actual.getAnnualVisitorCount());
//		assertEquals(expected.getInspirationalQuote(), actual.getInspirationalQuote());
//		assertEquals(expected.getInspirationalQuoteSource(), actual.getInspirationalQuoteSource());
//		assertEquals(expected.getParkDescription(), actual.getParkDescription());
//		assertEquals(expected.getEntryFee(), actual.getEntryFee());
//		assertEquals(expected.getNumberOfAnimalSpecies(), actual.getNumberOfAnimalSpecies());
	}

	private void assertSurveyListsAreEqual(List expected, List actual)
	{
		assertEquals(expected.get(0), actual.get(0));
		assertEquals(expected.get(1), actual.get(1));
	}

	private void assertMapsAreEqual(Map expected, Map actual)
	{
		assertEquals(expected.get("parks"), actual.get("parks"));
		assertEquals(expected.get("ranks"), actual.get("ranks"));
	}
}
