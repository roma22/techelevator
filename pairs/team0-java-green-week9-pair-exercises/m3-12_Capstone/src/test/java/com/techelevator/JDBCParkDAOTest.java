package com.techelevator;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;

import com.techelevator.npgeek.model.Park;
import com.techelevator.npgeek.model.jdbc.JDBCParkDAO;

public class JDBCParkDAOTest extends DAOIntegrationTest
{
	private JDBCParkDAO testing;
	private JdbcTemplate jdbcTemplate;

	@Before
	public void setUp()
	{
		testing = new JDBCParkDAO(getDataSource());
	}

	@Test
	public void listAllParksTest()
	{
		jdbcTemplate = new JdbcTemplate(getDataSource());

		SqlRowSet result = jdbcTemplate.queryForRowSet("SELECT count(*) as total from park");
		result.next();

		List<Park> parkList = testing.getListOfParks();

		assertEquals(result.getInt("total"), parkList.size());
	}

	@Test
	public void getParkByCodeTest()
	{
		jdbcTemplate = new JdbcTemplate(getDataSource());

		String statement = "INSERT INTO park(parkcode, parkname, state, acreage, elevationinfeet, milesoftrail, " +
			"numberofcampsites, climate, yearfounded, annualvisitorcount, inspirationalquote, " +
			"inspirationalquotesource, parkdescription, entryfee, numberofanimalspecies) " +
			"VALUES ('ABC', 'Crazy Park', 'Ohio', 9000, 4321, 9999, 1, 'Hot or Cold', 1999, 100001, 'Haha!', " +
			"'Me', 'Something for description.', 1, 0)";
		jdbcTemplate.execute(statement);

		assertParksAreEqual(createDummyPark(), testing.getParkByCode("ABC"));
	}

	private Park createDummyPark()
	{
		Park park = new Park();
		park.setParkCode("ABC");
		park.setParkName("Crazy Park");
		park.setState("Ohio");
		park.setAcreage(9000);
		park.setElevationInFeet(4321);
		park.setMilesOfTrail(9999.0);
		park.setNumberOfCampsites(1);
		park.setClimate("Hot or Cold");
		park.setYearFounded(1999);
		park.setAnnualVisitorCount(100001);
		park.setInspirationalQuote("Haha!");
		park.setInspirationalQuoteSource("Me");
		park.setParkDescription("Something for description.");
		park.setEntryFee(1);
		park.setNumberOfAnimalSpecies(0);
		return park;
	}

	private void assertParksAreEqual(Park expected, Park actual)
	{
		assertEquals(expected.getParkCode(), actual.getParkCode());
		assertEquals(expected.getParkName(), actual.getParkName());
		assertEquals(expected.getState(), actual.getState());
		assertEquals(expected.getAcreage(), actual.getAcreage());
		assertEquals(expected.getElevationInFeet(), actual.getElevationInFeet());
		assertEquals(expected.getMilesOfTrail(), actual.getMilesOfTrail());
		assertEquals(expected.getNumberOfCampsites(), actual.getNumberOfCampsites());
		assertEquals(expected.getClimate(), actual.getClimate());
		assertEquals(expected.getYearFounded(), actual.getYearFounded());
		assertEquals(expected.getAnnualVisitorCount(), actual.getAnnualVisitorCount());
		assertEquals(expected.getInspirationalQuote(), actual.getInspirationalQuote());
		assertEquals(expected.getInspirationalQuoteSource(), actual.getInspirationalQuoteSource());
		assertEquals(expected.getParkDescription(), actual.getParkDescription());
		assertEquals(expected.getEntryFee(), actual.getEntryFee());
		assertEquals(expected.getNumberOfAnimalSpecies(), actual.getNumberOfAnimalSpecies());
	}
}
