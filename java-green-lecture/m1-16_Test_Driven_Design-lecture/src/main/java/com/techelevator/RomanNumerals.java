package com.techelevator;

import java.util.HashMap;
import java.util.Map;

public class RomanNumerals {
	
	
	private Map<Integer, String> romanMap;
	
	public RomanNumerals() {
		romanMap = new HashMap<Integer, String> ();
	}
	
	
	public String convertToRoman(int number) {
		
		
		
		String romanFormat = "";
		loadHashMap();
		
		int evenlyDividedTimes = number / 10;
		
		if (evenlyDividedTimes > 0) {
			romanFormat = romanMap.get(new Integer(10 * evenlyDividedTimes));
		}
		
		int remainder = number % 10;
		
		if (remainder != 0 ) {
			romanFormat += romanMap.get(new Integer(remainder));
		}
		
		//romanFormat = romanMap.get(new Integer(number));
		
		
		
		return romanFormat;
		
	}
	
	
	private void loadHashMap() {
		

		romanMap.put(new Integer(1), "I");  // 1
		romanMap.put(new Integer(2), "II");  // 2
		romanMap.put(new Integer(3), "III");  // 3
		romanMap.put(new Integer(4), "IV");  // 4
		romanMap.put(new Integer(5), "V");  // 5
		romanMap.put(new Integer(6), "VI");  // 6
		romanMap.put(new Integer(7), "VII");  // 7
		romanMap.put(new Integer(8), "VIII");  // 8
		romanMap.put(new Integer(9), "IX");  // 9
		
		romanMap.put(new Integer(10), "X");  // 10
		romanMap.put(new Integer(20), "XX");  // 10
		romanMap.put(new Integer(30), "XXX");  // 10
		romanMap.put(new Integer(40), "XL");  // 10
		romanMap.put(new Integer(50), "L");  // 10
		romanMap.put(new Integer(60), "LX");  // 10
		romanMap.put(new Integer(70), "LXX");  // 10
		romanMap.put(new Integer(80), "LXXX");  // 10
		romanMap.put(new Integer(90), "XC");  // 10
		romanMap.put(new Integer(100), "C");  // 10
		
		
		romanMap.put(new Integer(50), "L");  // 50
		romanMap.put(new Integer(100), "C");  // 100
		romanMap.put(new Integer(500), "D");  // 500
		romanMap.put(new Integer(1000), "M");  // 1000
		
		
	}

}
