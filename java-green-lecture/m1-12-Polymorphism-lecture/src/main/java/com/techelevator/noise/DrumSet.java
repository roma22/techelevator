package com.techelevator.noise;

public class DrumSet implements INoise{
	
	private String manufacturer;
	private int numberOfCymbols;
	
	public String getManufacturer() {
		return manufacturer;
	}
	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}
	public int getNumberOfCymbols() {
		return numberOfCymbols;
	}
	public void setNumberOfCymbols(int numberOfCymbols) {
		this.numberOfCymbols = numberOfCymbols;
	}
	
	@Override
	public String makeNoise() {
		// TODO Auto-generated method stub
		return "BANG";
	}

	
	
}
