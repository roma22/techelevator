package com.techelevator.noise;

public interface IForSale {
     double getSalePrice();
}
