package com.techelevator.noise;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NoiseApp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		ChainSaw chainsaw = new ChainSaw();
		//System.out.println(chainsaw.makeNoise());
		
		DrumSet drumSet = new DrumSet();
		//System.out.println(drumSet.makeNoise());
		
		Guitar guitar = new Guitar();
		//System.out.println(guitar.makeNoise());
		
		Tractor tractor = new Tractor();
		//System.out.println(tractor.makeNoise());
		
		
		List<INoise> noisyItems = new ArrayList<INoise>();
		noisyItems.add(chainsaw);
		noisyItems.add(drumSet);
		noisyItems.add(guitar);
		noisyItems.add(tractor);
		
		
		for (INoise item : noisyItems) {
			System.out.println(item.makeNoise());
		}
		
		
		Map<String, IForSale> itemsForSale = new HashMap<String, IForSale> ();
		itemsForSale.put("123", chainsaw);
		itemsForSale.put("456", guitar);
		
		
		
		
		
		
		
		
		
		
		
		
		
		

	}

}
