package com.techelevator.noise;

public class Guitar implements INoise, IForSale{
	
	private String manufacturer;
	private String color;
	
	
	public String getManufacturer() {
		return manufacturer;
	}
	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	
	@Override
	public String makeNoise() {
		// TODO Auto-generated method stub
		return "AWESOME LEAD SOLO!";
	}
	@Override
	public double getSalePrice() {
		// TODO Auto-generated method stub
		return 1;
	}
	
	

}
