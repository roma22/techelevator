-- destroy old tables so we can recreate them from scratch
DROP TABLE IF EXISTS phone;
DROP TABLE IF EXISTS email;
DROP TABLE IF EXISTS address;
DROP TABLE IF EXISTS contact;


CREATE TABLE contact
(
	contactId serial,
	lastName varchar(100) not null,
	firstName varchar(100) not null,
	dateAdded timestamp not null,
	dateLastUpdated timestamp null,
	
	constraint pk_contactId primary key (contactId)
);

CREATE TABLE address
(
	addressId serial,
	contactId int not null,
	addressType varchar(100) not null,
	addressOtherDescription varchar(200),
	addressLine1 varchar(200) not null,
	addressLine2 varchar(200) null,
	city varchar(100) not null,
	state varchar(50) not null,
	postalCode varchar(10) not null,
	
	constraint pk_addressId primary key (addressId),
	constraint fk_addressContactId foreign key (contactId) references contact(contactId),
	constraint chk_addressType check (addressType IN ('Home', 'Work', 'Shipping', 'Billing', 'Other')),
	constraint chk_addressOtherDescription check ((addressType = 'Other' AND addressOtherDescription is not null) OR (addressType <> 'Other' AND addressOtherDescription is null))
);

CREATE TABLE phone
(
	phoneId serial,
	contactId int not null,
	phoneType varchar(100) not null,
	phoneOtherDescription varchar(200),
	phoneNumber varchar(100) not null,

	constraint pk_phoneId primary key (phoneId),
	constraint fk_phoneContactId foreign key (contactId) references contact(contactId),
	constraint chk_phoneType check (phoneType IN ('Home', 'Work', 'Mobile', 'Other')),
	constraint chk_phoneOtherDescription check ((phoneType = 'Other' AND phoneOtherDescription is not null) OR (phoneType <> 'Other' AND phoneOtherDescription is null))
);

CREATE TABLE email
(
	emailId serial,
	contactId int not null,
	emailType varchar(100) not null,
	emailOtherDescription varchar(200),
	emailAddress varchar(100) not null,

	constraint pk_emailId primary key (emailId),
	constraint fk_emailContactId foreign key (contactId) references contact(contactId),
	constraint chk_emailType check (emailType IN ('Home', 'Work', 'Other')),
	constraint chk_emailOtherDescription check ((emailType = 'Other' AND emailOtherDescription is not null) OR (emailType <> 'Other' AND emailOtherDescription is null))
);



--populate data
INSERT INTO contact (lastName, firstName, dateAdded) VALUES ('Detore', 'Katie', current_timestamp);
INSERT INTO contact (lastName, firstName, dateAdded) VALUES ('Knisley', 'Ben', current_timestamp);
INSERT INTO contact (lastName, firstName, dateAdded) VALUES ('Fulton', 'John', current_timestamp);
INSERT INTO contact (lastName, firstName, dateAdded) VALUES ('Carmichael', 'Steve', current_timestamp);
INSERT INTO contact (lastName, firstName, dateAdded) VALUES ('Lauvray', 'Brian', current_timestamp);


INSERT INTO email (contactId, emailType, emailAddress) VALUES (1, 'Home', 'katie@gmail.com');
INSERT INTO email (contactId, emailType, emailAddress) VALUES (1, 'Work', 'katie@techelevator.com');
INSERT INTO email (contactId, emailType, emailAddress) VALUES (2, 'Home', 'ben@gmail.com');
INSERT INTO email (contactId, emailType, emailAddress) VALUES (2, 'Work', 'ben@techelevator.com');
INSERT INTO email (contactId, emailType, emailAddress) VALUES (3, 'Home', 'john@gmail.com');
INSERT INTO email (contactId, emailType, emailAddress) VALUES (4, 'Work', 'steve@techelevator.com');


INSERT INTO phone (contactId, phoneType, phoneNumber) VALUES (1, 'Home', '555-555-5555');
INSERT INTO phone (contactId, phoneType, phoneNumber) VALUES (1, 'Mobile', '555-555-5555');
INSERT INTO phone (contactId, phoneType, phoneNumber) VALUES (2, 'Home', '555-555-5555');
INSERT INTO phone (contactId, phoneType, phoneNumber) VALUES (2, 'Work', '234-555-3567');
INSERT INTO phone (contactId, phoneType, phoneNumber) VALUES (3, 'Home', '333-555-4343');
INSERT INTO phone (contactId, phoneType, phoneNumber) VALUES (4, 'Mobile', '777-888-9999');

INSERT INTO address (contactId, addressType, addressOtherDescription, addressLine1, city, state, postalCode) 
VALUES (3, 'Other', 'Vacation Home', '123 Cayman Island Rd', 'Cayman City', 'Cayman Islands', '43333');