CREATE TABLE ourtable (
        id serial primary key,   -- serial will create an sequential value and a sequence
        name varchar(10)
        --CONSTRAINT pk_id PRIMARY KEY (id)
);

SELECT * FROM ourtable;

SELECT nextval('ourtable_id_seq');   -- Select the next sequence value

CREATE SEQUENCE our_customer_sequence START 101;  -- Creates a sequence manually
ALTER SEQUENCE our_customer_sequence RESTART 1000 INCREMENT 2;

SELECT nextval('our_customer_sequence');

DROP SEQUENCE our_customer_sequence;

INSERT INTO ourtable (id, name) VALUES (DEFAULT, 'Joe');  -- Can use default to insert with a sequence
INSERT INTO ourtable (id, name) VALUES ( (SELECT nextval('ourtable_id_seq')), 'Jane');  -- Can use a subquery to get the next sequence
INSERT INTO ourtable (name) VALUES ('Sally');  -- Can exclude it from the insert to get the default value
INSERT INTO ourtable (id, name) VALUES (11, 'Steve');  -- Can insert id manually, but this may have unexpected results
INSERT INTO ourtable (name) VALUES ('John');

-- Transactions do not affect sequences
START TRANSACTION;
INSERT INTO ourtable (name) VALUES ('Andrew');
ROLLBACK;

