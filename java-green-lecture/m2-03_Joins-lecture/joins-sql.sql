-- These queries require the JOINS database

--INNER JOIN  (DEFAULT IF JUST USE JOIN)

SELECT * 
FROM one 
INNER JOIN two 
ON one.number = two.number



--LEFT JOIN  (Left Outer Join)
SELECT *
FROM one
LEFT JOIN two 
ON one.number = two.number



--RIGHT JOIN  (Right Outer Join)
SELECT *
FROM one
RIGHT JOIN two
ON one.number = two.number



--FULL OUTER JOIN
SELECT *
FROM one
FULL OUTER JOIN TWO 
ON one.number = two.number



--Everything from ONLY the left table
SELECT *
FROM one
LEFT JOIN two 
ON one.number = two.number
WHERE two.number IS NULL


--Everything from ONLY the right table
SELECT *
FROM one
RIGHT JOIN two
ON one.number = two.number
WHERE one.number IS NULL



--Everything in ONLY the Left or Right table, but not both
SELECT *
FROM one
FULL OUTER JOIN two ON one.number = two.number
WHERE one.number IS NULL OR two.number IS NULL



