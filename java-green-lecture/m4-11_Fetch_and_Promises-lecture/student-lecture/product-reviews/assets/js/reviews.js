


let reviews = [];

function loadReviews () {
    console.log("Load Reviews");
    
    fetch ('data.json')
      .then((response) => {
          return response.json();
      })
      .then((data) => {
          reviews = data;
          displayReviews();
      })
      .catch((err) => {
         console.log();
      }) 
}

const button = document.querySelector('button');
button.addEventListener('click', function() {
  loadReviews();
  button.disabled = true;
});

function displayReviews() {

    console.log('DisplayReviews');

// first check to make sure the browser supports content templates
if('content' in document.createElement('template')) {
    // query the document for .reviews and assign it to a variable called container
    const container = document.querySelector(".reviews");
    // loop over the reviews array
    reviews.forEach((review) => {
      // get the template; find all the elements and add the data from our review to each element
      const tmpl = document.getElementById('review-template').content.cloneNode(true);
      tmpl.querySelector('img').setAttribute("src",review.avatar);
      tmpl.querySelector('.username').innerText = review.username;
      tmpl.querySelector('h2').innerText = review.title;
      tmpl.querySelector('.published-date').innerText = review.publishedOn;
      tmpl.querySelector('.user-review').innerText = review.review;
      container.appendChild(tmpl);
    });
  } else {
    console.error('Your browser does not support templates');
  }    


}

























/*
    Pending,  Fulfilled, Rejected

    What would you return to get the text contents of demo.txt

     fetch('demo.txt')
         .then( function(response) {
             // return here
        });

    -->   return response.text()

  What would you return to get the JSON data from a response

     fetch('data.json')
         .then( function(response) {
             // return here
      });  

    return response.json()

*/


/*

STEVE's NOTE TO NOT  FORGET COVERING TEMPLATES FOR EXAMPLE CODE IN LECTURE

The HTML Content Template (<template>) element is a mechanism for holding client-side content that 
is not to be rendered when a page is loaded but may subsequently be instantiated during runtime 
using JavaScript.

Think of a template as a content fragment that is being stored for subsequent use in the 
document. While the parser does process the contents of the <template> element 
while loading the page, it does so only to ensure that those contents are valid; 
the element's contents are not rendered, however.



<table id="producttable">
  <thead>
    <tr>
      <td>UPC_Code</td>
      <td>Product_Name</td>
    </tr>
  </thead>
  <tbody>
    <!-- existing data could optionally be included here -->
  </tbody>
</table>

<template id="productrow">
  <tr>
    <td class="record"></td>
    <td></td>
  </tr>
</template>

Now that the table has been created and the template defined, we use JavaScript to 
insert rows into the table, with each row being constructed using the template as its basis.


*/


