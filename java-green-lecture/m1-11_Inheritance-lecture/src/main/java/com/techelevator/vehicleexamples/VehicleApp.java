package com.techelevator.vehicleexamples;

import java.util.ArrayList;
import java.util.List;

public class VehicleApp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
		//creating an engine object so we can add it to the mazda
		Engine mazdaEngine = new Engine();
		mazdaEngine.setNumberOfCyclinders(4);
		mazdaEngine.setType("gas");
		
		// create a car object and initialize it using the car constructor
		Car mazda = new Car(mazdaEngine, 4);
		mazda.setFuelType("gas");

		
		System.out.println(mazda.calculateSomeRandomThing());
		
		//lets create a tank engine so we can add it to the tank below.
		Engine tankEngine = new Engine();
		tankEngine.setNumberOfCyclinders(400);  // because why not, it's a tank :-)
		tankEngine.setType("diesel");
		
		// create a car object and initialize it using the tank constructor
		Tank tank = new Tank(tankEngine, 20, "105mm");
        tank.calculateSomeRandomThing();

		//lets create a tank engine so we can add it to the ford truck below.
		Engine fordEngine = new Engine();
		fordEngine.setNumberOfCyclinders(8);  
		fordEngine.setType("grandpa's fire whiskey");

		Truck myFord = new Truck(fordEngine, 4);
		
		
		List<Vehicle> myVehicles = new ArrayList<Vehicle>();
		myVehicles.add(mazda);
		myVehicles.add(tank);
		myVehicles.add(myFord);
		
		
		Vehicle myGenericVehicle = new Vehicle();
		myGenericVehicle.setFuelType("gas");
		
		
		
		
		
		

	}

}
