package com.techelevator.vehicleexamples;

public class Engine {
	
	private int numberOfCyclinders;
	private String type;

	public int getNumberOfCyclinders() {
		return numberOfCyclinders;
	}

	public void setNumberOfCyclinders(int numberOfCyclinders) {
		this.numberOfCyclinders = numberOfCyclinders;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	

}
