package com.techelevator.vehicleexamples;

public class Vehicle {
	
	private Engine vehicleEngine;
	
	private int numberOfWheels;
	private String fuelType;
	
	//constructor
	public Vehicle (Engine engine, int numberOfWheels) {
		this.numberOfWheels = numberOfWheels;
		this.vehicleEngine = engine;
		
	}
	
	public Vehicle () {
		
	}
	
	public int getNumberOfWheels() {
		return numberOfWheels;
	}
	public void setNumberOfWheels(int numberOfWheels) {
		this.numberOfWheels = numberOfWheels;
	}
	public String getFuelType() {
		return fuelType;
	}
	public void setFuelType(String fuelType) {
		this.fuelType = fuelType;
	}
	
	public Engine getVehicleEngine() {
		return vehicleEngine;
	}
	public void setVehicleEngine(Engine vehicleEngine) {
		this.vehicleEngine = vehicleEngine;
	}
	
	public String calculateSomeRandomThing() {
		return "random";
	}
	
	

}
