package com.techelevator.vehicleexamples;

public class Tank extends Vehicle{
	
  private String turretSize;
  
	public Tank (Engine engine, int numberOfWheels, String turretSize) {
		super(engine, numberOfWheels);
		this.turretSize = turretSize;
	}  

public String getTurretSize() {
	return turretSize;
}

public void setTurretSize(String turretSize) {
	this.turretSize = turretSize;
}
  
  
  
  
}
