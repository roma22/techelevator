package com.techelevator.vehicleexamples;

public class Truck extends Vehicle{
	
	public Truck (Engine engine, int numberOfWheels) {
		super(engine, numberOfWheels);
	}

}
