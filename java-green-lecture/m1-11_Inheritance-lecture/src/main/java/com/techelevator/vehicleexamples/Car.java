package com.techelevator.vehicleexamples;

public class Car extends Vehicle {
	
	public Car (Engine engine, int numberOfWheels) {
		super();
	}
	
	public Car () {
		super();
	}
	
	public String calculateSomeRandomThing() {
		System.out.println(super.calculateSomeRandomThing());
		return "my way is better";
	}

}
