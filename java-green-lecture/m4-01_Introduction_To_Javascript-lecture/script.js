/*
    Example of a multi-line comment just like in C#/Java
*/

// Single line comment

/**
 * Functions start with the word function.
 * They don't have a return type and the naming convention is camel-case.
 */
function variables() {

  let x;              // Now x is undefined
  console.log('The value of x: ' + x);

  x = 5;              // Now x is a Number
  console.log('The value of x: ' + x);

  x = 'John';         // Now x is a String
  console.log('The value of x: ' + x);

  x = null;         // Now x is a null
  console.log('The value of x: ' + x);

  x = NaN;          // Now x is NaN
  console.log('The value of x: ' + x);
  console.log('Is x an IsNan? ' + isNaN(x));

  x = new Date();     // Now x is a Date object
  console.log('The value of x: ' + x.toString() );

  x = ["Four", "Leaf", "Clover"];     // Now x is an array
  console.log('The value of x: ' + x );
  x = x[1];
  console.log('The value of x: ' + x );

  // Declares a variable where the value cannot be changed
  const daysPerWeek = 7;
  console.log(`There are ${daysPerWeek} days in the week.`);

  // Declares a variable whose value can be changed
  let daysPerMonth = 30;
  console.log(`There are ${daysPerMonth} days in the month.`);

  // Declares a variable that will always be an array
  const weekdays = [
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
    "Sunday"
  ];
  console.table(weekdays);
}

/**
 * Arrays can grow or shrink in Javascript
 * .concat()
 * .push()       - adds an element to the end of an array
 * .pop()        - removes the last element of the array
 * .splice()     - splice(2,1) starts at 2 for a length of 1
 */
function arrayDemo() {

   let fruitArray = ['Apples', 'Bananas', 'Oranges'];
   console.log(fruitArray);

  fruitArray.push('lemon');
  console.log(fruitArray);

  fruitArray.pop();
  console.log(fruitArray);

  let nutsArray = ['peanuts', 'pecans', 'almonds', 'instructors'];
  console.log(fruitArray.concat(nutsArray));

  for (let i in fruitArray){
     console.log(fruitArray[i]);
  }
  // wait... why didn't the concatenated version print?

  fruitArray = fruitArray.concat(nutsArray);
  console.log(fruitArray);

  //selects from index 2 to end of the array
  fruitArray = fruitArray.splice(2);
  console.log(fruitArray);

  //selects from index 2 for a length of one element.
  nutsArray = nutsArray.splice(2,1);
  console.log(nutsArray);

}


/**
 * Functions can also accept parameters.
 * Notice the parameters do not have types.
 * @param {Number} param1 The first number to display
 * @param {Number} param2 The second number to display
 */
function printParameters(param1, param2) {
  console.log(`The value of param1 is ${param1}`);
  console.log(`The value of param2 is ${param2}`);
}

/**
 * Compares two values x and y.
 * == is loose equality
 * === is strict equality
 * @param {Object} x
 * @param {Object} y
 */
function equality(x, y) {
  console.log(`x is ${typeof x}`);
  console.log(`y is ${typeof y}`);

  console.log(`x == y : ${x == y}`); // true
  console.log(`x === y : ${x === y}`); // false
}

/**
 * Each value is inherently truthy or falsy.
 * false, 0, '', null, undefined, and NaN are always falsy
 * everything else is always truthy
 * @param {Object} x The object to check for truthy or falsy,
 */
function falsy(x) {
  if (x) {
    console.log(`${x} is truthy`);
  } else {
    console.log(`${x} is falsy`);
  }
}

/**
 * Maps:
 */
function mapDemo() {

  const map = new Map([
    [1, 'one'],
    [2, 'two'],
    [3, 'three'],
    ]);
    
    
    console.log(map.size);

    map.set('foo', true);
    console.log(map.size);

    map.set(4, "four");
    console.log(map.size);

    console.log(map.get(2));
    console.log(map.get('foo'));

    /**
     * wait? I thought you couln't change a constant?
     * 
     * If we assign a primitive valie to a constant, we cannot change
     * the primitive value.
     * 
     * Constant 'Objects' can change... but you can't reassign 
     * another object to the const variable.
     * 
     * Ref: https://www.w3schools.com/js/js_const.asp
     * */ 

}

/**
 *  Objects are simple key-value pairs
    - values can be primitive data types
    - values can be arrays
    - or they can be functions
*/
function objects() {
  const person = {
    firstName: "Bill",
    lastName: "Lumbergh",
    age: 42,
    employees: [
      "Peter Gibbons",
      "Milton Waddams",
      "Samir Nagheenanajar",
      "Michael Bolton"
    ],
    toString: function() {
      return `${this.lastName}, ${this.firstName} (${this.age})`;
    }
  };

  console.table(person);

  console.log(`${person.firstName} ${person.lastName}`);

  for (let i = 0; i < person.employees.length; i++) {
    console.log(`Employee ${i + 1} is ${person.employees[i]}`);
  }

  console.log(person.toString());
}

/*
########################
Function Overloading
########################

Function Overloading is not available in Javascript. If you declare a
function with the same name, more than one time in a script file, the
earlier ones are overriden and the most recent one will be used.
*/

function add(num1, num2) {
  return num1 + num2;
}

function add(num1, num2, num3) {
  return num1 + num2 + num3;
}

/*
########################
Math Library
########################

A built-in `Math` object has properties and methods for mathematical constants and functions.
*/

function mathFunctions() {
  console.log("Math.PI : " + Math.PI);
  console.log("Math.LOG10E : " + Math.LOG10E);
  console.log("Math.abs(-10) : " + Math.abs(-10));
  console.log("Math.floor(1.99) : " + Math.floor(1.99));
  console.log("Math.ceil(1.01) : " + Math.ceil(1.01));
  console.log("Math.random() : " + Math.random());
}

/*
########################
String Methods
########################

The string data type has a lot of properties and methods similar to strings in Java/C#
*/

function stringFunctions(value) {
  console.log(`.length -  ${value.length}`);
  console.log(`.endsWith('World') - ${value.endsWith("World")}`);
  console.log(`.startsWith('Hello') - ${value.startsWith("Hello")}`);
  console.log(`.indexOf('Hello') - ${value.indexOf("Hello")}`);

  /*
    Other Methods
        - split(string)
        - substr(number, number)
        - substring(number, number)
        - toLowerCase()
        - trim()
        - https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String
    */
}
