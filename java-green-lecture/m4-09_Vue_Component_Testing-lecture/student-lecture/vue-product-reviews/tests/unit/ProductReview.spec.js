import ProductReview from '@/components/ProductReview.vue';
/* eslint-disable-next-line no-unused-vars */
import { shallowMount, Wrapper } from '@vue/test-utils';

//the following two lines initialize chai
import chai from 'chai';
chai.should();

//describe() is where are tests are created. 

// Generally 3 categories of tests:
//  1.  DOM tests
//  2.  Component Data Change tests
//  3.  Logic tests

//shallow mount allows us to fire up the component in an isolated environment, without needing the whole Vue app 
describe('ProductReview', () => {
  /** @type Wrapper */
  let wrapper;

  //beforeEach is like the @Before in Junit tests.  The function inside gives us a clean instance each time (like new SomeClass() did in Java) 
  beforeEach( () => {
    wrapper = shallowMount(ProductReview);
  });

  // This tests to make sure a valid wrapper instance has been created
  // This test should always be included to make sure everything was created
  // correctly and we didn't have a run-time error on load.

  it('should be a Vue instance', () => {
    wrapper.isVueInstance().should.be.true;
  });

  // this is an example of a DOM CHANGE TEST
  it('should show a review on the UI', () => {
    const testReviews = [{
      reviewer: 'TEST NAME',
      title: 'TEST TITLE',
      rating: 3,
      review: 'TEST REVIEW'
    }];

   //SET DATA takes our testReviews array and set's it in the component
    wrapper.setData({ reviews: testReviews });

    // Should only have one review
    wrapper.findAll('div.review').length.should.equal(1);

    // Review should show what was set on data
    wrapper.find('div.review h4').text().should.equal('TEST NAME');
    wrapper.find('div.review h3').text().should.equal('TEST TITLE');
    wrapper.find('div.review p').text().should.equal('TEST REVIEW');
    wrapper.find('div.review div.rating').element.children.length.should.equal(3);
  });

   // this is an example of a DOM CHANGE TEST
  it('should filter reviews when filter is set', () => {
    const testReviews = [
      {
        reviewer: 'NO SHOW',
        title: 'NO SHOW',
        rating: 1,
        review: 'NO SHOW'
      },
      {
        reviewer: 'TEST NAME',
        title: 'TEST TITLE',
        rating: 5,
        review: 'TEST REVIEW'
      }
    ];

    //SET DATA takes our testReview array and set's it in the component
    wrapper.setData({
      reviews: testReviews,
      filter: 5
    });

    // Should only have one review pass the filter - findAll grabs all matching istances of the element - in this case 
    // we only want to find one, the filtered review
    wrapper.findAll('div.review').length.should.equal(1);

    // Review should show what was set on data
    // find should go after an exact match.
    wrapper.find('div.review h4').text().should.equal('TEST NAME');
    wrapper.find('div.review h3').text().should.equal('TEST TITLE');
    wrapper.find('div.review p').text().should.equal('TEST REVIEW');
    wrapper.find('div.review div.rating').element.children.length.should.equal(5);
  });


  // this is an example of a DOM CHANGE TEST

  it('should show and hide link when showForm is toggled', () => {
    wrapper.setData({ showForm: true });

    //data-* is a HTML5 attribute added to our html code so that we can set data-'anything here' to set up a data value 
    //that we want associated with our element.  It can then be accessed in Javascript at run time to check and see if the
    //rendered component exists for a given scenario. In this case, we are accessing it during the test to check the 
    // presence of the element specifically if the element should be shown or not.
    wrapper.find('a[data-test="show-form-anchor"]').exists().should.be.false;

    wrapper.setData({ showForm: false });

    wrapper.find('a[data-test="show-form-anchor"]').exists().should.be.true;
  });


    // this is an example of a DOM CHANGE TEST
  it('should show and hide the form when showForm is toggled', () => {
    wrapper.setData({ showForm: true });

    wrapper.find('form').exists().should.be.true;

    wrapper.setData({ showForm: false });

    wrapper.find('form').exists().should.be.false;
  });



    // this is an example of a DATA CHANGE TEST
  it('should fill out a newReview object when form is filled', () => {
    wrapper.setData({ showForm: true });

    // Fill the form
    wrapper.find('input#reviewer').setValue('TEST NAME');
    wrapper.find('input#title').setValue('TEST TITLE');
    wrapper.find('textarea#review').setValue('TEST REVIEW');
    wrapper.find('select#rating').setValue('3');

    // deep.equal is override === to allow for values in inner objects to be checked against
    // other inner objects(etc. expected result).   So it is like a recursive equals for objects.
    wrapper.vm.newReview.should.deep.equal({
      reviewer: 'TEST NAME',
      title: 'TEST TITLE',
      review: 'TEST REVIEW',
      rating: 3
    });
  });



  // THESE TESTS SPECIFICALLY TEST LOGIC IN OUR METHODS
  // Most follow a pattern of: 
  // 1. set the data of the component
  // 2. call the method
  // 3. verify that the method did as desired.

  describe('Methods', () => {
    it('should add a new review when addNewReview is called', () => {
      const testReview = {
        reviewer: 'TEST NAME',
        title: 'TEST TITLE',
        review: 'TEST REVIEW',
        rating: 5
      };

      wrapper.setData({
        reviews: [],              //create an empty reviews array
        newReview: testReview     // set newReview object in component equal to testReview
      });

      wrapper.vm.addNewReview();    // add the new review previously set above
      wrapper.vm.reviews[0].should.deep.equals(testReview);   //test that it was added to reviews array inside component
    });

    // note: we are not testing for DOM elements, we did that earlier. We are adding a test value for reviewer, then resetting the form
    // and then making sure the data is empty inside with the deep equal.

    it('should clear the form and close the form when resetForm is called', () => {
      wrapper.setData({
        newReview: { reviewer: 'TEST' },
        showForm: true
      });

      wrapper.vm.resetForm();

      wrapper.vm.newReview.should.deep.equal({});
      wrapper.vm.showForm.should.be.false;
    });

    // since we have a lot of methods testing the number or reviews, we can bundle them inside their own describe method

    describe('numberOfReviews', () => {
      it('should return a proper count of reviews', () => {
        const testReviews = [
          {
            reviewer: 'TEST1',
            rating: 1
          },
          {
            reviewer: 'TEST2',
            rating: 3
          },
          {
            reviewer: 'TEST3',
            rating: 2
          },
          {
            reviewer: 'TEST4',
            rating: 3
          }
        ];

        wrapper.vm.numberOfReviews(testReviews, 3).should.equal(2);
        wrapper.vm.numberOfReviews(testReviews, 1).should.equal(1);
        wrapper.vm.numberOfReviews(testReviews, 5).should.equal(0);
      });

      it('should return 0 when there are no reviews', () => {
        wrapper.vm.numberOfReviews([], 1).should.equal(0);
        wrapper.vm.numberOfReviews([], 5).should.equal(0);
      });
    });
  });


  //the next group of tests,  tests computed properties.

  describe('Computed Properties', () => {
    const testReviews = [
      {
        reviewer: 'TEST1',
        rating: 1
      },
      {
        reviewer: 'TEST2',
        rating: 3
      },
      {
        reviewer: 'TEST3',
        rating: 2
      },
      {
        reviewer: 'TEST4',
        rating: 3
      },
      {
        reviewer: 'TEST5',
        rating: 2
      },
      {
        reviewer: 'TEST6',
        rating: 3
      },
      {
        reviewer: 'TEST7',
        rating: 5
      },
      {
        reviewer: 'TEST8',
        rating: 5
      }
    ];

    it('should return only count of one star reviews from numberOfOneStarReviews', () => {
      wrapper.setData({ reviews: testReviews });
      wrapper.vm.numberOfOneStarReviews.should.equal(1);
    });

    it('should return only count of two star reviews from numberOfTwoStarReviews', () => {
      wrapper.setData({ reviews: testReviews });
      wrapper.vm.numberOfTwoStarReviews.should.equal(2);
    });
    it('should return only count of three star reviews from numberOfThreeStarReviews', () => {
      wrapper.setData({ reviews: testReviews });
      wrapper.vm.numberOfThreeStarReviews.should.equal(3);
    });
    it('should return only count of four star reviews from numberOfFourStarReviews', () => {
      wrapper.setData({ reviews: testReviews });
      wrapper.vm.numberOfFourStarReviews.should.equal(0);
    });
    it('should return only count of five star reviews from numberOfFiveStarReviews', () => {
      wrapper.setData({ reviews: testReviews });
      wrapper.vm.numberOfFiveStarReviews.should.equal(2);
    });

    it('should have only filtered reviews in filteredReviews', () => {
      wrapper.setData({ reviews: testReviews, filter: 2 });
      wrapper.vm.filteredReviews.length.should.equal(2);
      wrapper.vm.filteredReviews[0].reviewer.should.equal('TEST3');
      wrapper.vm.filteredReviews[1].reviewer.should.equal('TEST5');
    });

    it('should have all reviews in filteredReviews when filter is zero', () => {
      wrapper.setData({ reviews: testReviews, filter: 0 });
      wrapper.vm.filteredReviews.length.should.equal(8);
    });

    it('should have average review total in averageRating', () => {
      wrapper.setData({
        reviews: [
          {
            reviewer: 'TEST1',
            rating: 1
          },
          {
            reviewer: 'TEST2',
            rating: 5
          }
        ]
      });

      wrapper.vm.averageRating.should.equal(3);
    });


    // THIS IS JUST A DEMO USING CHAI ASSERT... WE COULD HAVE USED should.equal(3)
    it('numberOfReviews should return correct count', () => {
      const testReviews = [
        {
          reviewer: 'TEST1',
          rating: 2
        },
        {
          reviewer: 'TEST2',
          rating: 3
        },
        {
          reviewer: 'TEST3',
          rating: 1
        }
      ];

      let count = wrapper.vm.numberOfReviews(testReviews, 3);
      chai.assert(1, count);
      /* The preferred way to test this would be using a chained should:
       *    wrapper.vm.numberOfReviews(testReviews, 3).should.equal(1);
       * Direct use of the chai Asserts are rarely needed, in fact there is nothing
       * in this component that requires it, but this gives an example of using one.
       */
    });


    
  });
});