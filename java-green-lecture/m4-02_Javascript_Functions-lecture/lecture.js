/**
 * All named functions will have the function keyword and
 * a name followed by parentheses.
 */
function returnOne() {
  return 1;
}

/**
 * Functions can also take parameters. These are just variables that are filled
 * in by whoever is calling the function.
 *
 * Also, we don't *have* to return anything from the actual function.
 * 
 * PARAMETERS IN JAVASCRIPT ARE NEVER REQUIRED!
 *
 * @param {any} value the value to print to the console
 */
function printToConsole(value) {
  console.log(value);
}

/**
 * Multiply two numbers together. But what happens of we don't pass
 * a value in? What happens if the value is not a number?
 *
 * @param {number} firstParameter the first parameter to multiply
 * @param {number} secondParameter the second parameter to multiply
 */
function multiplyTogether(firstParameter, secondParameter) {
  return firstParameter * secondParameter;
}

/**
 * This version makes sure that no parameters are ever missing. If
 * someone calls this function without parameters, we default the
 * values to 0. However, it is impossible in JavaScript to prevent
 * someone from calling this function with data that is not a number.
 *
 * @param {number} [firstParameter=0] the first parameter to multiply
 * @param {number} [secondParameter=0] the second parameter to multiply
 */
function multiplyNoUndefined(firstParameter = 0, secondParameter = 0) {
  return firstParameter * secondParameter;
}

/**
 * Scope is defined as where a variable is available to be used.
 *
 * If a variable is declare inside of a block, it will only exist in
 * that block and any block underneath it. Once the block that the
 * variable was defined in ends, the variable disappears.
 */
function scopeTest() {
  // This variable will always be in scope in this function
  let inScopeInScopeTest = true;

  {
    // this variable lives inside this block and doesn't
    // exist outside of the block
    let scopedToBlock = inScopeInScopeTest;
  }

  // scopedToBlock doesn't exist here so an error will be thrown
  if (inScopeInScopeTest && scopedToBlock) {
    console.log("This won't print!");
  }
}

/**
 * JSDoc Example
 *
 * Take the details of a person and create an English readable sentence
 * that uses that information to describe them. We join the quirks together
 * with the separator, or `, ` by default.
 *
 * @param {string} name the name of the person we're describing
 * @param {number} age the age of the person
 * @param {string[]} [listOfQuirks] a list of funny quirks that we'll list out
 * @param {string} [separator=', '] the string to separate the quirks by
 * @returns {string} the full descriptive string
 */
function createSentenceFromUser(name, age, listOfQuirks = [], separator = ', ') {
  let description = `${name} is currently ${age} years old. Their quirks are: `;
  return description + listOfQuirks.join(separator);
}


/**
 * A simple function that we will use below ti show how it can be assigned to variables
 * 
 * Functions are VALUES just like a string, number, Array, etc.
 * 
 * @param {number} x number to be doubled
 * @returns {number} the result of doubling x 
 * 
 */
function doubleMe(x) {
  return x * 2;
}

console.log('1: This is a simple example of calling a function directly...');
console.log(doubleMe(2));

// Function values can be assigned to other variables!
console.log('2: This is a simple example showing how we can assign a function to a variable and call it');
let doubleMyX = doubleMe(5);
console.log(doubleMyX);

// Functions can be passed into other functions known as higher ordered functions. This allows us
// to take smaller functions and compose them into more complex functions leading to better
// organized code, less lines of code, and reusability which all leads to less bugs in our code.

/**
 * 
 * Let's look at an example using filter. 
 * filter is a function of an array. It allows us to pass it a function as input and
 * then use that function to return a new array that has values filtered out based on
 * the actions of the passed function.
 * 
 * Functions that are passed to other functions are known as: callback functions
 *  
 */
let animals = [
   { name:'Bugs Bunny', species: 'rabbit'},
   { name:'Pluto', species: 'dog'},
   { name:'Snoopy', species: 'dog'},
   { name:'Tom', species: 'cat'},
   { name:'Jerry', species: 'mouse'}
]

//let's filter out just the dogs...

//we could do this using an ordinary for loop (ES5)

let dogArray = [];

for (let i=0; i<animals.length; i++) {
  if (animals[i].species === 'dog') {
    dogArray.push(animals[i]);
  }
}
console.log('3: for loop example: ' + JSON.stringify(dogArray));   //useful for turning objects into strings
console.log(dogArray);  //another way


/**
 * now let's rewrite as a filter function. 
 * 
 * Filter will loop through each element of the array and pass the value of each element
 * into the callback function. When it does, the callback will test the condition defined in
 * the callback function and return either 'true' or 'false'.  If true, it will add that element
 * to the new array, thus filtering out any elements that don't pass the condition.
 * */

let filteredDogs = animals.filter(function(animals) {
   return animals.species === 'dog';
})

console.log('4: array filter example: ' + JSON.stringify(filteredDogs));  //useful for turning objects into strings
console.log(filteredDogs);   //another way

/**
 * 
 * Now, let's start over and rewrite our function in another way to make it more reusable and composable
 * Because this is it's own method, it is no longer dependent on filter. This looks a lot cleaner!
 */
let isDog = function(animals) {
  return animals.species === 'dog';
}

let dogList = animals.filter(isDog);
console.log('5: another filter example: ' + JSON.stringify(dogList)); 


/**
 * Takes an array and returns a new array of only numbers that are
 * multiples of 3
 *
 * @param {number[]} numbersToFilter numbers to filter through
 * @returns {number[]} a new array with only those numbers that are
 *   multiples of 3
 */
function allDivisibleByThree(numbersToFilter) {
  return numbersToFilter.filter((number) => {
    return number % 3 === 0;
  });
}



/**
 * 
 * Map Example:  Map is another highered ordered function that goes through an array, but instead of throwing
 * aways values like filter,  it transforms them in some way.
 * 
 * Let's REUSE our animal object array from above
 * 
 */

 // let's solve this using the for loop approach first
 let names = [];
 for (let i=0; i<animals.length; i++) {
   names.push(animals[i].name);
 }
 console.log(names); 


/** when we used filter, it expected a callback function that returned true/false if an item was to be filtered.
* In map, it will return all items, but it expects a transformed obect that it will put into a new array.
* A common use case is to just return all names in an object array (not the whole object) or any field (like species)
*/
let mapNames = animals.map(function(animal){
    return animal.name;
})
console.log(mapNames);    // this returns the exact same response as the for loop

let modifiedMapNames = animals.map(function(animal){
  return animal.name + ' is a type of ' + animal.species;
})
console.log(modifiedMapNames); 


/**
 * 
 * Another big difference between the for loop approach and the new filter, map, approach is method size. We can 
 * compress function size down in terms of number of characters.  This is important as web pages and mobile apps
 * need to load these files.  (Compare # of characters in for loop to mapNames above)
 * 
 * But we can shorten things even further with =>, AKA  arrow, fat arrow, or rocket notation
 */

 //now rewrite the code above and put on one line

 let mapNamesBefore = animals.map(function(animal){ return animal.name; })

//now lets use arrow scripts.  I will leave them commented out so code still compiles and you can see 
//the transition from above.  We can remove the word function and then insert => arrow before return

//let mapNamesBefore = animals.map((animal) => { return animal.name; })

 

//now if your statement fits on one line like this one does, you can get rid of the return statement and
//curly brackets
// let mapNamesBefore = animals.map((animal) => animal.name);

console.log(mapNamesBefore); 


/**
 * 
 * Reduce example - 
 * 
 * We have seen a few example of array functions (map, filter, etc) and there are others. But what
 * happens when you don't have one that seems to match what you want to do?  That is where reduce comes in.
 * 
 * Reduce allows you to do ANY type of transformation to a list based on yorur rules
 * 
 * 
 */
let ages = [
  { age: 25 },
  { age: 15 },
  { age: 75 },
  { age: 28 },
  { age: 33 },
  { age: 41 }
]

//let's say you want to add up and summarize all the amounts.

//let's start with a traditional for loop
let sum = 0;
for (let i=0; i<ages.length; i++) {
  sum += ages[i].age;
}
console.log('sum: ' + sum);

//now let's use reduce
let sumOfAges = ages.reduce(function(sum, age) {
    //console.log("here:", sum, ages);
    return sum + age.age;
}, 0)
console.log('sumOfAges: ' + sumOfAges);





/**
 * Takes an array and, using the power of anonymous functions, generates
 * their sum.
 *
 * @param {number[]} numbersToSum numbers to add up
 * @returns {number} sum of all the numbers
 */
function sumAllNumbers(numbersToSum) {
  return numbersToSum.reduce(
    /**
     * Add each number to the previous sum using reduce
     */
    (sum, number) => {
      return sum + number;
    }
  );
}


