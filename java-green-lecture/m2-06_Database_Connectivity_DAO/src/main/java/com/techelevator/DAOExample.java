package com.techelevator;

import java.util.List;

import org.apache.commons.dbcp2.BasicDataSource;

import com.techelevator.city.City;
import com.techelevator.city.CityDAO;
import com.techelevator.city.JDBCCityDAO;

public class DAOExample {

	public static void main(String[] args) {
	
		//setting up Datasource
		BasicDataSource worldDataSource = new BasicDataSource();
		worldDataSource.setUrl("jdbc:postgresql://localhost:5432/world");
		worldDataSource.setUsername("postgres");
		worldDataSource.setPassword("postgres1");
		
		
		// create a DAO object and pass it the datasource
		CityDAO dao = new JDBCCityDAO(worldDataSource);
		
		// create a city object to pass to the DAO to 'save'
		City smallville = new City();
		smallville.setCountryCode("USA");
		smallville.setDistrict("KS");
		smallville.setName("Smallville");
		smallville.setPopulation(42080);
		
		//call the dao to INSERT the city  to the database
		dao.save(smallville);
		
		//now let's SELECT the city we just saved... How did we get the id?  Let's look at the save method in the DAO
		City theCity = dao.findCityById(smallville.getId());
		System.out.println("City name retrieved is: " + theCity.getName());
		
		//now let's UPDATE the city we just saved with a new name
		theCity.setName("BigVille");
		dao.update(theCity);
		
		//now let's SELECT the city again and see if the name changed
		theCity = dao.findCityById(theCity.getId());
		System.out.println("City new name is: " + theCity.getName());
		
		//now let's delete the city we just added
		dao.delete(theCity.getId());
		
		//now let's SELECT again and see if it is there
		theCity = dao.findCityById(theCity.getId());
		if (theCity == null) {
			System.out.println("city has been deleted");
		} 
		else {
			System.out.println("City name: " + theCity.getName() + " was found!");
		}
		
		
		// call another dao method to find all cities in Dhaka....
		List<City> cities = dao.findCityByDistrict("Dhaka");
		for (City c : cities) {
			System.out.println(c.getName());
		}
		
		
		
	}

}
